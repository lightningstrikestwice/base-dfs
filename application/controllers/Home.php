<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index(){
        if ($this->Users->is_loggedin()) {redirect('lobby');}

        $this->load->view(THEME.'/header_home');
        $this->load->view(THEME.'/home');
        $this->load->view(THEME.'/footer');
	}
}
