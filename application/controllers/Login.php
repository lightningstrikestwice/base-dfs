<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->form_validation->set_error_delimiters('<p class="alert alert-danger p-1 mt-1">', '</p>');
	}

	public function index(){
        if ($this->Users->is_loggedin()) {redirect('lobby');}

        $siteOptions = $this->Options->get_group(1);

        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == false) {
		} else {

			// set variables from the form
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if ($this->Users->resolve_user_login($username, $password)) {
				$user   = $this->Users->get_user_from_login($username);
				$userID = $user->cid;

                // Update IP Address and Login Time
                $ip = $this->Misc->get_ip();

                $update = array(
                    'ip'=>$ip,
                    'last_login'=>time()
                );

                $this->Users->add_update($update,$userID);

                $this->session->set_userdata('user',$user);
                $this->session->set_userdata('email', $user->email);

				if($_SESSION['camefrom']){
					$camefrom = $_SESSION['camefrom'];
					unset($_SESSION['camefrom']);

                    $this->session->set_flashdata('success', 'Welcome back to '. $siteOptions['website_name']);
					redirect($camefrom);
				}

                $this->session->set_flashdata('success', 'Welcome back to '. $siteOptions['website_name']);
				redirect('/lobby');
			} else {
				// login failed
                $this->session->set_flashdata('error', 'Wrong username or password. Please try again.');
                redirect('/login');
			}


        }



        $this->load->view(THEME.'/header', $head);
        $this->load->view(THEME.'/login', $data);
        $this->load->view(THEME.'/footer');

    }

    public function login_as($userID) {

    }
}
