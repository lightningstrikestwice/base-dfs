<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_account extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->Users->is_loggedin(1);

        $this->form_validation->set_error_delimiters('<p class="alert alert-danger p-1 mt-1">', '</p>');
	}

	public function index(){
        redirect('my_account/manage');
	}

    public function invite_friends() {
        $siteOptions = $this->Options->get_group(1);

        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('email[]', 'Email Addresses', 'trim');

		if ($this->form_validation->run() == false) {
		} else {
            $post = $this->input->post();

            $user = $this->Users->get_by_id($this->Users->id());
            $emailData = array('name'=>$user->first_name.' '.$user->last_name);

            foreach ($post['email'] as $friend) {
                if ($friend != "") {
                    $this->Emails->send_mail($friend, 'invite', $emailData);
                }
            }

            $this->Alerts->set('success', 'Invites were sent to your friends');
            redirect('my_account/invite_friends');
        }

        $this->load->view(THEME.'/header', $head);
        $this->load->view(THEME.'/invite', $data);
        $this->load->view(THEME.'/footer');
    }


    public function manage() {
        $siteOptions = $this->Options->get_group(1);
        $genOptions = $this->Options->get_group(3);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $data['user'] = $user = $this->Users->get_by_id($this->Users->id());
        $data['provinces'] = $this->Misc->province_list();
        $data['countries'] = $this->Misc->countries_list();
        $data['months'] = $this->Misc->months();


        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$password = $this->input->post('password');
        $email = $this->input->post('email');

		$this->form_validation->set_rules('email', 'Email', 'strip_tags|trim|required|valid_email|max_length[256]|min_length[4]|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'strip_tags|trim|required|max_length[64]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'strip_tags|trim|required|max_length[64]|xss_clean');
 		$this->form_validation->set_rules('phone', 'Phone Number', 'strip_tags|trim|xss_clean|min_length[8]');
		$this->form_validation->set_rules('address', 'Address', 'strip_tags|trim|xss_clean');
		$this->form_validation->set_rules('city', 'City', 'strip_tags|trim|xss_clean');
		$this->form_validation->set_rules('state', 'State', 'strip_tags|required|trim|xss_clean');
		$this->form_validation->set_rules('country', 'Country', 'strip_tags|required|trim|xss_clean');
		$this->form_validation->set_rules('zip', 'Zip/Postal Code', 'strip_tags|trim|xss_clean');
		$this->form_validation->set_rules('gender', 'Gender', 'strip_tags|trim|xss_clean');
		$this->form_validation->set_rules('dob[]', 'Date of Birth', 'strip_tags|trim|strip_tags|xss_clean');

        if (strlen($password) > 0){
			$this->form_validation->set_rules('password', 'Password', 'required|xss_clean|max_length['.$genOptions['max_password_length'].']|min_length['.$genOptions['min_password_length'].']');
			$this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required|xss_clean|matches[password]|max_length['.$genOptions['max_password_length'].']|min_length['.$this->options->get('min_password_length').']');
		}

        if ($user->email != $email) {
            $this->form_validation->set_rules('email', 'Email', 'strip_tags|trim|required|valid_email|max_length[256]|min_length[4]|xss_clean|is_unique[users.email]', array('is_unique' => 'This email address already exists. Please choose another one.'));
        } else {
            $this->form_validation->set_rules('email', 'Email', 'strip_tags|trim|required|valid_email|max_length[256]|min_length[4]|xss_clean');
        }

		if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();

            $dob = array(
                "month" => $post['month'],
                "date" => $post['date'],
                "year" => $post['year']
            );

            $post['dob'] = json_encode($dob);

            unset($post['month']);
            unset($post['date']);
            unset($post['year']);
            unset($post['password']);
            unset($post['password_confirm']);

            $this->Users->add_update($post,$post['cid']);

            $this->Alerts->set('success','Your account information has been saved');
            redirect('my_account/manage');
        }
        $this->load->view(THEME.'/header', $head);
        $this->load->view(THEME.'/my_account/manage', $data);
        $this->load->view(THEME.'/footer');
    }

    public function profile_settings() {
        $this->load->model('Mailchimp');

        $siteOptions = $this->Options->get_group(1);
        $genOptions = $this->Options->get_group(3);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $data['user'] = $user = $this->Users->get_by_id($this->Users->id());

        $data['profileSettings'] = $settings = json_decode($user->profile_settings,true);
        $data['mcLists'] = $this->Options->get_option('mc_active_lists');

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$password = $this->input->post('password');
        $email = $this->input->post('email');

		$this->form_validation->set_rules('profile_settings[]', 'Settings', 'trim|xss_clean');

        if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();

            $mcSettings = $post['mc_sett'];
            unset($post['mc_sett']);

            $post['profile_settings'] = json_encode($post['profile_settings']);
            $this->Users->add_update($post,$post['cid']);

			$mcSubscribe = false;

            foreach($mcLists as $id=>$name){
				$mcInfo = $this->Mailchimp->lists_member_info($id,$user->email);
				$status = $mc_info->data[0]->status;

                if($status=="subscribed" && (empty($mcSettings) || !in_array($id,$mcSettings))){
                    // Unsubscribing
					$this->Mailchimp->lists_unsubscribe($id,$user->email);
				}elseif($status!="subscribed" && !empty($mcSettings) && in_array($id,$mcSettings)){
                    // Subscribing
					$this->Mailchimp->lists_subscribe($id,$user->email);
					$mcSubscribe=true;
				}
			}

			if($mcSubscribe){
                $alert = "Your settings have been saved. You have also signed up for a newsletter, check your email to confirm your subscription.";
			}else{
                $alert = "Your Profile Settings been saved";
			}

            $this->Alerts->set('success',$alert);
            redirect('my_account/profile_settings');
        }
        $this->load->view(THEME.'/header', $head);
        $this->load->view(THEME.'/my_account/profile_settings', $data);
        $this->load->view(THEME.'/footer');
    }

    public function edit_photo() {
        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        // Add Scripts/Styles
        $ftd = $this->config->item('ftd_version');

        $scripts = array(
            site_url('assets/'.GLOBAL_CONTENT.'/js/jcrop/js/jquery.Jcrop.js?v='.$ftd),
            site_url('assets/'.GLOBAL_CONTENT.'/js/plupload/js/plupload.full.min.js?v='.$ftd),
            site_url('assets/'.THEME.'/js/picture_upload.js?v='.$ftd),
        );
        $head['scripts'] = $scripts;

        $styles = array(
            site_url('assets/'.GLOBAL_CONTENT.'/js/jcrop/css/jquery.Jcrop.css?v='.$ftd)
        );
        $head['styles'] = $styles;

        $userID = $this->Users->id();
        $user = $this->Users->get_by_id($userID);

        $data['user'] = $user;

        $this->load->view(THEME.'/header', $head);
		$this->load->view(THEME.'/my_account/edit_photo',$data);
		$this->load->view(THEME.'/footer');
    }

    function delete_avatar() {
        $userID = $this->session->userdata('user')->cid;
        $user = $this->Users->get_by_id($userID);
        $avatar = $user->avatar;

        if(!empty($avatar)){
            //delete files
            @unlink('./uploads/user_avatar/'.$avatar);

            //update db
            $update=array("avatar"=>"");
            $this->db->where("cid",$userID);
            $this->db->update("users",$update);

            //update session
            $this->session->set_userdata('avatar','');

            $this->Alerts->set('success','Profile picture deleted');
        }else{
            $this->Alerts->set('error','No image to delete');
        }

        redirect('my_account/edit_photo');
    }

    public function withdrawal_request() {
        $siteOptions = $this->Options->get_group(1);
        $data['genOptions'] = $genOptions = $this->Options->get_group(3);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $userID = $this->Users->id();
        $user = $this->Users->get_by_id($userID);
        $data['user'] = $user;
        $data['userBalance'] = $userBalance = $this->User_funds->user_balance($userID)['balance'];

        $methods = $this->Options->get_option('withdrawal_methods');

        $data['methods'] = json_decode($methods,true);
        $data['statuses'] = $this->config->item('withdrawal_statuses');



		//get recent withdrawals
		$data['requests'] = $this->User_funds->get_withdrawals(array("user_id"=>$userID));

        $payment_method_posted = $this->input->post('payment_method');
		$this->form_validation->set_rules('amount', 'Withdrawal Amount', 'required|numeric|callback_has_enough_money');
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'required|integer|callback_in_range[0,2]');
		if ($payment_method_posted == '0'){
			$this->form_validation->set_rules('paypal_email', 'PayPal Email', 'required|valid_email');
		}
		if ($payment_method_posted == '1'){
			$this->form_validation->set_rules('check_name', 'Check Name', 'required|xss_clean|trim|max_length[64]');
			$this->form_validation->set_rules('check_address[]', 'Check Address', 'required|xss_clean|trim|max_length[256]');
		}
		if ($payment_method_posted == '2'){
			$this->form_validation->set_rules('bitcoin_address', 'Bitcoin Address', 'required|trim');
		}

		if ($this->form_validation->run() === FALSE){
		}else{
            $post = $this->input->post();

			if($post['amount'] < 0 ){
                $this->Alerts->set('error','Negative numbers not allowed');
				redirect('my_account/withdrawal_request');
			}

            //success
			$item = array(
				'user_id' => $userID,
				'amount' => $post['amount'],
				'method' => $payment_method_posted,
				'req_timestamp' => time(),
			);

			if($payment_method_posted == '0'){
				$item['paypal_email'] = $post['paypal_email'];
			}elseif($payment_method_posted == '1'){
				$address = $post['check_address'];
				$f_add=$address['address']."\n".$address['city'].", ".$address['zip'];
				$item['check_name'] = $post['check_name'];
				$item['check_address'] = $f_add;
            } elseif($payment_method_posted == '2') {
                $item['bitcoin_address'] = $post['bitcoin_address'];
            }
			$this->User_funds->create_withdrawal($item);

            $this->Alerts->set('success','Withdrawal request received. Please allow 3-5 business days to process your request.');
			redirect('my_account/withdrawal_request');
		}

        $this->load->view(THEME.'/header', $head);
		$this->load->view(THEME.'/my_account/withdrawal_request',$data);
		$this->load->view(THEME.'/footer');
    }

    public function deposit_funds() {
        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $userID = $this->Users->id();
        $user = $this->Users->get_by_id($userID);

        $data['user'] = $user;

        $this->load->view(THEME.'/header', $head);
		$this->load->view(THEME.'/my_account/deposit_funds',$data);
		$this->load->view(THEME.'/footer');
    }


























	/*--------------------------------------------------------------------------
		CALLBACK FUNCTIONS
	--------------------------------------------------------------------------*/
	function in_range($str = '', $range_str = ''){
		//parse arr
		$range = explode(',', $range_str);

		if (is_array($range) && count($range) == 3)
		{
			if (!in_array($str, $range))
			{
				$this->form_validation->set_message('in_range', 'The %s field must be in the range of '.$range[0].'-'.$range[1]);
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		return TRUE;
	}

	function has_enough_money($amount){

		$user_balance = $this->User_funds->user_balance($this->Users->id());
		$total_pending = $this->User_funds->pending_withdrawal_amt($this->Users->id());
		if ($user_balance['balance'] < $amount)
		{
			$this->form_validation->set_message('has_enough_money', 'Not enough funds. <br />Your current balance is $'.$user_balance['balance'].($total_pending > 0?'  ( $'.number_format($total_pending, 2, '.', '').' pending withdrawal. )':''));
			return FALSE;
		}
		else
		{
			return TRUE;
		}

	}


}
