<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->load->model('Pages');
	}

	public function index(){

	}

    public function view($slug) {
        $siteOptions = $this->Options->get_group(1);

        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $page = $this->Pages->get_by_slug($slug);


        $head['title'] = $page->browser_title;
        $head['seoDescription'] = $page->meta_description;
        $head['seoKeywords'] = $page->meta_keywords;

        $data['pageTitle'] = $page->title;
        $data['body'] = $page->body;

        $this->load->view(THEME.'/header', $head);
        $this->load->view(THEME.'/page', $data);
        $this->load->view(THEME.'/footer');
    }
}
