<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model('Pages');
        $this->load->model('Emails');
        $this->load->model('Faqs');
        $this->load->model('Tickers');
        $this->load->model('Sliders');

        $this->form_validation->set_error_delimiters('<p class="alert alert-danger p-1 mt-1">', '</p>');
	}

	public function index(){
        redirect('admin/content/list_pages');
	}

    public function list_pages() {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "Content Pages";

        $data['pages'] = $pages = $this->Pages->get(array('field'=>'title','dir'=>'asc'));

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/content/list_pages',$data);
        $this->load->view(ADMIN_THEME.'/footer');

    }

    public function edit_page($pageID="") {
        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['scripts'] = array(base_url().'assets/global/js/tinymce/tinymce.min.js?c='.time());

        if ($pageID == "") {
            $page = array();
            $pageTitle = "Create New Page";
        } else {
            $page = $this->Pages->get_by_id($pageID);
            $pageTitle = "Edit ".$page->slug." Page";
        }

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('title', 'Page Title', 'required|xss_clean|max_length[256]');
		$this->form_validation->set_rules('slug', 'Slug', 'required|xss_clean|max_length[256]');
		$this->form_validation->set_rules('body', 'Body', 'required');

        if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();

            $this->Pages->add_update($post, $post['cid']);

            $this->Alerts->set('success',$post['slug'].' Page has been saved.');
            redirect('admin/content/list_pages');
        }

        $data['page'] = $page;
        $data['pageID'] = $pageID;
        $head['pageTitle'] = $data['pageTitle'] = $pageTitle;

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/content/edit_page',$data);
        $this->load->view(ADMIN_THEME.'/footer');

    }

    public function delete_page($pageID) {
        $page = $this->Pages->get_by_id($pageID);

        $this->Pages->delete($pageID);

        $this->Alerts->set('success',$page->slug.' Page has been removed.');
        redirect('admin/content/list_pages');
    }

    public function list_emails() {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "Emails List";

        $data['emails'] = $pages = $this->Emails->get(array('field'=>'slug','dir'=>'asc'));
        $data['lang'] = $siteOptions['default_language'];

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/content/list_emails',$data);
        $this->load->view(ADMIN_THEME.'/footer');


    }

    public function edit_email($emailID) {
        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['scripts'] = array(base_url().'assets/global/js/tinymce/tinymce.min.js?c='.time());

        if ($emailID == "") {
            $email = array();
            $pageTitle = "Create New Email";
        } else {
            $email = $this->Emails->get_by_id($emailID);
            $email->subject = json_decode($email->subject,true)[$siteOptions['default_language']];
            $email->body = json_decode($email->body,true)[$siteOptions['default_language']];
            $pageTitle = "Edit ".$email->slug." Email";
        }

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('subject', 'Subject', 'required|xss_clean');
		$this->form_validation->set_rules('slug', 'Slug', 'required|xss_clean|max_length[256]');
		$this->form_validation->set_rules('body', 'Body', 'required');

        if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();

            $subj = array($siteOptions['default_language'] => $post['subject']);
            $body = array($siteOptions['default_language'] => $post['body']);
            $post['subject'] = json_encode($subj);
            $post['body'] = json_encode($body);

            $this->Emails->add_update($post, $post['cid']);

            $this->Alerts->set('success',$post['slug'].' Email has been saved.');
            redirect('admin/content/list_emails');
        }

        $data['email'] = $email;
        $data['emailID'] = $emailID;
        $head['pageTitle'] = $data['pageTitle'] = $pageTitle;

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/content/edit_email',$data);
        $this->load->view(ADMIN_THEME.'/footer');

   }

    public function delete_email($emailID) {
        $email = $this->Emails->get_by_id($emailID);

        $this->Emails->delete($emailID);

        $this->Alerts->set('success',$email->slug.' email has been removed.');
        redirect('admin/content/list_emails');
    }

    public function list_faqs() {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "FAQs List";

        $data['faqs'] = $faqs = $this->Faqs->get(array('field'=>'question','dir'=>'asc'));

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/content/list_faqs',$data);
        $this->load->view(ADMIN_THEME.'/footer');

    }

    public function edit_faq($faqID) {
        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['scripts'] = array();

        if ($faqID == "") {
            $faq = array();
            $pageTitle = "Create New FAQ";
        } else {
            $faq = $this->Faqs->get_by_id($faqID);
            $pageTitle = "Edit FAQ";
        }

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('question', 'Question', 'required|xss_clean');
		$this->form_validation->set_rules('answer', 'Answer', 'required|xss_clean');

        if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();

            $this->Faqs->add_update($post, $post['cid']);

            $this->Alerts->set('success','FAQ has been saved.');
            redirect('admin/content/list_faqs');
        }

        $data['faq'] = $faq;
        $data['faqID'] = $faqID;
        $head['pageTitle'] = $data['pageTitle'] = $pageTitle;

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/content/edit_faq',$data);
        $this->load->view(ADMIN_THEME.'/footer');

    }

    public function delete_faq($faqID) {
        $faq = $this->Faqs->get_by_id($faqID);

        $this->Faqs->delete($faqID);

        $this->Alerts->set('success','FAQ has been removed.');
        redirect('admin/content/list_faqs');

    }

    public function list_tickers() {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "Tickers List";

        $head['styles'] = array('//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css',);
        $head['scripts'] = array('//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js',);

        $data['tickers'] = $tickers = $this->Tickers->get(array('field'=>'content','dir'=>'asc'));

        $sortOrder = $this->Misc->convert_to_array($tickers);
        $sortOrder = $this->Misc->multi_sort_array($sortOrder, "sort_order", SORT_ASC);
        $data['sortOrder'] = $this->Misc->convert_to_object($sortOrder);

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/content/list_tickers',$data);
        $this->load->view(ADMIN_THEME.'/footer');


    }

    public function edit_ticker($tickerID) {
        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['scripts'] = array();

        if ($tickerID == "") {
            $ticker = array();
            $pageTitle = "Create New Ticker";
        } else {
            $ticker = $this->Tickers->get_by_id($tickerID);
            $pageTitle = "Edit Ticker";
        }

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('content', 'Content', 'required|xss_clean');
		$this->form_validation->set_rules('link', 'Link', 'xss_clean');

        if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();
            $post['timestamp'] = time();

            $next = $this->Tickers->get_next_num();

            $post['sort_order'] = $post['sort_order'] == 0 ? $next : $post['sort_order'];

            $this->Tickers->add_update($post, $post['cid']);

            $this->Alerts->set('success','Ticker has been saved.');
            redirect('admin/content/list_tickers');
        }

        $data['ticker'] = $ticker;
        $data['tickerID'] = $tickerID;
        $head['pageTitle'] = $data['pageTitle'] = $pageTitle;

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/content/edit_ticker',$data);
        $this->load->view(ADMIN_THEME.'/footer');


    }

    public function save_ticker_sort_order() {
        $post = $_POST;

        $tickers = json_decode($_POST['sort_order'],true);

        $sort = 0;
        foreach ($tickers as $ticker) {
            $sort++;

            $update = array(
                'sort_order'=>$sort
            );

            $this->db->where('cid',$ticker['id']);
            $this->db->update('ticker',$update);
        }

        echo "saved";
    }

    public function delete_ticker($tickerID) {
        $ticker = $this->Tickers->get_by_id($tickerID);

        $this->Tickers->delete($tickerID);

        $this->Alerts->set('success','Ticker has been removed.');
        redirect('admin/content/list_tickers');
    }

    public function list_slides() {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "Lobby Slider";

        $data['slides'] = $this->Sliders->get(array('field'=>'cid','dir'=>'asc'));

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/content/list_slides',$data);
        $this->load->view(ADMIN_THEME.'/footer');

    }

    public function edit_slide($slideID) {
        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['scripts'] = array();

        if ($slideID == "") {
            $slide = array();
            $pageTitle = "Create New Slide";
        } else {
            $slide = $this->Sliders->get_by_id($slideID);
            $pageTitle = "Edit Slide";
        }

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('link', 'Link', 'trim|required');

        if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();

            if(!empty($_FILES['upload']['name'])){
                $filename = strtolower(str_replace(" ", "_", $_FILES['upload']['name']));
                $config['upload_path']          = './uploads/slides/';
                $config['file_name']          =  $filename;
                $config['allowed_types']        = 'jpg|png';
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('upload')){
                    $error = array('error' => $this->upload->display_errors());
                    $this->Alerts->set('error',$this->upload->display_errors());
                    redirect('admin/content/edit_slide/'.$post['cid']);
                } else {
                    $uploadData = array('upload_data' => $this->upload->data());

                    $filename = $uploadData['upload_data']['file_name'];
                }
                $post['image'] = $filename;
            }

            $post['new_window'] = isset($post['new_window']) ? $post['new_window'] : 0;
            $post['show_slide'] = isset($post['show_slide']) ? $post['show_slide'] : 0;
            $post['timestamp'] = time();

            $next = $this->Sliders->get_next_num();

            $post['sort_order'] = $post['sort_order'] == 0 ? $next : $post['sort_order'];

            $this->Sliders->add_update($post, $post['cid']);

            $this->Alerts->set('success','Slide has been saved.');
            redirect('admin/content/list_slides');
        }

        $data['slide'] = $slide;
        $data['slideID'] = $slideID;
        $head['pageTitle'] = $data['pageTitle'] = $pageTitle;

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/content/edit_slide',$data);
        $this->load->view(ADMIN_THEME.'/footer');
    }

    public function delete_slide($slideID) {
        $slide = $this->Sliders->get_by_id($slideID);

        //delete files
        @unlink('./uploads/slides/'.$slide->image);

        $this->Sliders->delete($slideID);

        $this->Alerts->set('success','Slide has been removed.');
        redirect('admin/content/list_slides');
    }
}
