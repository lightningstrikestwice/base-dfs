<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index(){
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = $data['pageTitle'] = "Site Settings";

        $orderBy = array('field'=>'group_id','dir'=>'ASC');
        $where = array('editable'=>1);
        $data['settings'] = $settings = $this->Options->get($orderBy,$where);

		foreach($settings as $setting){
			$this->form_validation->set_rules($setting->name, $setting->nice_name, $setting->rules);
		}

        if ($this->form_validation->run() == FALSE){
		} else {
            $post = $this->input->post();

			foreach($settings as $setting){
				$update=array(
					'value' => $post[$setting->name],
				);

                $this->Options->add_update($update,$setting->name);
			}

            $this->Alerts->set('success','Settings have been saved!');
            redirect('admin/setting/');
		}

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/settings/settings',$data);
        $this->load->view(ADMIN_THEME.'/footer');
	}

	public function mail_chimp(){
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = $data['pageTitle'] = "Mailchimp Settings";

        $data['settings'] = $settings = $this->Options->get('',array('group_id'=>6));

		foreach($settings as $setting){
			$this->form_validation->set_rules($setting->name, $setting->nice_name, $setting->rules);

            if ($setting->name == "mc_active_lists"){
                $activeLists = json_decode($setting->value,true);
            }
        }

        $data['activeLists'] = $activeLists;

        if ($this->form_validation->run() == FALSE){
		} else {
            $post = $this->input->post();


            foreach ($post as $field=>$val) {
                if (strpos($field, "mc_") > -1) {
                    $update = array('value' => $val);
                    $this->Options->add_update($update,$field);
                }
            }

            $main = array();
            $num = count($post['list_id']);

            for($x=0; $x<=$num; $x++) {
                if (!empty($post['list_id'][$x])) {
                    $main[$post['list_id'][$x]] = $post['list_name'][$x];
                }
            }

            $active = json_encode($main);

            $update = array('value'=>$active);
            $this->Options->add_update($update,"mc_active_lists");

            $this->Alerts->set('success','Mailchimp Settings have been saved!');
            redirect('admin/setting/mail_chimp');
		}

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/settings/mail_chimp',$data);
        $this->load->view(ADMIN_THEME.'/footer');
	}

}
