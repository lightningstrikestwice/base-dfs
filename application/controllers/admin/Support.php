<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->load->model('Support_tickets');
	}

	public function index(){
        redirect('admin/support/browse');
	}

    public function browse($show="all") {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = $data['pageTitle'] = "Browse Support Tickets";

        $data['statuses'] = $statuses = $this->config->item('support_statuses');
        $data['admins'] = $this->Users->get_admins();
        $data['tickets'] = $tickets = $this->Support_tickets->get_list($show);
        $data['show'] = $show;

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/support/list_tickets',$data);
        $this->load->view(ADMIN_THEME.'/footer');

    }

    public function view_ticket($ticketID) {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = $data['pageTitle'] = "View Support Ticket";

        $data['statuses'] = $statuses = $this->config->item('support_statuses');
        $data['admins'] = $admins = $this->Users->get_admins();

        $userID = $this->Users->id();

		// set validation rules
		$this->form_validation->set_rules('status', 'Ticket Status', 'required|numeric');
		$this->form_validation->set_rules('assigned_user_id', 'Ticket Assigned To', 'required|numeric');

        if ($this->form_validation->run() === false) {
		} else {
            $thisTicket = $this->Support_tickets->get_by_id($ticketID);

            $post = $this->input->post();
            $this->Support_tickets->add_update($post,$ticketID);

            if ($post['status'] != $thisTicket->status) {
                $note = "Setting ticket status to ".$statuses[$post['status']]['status'];
                $this->Support_tickets->add_note($ticketID, $userID, $note);
            }

            if ($post['assigned_user_id'] != $thisTicket->assigned_user_id) {
                $note = "Assigning ticket to ".$admins[$post['assigned_user_id']]->username;
                $this->Support_tickets->add_note($ticketID, $userID, $note);
            }

            $this->Alerts->set('success','Ticket has been updated');
            redirect('admin/support/view_ticket/'.$ticketID);
        }


        $data['ticket'] = $ticket = $this->Support_tickets->get_by_id($ticketID);
        $data['notes'] = $notes = $this->Support_tickets->get_notes($ticketID);

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/support/view_ticket',$data);
        $this->load->view(ADMIN_THEME.'/footer');
    }

    public function complete_ticket($ticketID,$filter) {
        $update = array("status"=>3);

        $this->db->where('cid',$ticketID);
        $this->db->update('support_ticket',$update);

        $this->Alerts->set('success','Ticket has been completed.');
        redirect('admin/support/browse/'.$filter);
    }

    public function delete_ticket($ticketID) {
        $this->Support_tickets->delete($ticketID);

        $this->Alerts->set('success','Ticket has been removed.');
        redirect('admin/support/browse/');
    }

    public function add_note() {
        $userID = $this->Users->id();
        $post = $this->input->post();

        $this->Support_tickets->add_note($post['ticket_id'], $userID, $post['message']);
        $this->Alerts->set('success','A note has been added to this ticket');
        redirect('admin/support/view_ticket/'.$post['ticket_id']);

    }
}
