<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_info extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->load->model('Products');
        $this->load->model('Payments');
	}

	public function index(){

	}

    public function list_products() {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "Products";

        $data['products'] = $products = $this->Products->get(array('field'=>'price','dir'=>'asc'));

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/payment_info/list_products',$data);
        $this->load->view(ADMIN_THEME.'/footer');

    }

    public function edit_product($prodID) {
        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        if ($prodID == "") {
            $product = array();
            $pageTitle = "Create New Product";
        } else {
            $product = $this->Products->get_by_id($prodID);
            $pageTitle = "Edit ".$product->name;
        }

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		$this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();

            $this->Products->add_update($post, $post['cid']);

            $this->Alerts->set('success',$post['name'].' has been saved.');
            redirect('admin/payment_info/list_products');
        }

        $data['product'] = $product;
        $data['productID'] = $prodID;
        $head['pageTitle'] = $data['pageTitle'] = $pageTitle;

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/payment_info/edit_product',$data);
        $this->load->view(ADMIN_THEME.'/footer');


    }

    public function delete_product($prodID) {
        $product = $this->Products->get_by_id($prodID);

        $this->Products->delete($prodID);

        $this->Alerts->set('success', $product->name . ' has been removed.');
        redirect('admin/payment_info/list_products');
    }

    public function view_log($timeWindow='ALL') {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "Payment Log";


		if($timeWindow != 'ALL') {
			if ($timeWindow == 'year') { $when = strtotime('-1 year'); }
			elseif ($timeWindow == 'month') { $when = strtotime('-1 month'); }
			elseif ($timeWindow =='week') { $when = strtotime('-7 days'); }
			elseif ($timeWindow =='3') { $when = strtotime('-3 days'); }
			elseif ($timeWindow =='1') { $when = strtotime('-1 days'); }
			else { $when = strtotime('-3 days'); }
		} else {
			$when = '';
		}

        $data['payments'] = $this->Payments->get_all_payments($when);
        $data['timeWindow'] = $timeWindow;
        $data['timeWindows'] = $this->config->item('timeWindows');

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/payment_info/payment_log',$data);
        $this->load->view(ADMIN_THEME.'/footer');
    }

    public function transaction_log() {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "Transaction Log";

        $data['transactions'] = $this->User_funds->get_transactions();

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/payment_info/transaction_log',$data);
        $this->load->view(ADMIN_THEME.'/footer');
    }

    public function withdrawal_requests() {

    }

    public function ytd_earnings() {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "YTD Earnings";

        $data['transactions'] = $this->User_funds->get_transactions();

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/payment_info/ytd_earnings',$data);
        $this->load->view(ADMIN_THEME.'/footer');
    }
}
