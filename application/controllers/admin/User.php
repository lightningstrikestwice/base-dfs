<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	public function index(){
        redirect('admin/user/list_all');
	}

    public function list_all($alpha='') {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "User List";


        $alphaString = $alpha != '' ? "username LIKE '".$alpha."%'" : "";

        $data['users'] = $users = $this->Users->get('',$alphaString);
        $data['alpha'] = $alpha;

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/users/list_all',$data);
        $this->load->view(ADMIN_THEME.'/footer');
    }

    public function user_log($userID) {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = "User Log";

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/users/user_log',$data);
        $this->load->view(ADMIN_THEME.'/footer');
    }

    public function edit_user($userID) {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $genOptions = $this->Options->get_group(3);
        $generalOptions = $this->Options->get_group(4);

        $head['pageTitle'] = "Edit User";

        $data['states'] = $this->Misc->province_list();
        $data['months'] = $this->Misc->months();
        $data['sources'] = $this->Misc->sources();

        $countries = $this->Misc->countries_list();
        foreach ($countries as $a=>$c) {
            if (!in_array($a,json_decode($generalOptions['allowed_countries'],true))) {
                unset($countries[$a]);
            }
        }
        $data['countries'] = $countries;

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('email', 'Email', 'strip_tags|trim|required|valid_email|max_length[256]|min_length[4]|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'strip_tags|trim|required|max_length[64]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'strip_tags|trim|required|max_length[64]|xss_clean');
        $this->form_validation->set_rules('state', 'Province', 'strip_tags|required|trim|xss_clean');
		$this->form_validation->set_rules('country', 'Country', 'strip_tags|required|trim|xss_clean');

        if (strlen($_POST['password']) > 0) {
            $this->form_validation->set_rules('password', 'Password', 'required|xss_clean|max_length['.$genOptions['max_password_length'].']|min_length['.$genOptions['min_password_length'].']');
        }

        if ($_POST['username'] != $_POST['existing_username']){
    		$this->form_validation->set_rules('username', 'Username', 'strip_tags|trim|required|max_length[14]|min_length[2]|xss_clean|alpha_dash|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
        }

        if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();
            $userID = $post['cid'];

            unset($post['existing_username']);
            unset($post['cid']);

            $post['dob'] = json_encode($post['dob']);

            if (strlen($post['password'])){
                $post['password'] = $this->Users->hash_password($post['password']);
            }

            $this->Users->add_update($post,$userID);

            $this->Alerts->set('success','User Information has been saved');
            redirect('admin/user/edit_user/'.$userID);
        }

        $data['user'] = $this->Users->get_by_id($userID);

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/users/edit_user',$data);
        $this->load->view(ADMIN_THEME.'/footer');
    }

    public function edit_funds($userID) {
        $head = $data = array();

        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $data['generalOptions'] = $this->Options->get_group(4);

        $head['pageTitle'] = $data['title'] = "Edit User Funds";


        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('debitcredit', 'Action', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|numeric');
		$this->form_validation->set_rules('message', 'Message', 'trim|xss_clean');

        if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();

			$ticket_value = $amount = $bonus_amount = $tourney_amount = $ticket_amount = $ticket_type = 0;

            $debitCredit = $post['debitcredit'];
            $type = $post['type'];

			switch($type) {
				case 0:
                    $amount = $post['amount'];
                	break;
				case 1:
                    $bonus_amount = $post['amount'];
            		break;
				case 2:
					$ticket_amount = $post['amount'];;
					$ticket_type = $post['ticket_type'];
					for($i=0; $i<$post['amount']; $i++) {
						//add or remove a ticket
						if($debitCredit == 1) {
							$ticket_type = $post['ticket_type'];
							$insert = array(
									'user_id' => $userID,
									'ticket_type' => $ticket_type,
									'ticket_value' => 0,
									'issued' => time()
								);
							$this->db->insert('user_tickets',$insert);

						} else {
							//find oldest ticket and redeem it, also find out what it's worth
							$this->db->where('user_id', $userID);
							$this->db->where('redeemed',0);
							$this->db->where('ticket_type', $post['ticket_type']);
							$this->db->order_by('issued','asc');
							$this->db->limit(1);

							$ticket = $this->db->get('user_tickets')->row();

							$ticket_value = $ticket_value + $ticket->ticket_value;

							$this->db->where('id',$ticket->id);
							$this->db->update('user_tickets',array('redeemed'=>1,'redeemed_time'=>time()));
						}
					}
        			break;
				case 3:
					$tourney_amount = $post['amount'];
    				break;
			}

			$message = $post['message'];

			if($type == 2) {
				$comment = $debitCredit == 1 ? "Tickets added to account by Administrator" : "Tickets removed from account by Administrator";
			} else {
				$comment = $debitCredit == 1 ? "Funds added to account by Administrator" : "Funds removed from account by Administrator";
			}
            $comment = !empty($message) ? $comment.": ".$message : $comment;

			$money = $type == 0 ? $amount : 0;
			$points = $type == 1 ? $amount : 0;
			$tickets = $type == 2 ? $amount : 0;
            $tourney = $type == 3 ? $amount : 0;

            $ticket_type = $tickets > 0 ? $post['ticket_type'] : 0;

			if(isset($post['paid']) && $post['paid'] == 1){
				$this->User_funds->insert_payment($userID, $debitCredit, $amount);
			}

            $this->User_funds->add_transaction($userID, 8, $debitCredit, $money, $comment, 0, $points,0,0,$tickets,$ticket_type,$ticket_value, $tourney);
			$this->User_funds->update_balance($userID, $amount, $debitCredit, $bonus_amount, $ticket_amount, $ticket_type, $tourney_amount);

			if($type == 2) {
                $alertMsg = "User's Tickets Updated";
			} else {
                $alertMsg = "User's Funds Updated";
			}

            $this->Alerts->set('success',$alertMsg);
            redirect('admin/user/edit_funds/'.$userID);
        }

        $data['user'] = $this->Users->get_by_id($userID);
        $data['userBalance'] = $userBalance = $this->User_funds->user_balance($userID)['balance'];

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/users/edit_user_funds',$data);
        $this->load->view(ADMIN_THEME.'/footer');

    }
}
