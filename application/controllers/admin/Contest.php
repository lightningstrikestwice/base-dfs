<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contest extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->form_validation->set_error_delimiters('<p class="alert alert-danger p-1 mt-1">', '</p>');
	}

	public function index(){
	}

    public function featured_contest($templateID=0) {
        $head['scripts'] = array('https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js');
        $head['styles'] = array('https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css');

        $siteOptions = $this->Options->get_group(1);
        $genOptions = $this->Options->get_group(3);

        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $head['pageTitle'] = $data['pageTitle'] = "Featured Contest Generator";

        $data['gameTypes'] = $this->config->item('game_types');
        $data['salaryTypes'] = $this->config->item('salary_cap_options');
        $data['durations'] = $this->config->item('durations');

        $data['sports'] = json_decode($genOptions['active_sports'],true);

        $data['templates'] = $this->Leagues->get_templates();

        if ($templateID != 0) {
            $data['template'] = $this->Leagues->get_template_by_id($templateID);
        }

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('type', 'Contest Type', 'required|numeric',array('numeric'=>'Please select a valid Contest Type'));
		$this->form_validation->set_rules('sport[]', 'Sport', 'required');
		$this->form_validation->set_rules('duration', 'Contest Duration', 'required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required');
		$this->form_validation->set_rules('size_val', '# of Entries', 'required|numeric|greater_than[1]');
		$this->form_validation->set_rules('multi_entry_limit', 'Entry Limit', 'required|numeric|greater_than[0]');
		$this->form_validation->set_rules('entry_fee', 'Buy In', 'required|numeric|greater_than_equal_to[0]');
		$this->form_validation->set_rules('prize', 'Prize', 'required');
		$this->form_validation->set_rules('name', 'Contest Name', 'required');

        if (isset($_POST['extreme'])) {
            $this->form_validation->set_rules('extreme_spread', 'Point Spread', 'required|numeric');
        }

        if ($this->form_validation->run() === false) {
		} else {
            $post = $this->input->post();
            $info = $template = array();

            $multi = $post['multi_entry_limit'] > 1 ? 1 : 0;
            $guaranteed = isset($post['guaranteed']) ? $post['guaranteed'] : 0;
            $featured = isset($post['featured']) ? $post['featured'] : 0;
            $firstGameCutoff = strtotime($post['start_date']);

            $info = array(
                "cid" => $post['cid'],
                "name" => $post['name'],
                "size" => $post['size_val'],
                "sport" => json_encode($post['sport']),
                "created_date" => time(),
                "duration" => $post['duration'],
                "type" => $post['type'],
                "prize_structure" => 1,
                "entry_fee" => $post['entry_fee'],
                "total_prize" => $post['prize'],
                "multi_entry" => $multi,
                "multi_entry_limit" => $post['multi_entry_limit'],
                "guaranteed" => $guaranteed,
                "feat" => $featured,
                "start_date" => strtotime($post['start_date']),
                "first_game_cutoff" => $firstGameCutoff,
                "status" => 1,
                "active" => 1,
                "finalized" => 0,
            );

            if (isset($post['extreme'])){
                $info['extreme'] = $post['extreme'];
                $info['extreme_spread'] = $post['extreme_spread'];
            }

            $this->Leagues->add_update($info,$info['cid']);

            if (isset($post['createTemplate'])){
                $template = array(
                    "size" => $post['size_val'],
                    "sport" => json_encode($post['sport']),
                    "type" => $post['type'],
                    "prize_structure" => 1,
                    "entry_fee" => $post['entry_fee'],
                    "total_prize" => $post['prize'],
                    "multi_entry" => $multi,
                    "multi_entry_limit" => $post['multi_entry_limit'],
                    "guaranteed" => $guaranteed,
                    "feat" => $featured,
                    "duration" => $post['duration'],
                );

                if (isset($post['extreme'])){
                    $template['extreme'] = $post['extreme'];
                    $template['extreme_spread'] = $post['extreme_spread'];
                }

                $this->Leagues->add_update_template($template);
            }

            $this->Alerts->set('success', 'Your contest has been created');
            redirect('admin/contest/featured_contest');
        }
        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/contest/featured_contest',$data);
        $this->load->view(ADMIN_THEME.'/footer');
    }

    public function show_sport_list($contestType, $selSports="") {
        $gameTypes = $this->config->item('game_type_data');
        $data['fieldType'] = $contestType == 15 ? "checkbox" : "radio";
        $data['sports'] = $sports = $gameTypes[$contestType]['sports'];


        $data['selSports'] = $selSports == "" ? array() : explode("_", $selSports);

        $this->load->view(ADMIN_THEME.'/contest/sportList',$data);
    }

    public function show_duration_list($sports,$contestType, $selDuration="") {
        $gameTypes = $this->config->item('game_type_data');

        $sportList = explode("_", $sports);
        $durationList = $gameTypes[$contestType]['timeframe'];

        $data['selDuration'] = $selDuration;

        $durations = array();
        foreach ($durationList as $k=>$dur) {
            if (!is_array($dur)){
                $durations[$k] = $dur;
            } else {
                $d = count($dur);

                $in = 0;
                foreach ($dur as $sport) {
                    if (in_array($sport, $sportList)) {
                        $in = 1;
                    } else {
                        continue;
                    }
                }

                if ($d == count($sportList) && $in == 1) {
                    switch ($k) {
                        case "weekly":  $desc = "Weekly Contest";   break;
                        case "single":  $desc = "Single Event"; break;
                    }
                    $durations[$k] = $desc;
                }
            }
        }


        $data['durationList'] = $durations;
        $this->load->view(ADMIN_THEME.'/contest/durationList',$data);
    }

    public function get_start_list($duration) {

    }

    public function delete_template($ID) {
        $this->Leagues->delete_template($ID);

        $this->Alerts->set('success',$page->slug.' Template has been removed.');
        redirect('admin/contest/featured_contest');
    }

    public function manage($type="upcoming") {
        $head['scripts'] = array('https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js');
        $head['styles'] = array('https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css');

        $siteOptions = $this->Options->get_group(1);
        $genOptions = $this->Options->get_group(3);

        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;
        $head['genOptions'] = $data['genOptions'] = $genOptions;
        $head['pageTitle'] = "Manage Contests";

        $data['leagues'] = $leagues = $this->Leagues->get_manage_leagues($type);

        $data['types'] = $this->config->item('game_types');
        $data['durations'] = $this->config->item('durations');
        $data['type'] = $type;

        $this->load->view(ADMIN_THEME.'/header',$head);
        $this->load->view(ADMIN_THEME.'/contest/manage',$data);
        $this->load->view(ADMIN_THEME.'/footer');
    }

	public function remove_game($leagueID=""){
		if(!empty($leagueID)){
			$this->load->model('user_funds');

            $league=$this->leagues->get_by_id($leagueID);

            //Send out emails & refund people
			$this->db->where('league_id',$leagueID);
			$teams = $this->db->get('league_team');
			if($teams->num_rows()>0){
				foreach($teams->result() as $t){
					//find out if we're refunding money or tickets
					if($league->entry_fee_type == 1) {
						//money entry so carry on as normal

						if($league->entry_fee>0){

							//give them back their money
							//$this->user_funds->add_transaction($t->user_id, 7, 0, $league->entry_fee, 'Fee reversal from match removal: '.$league->name, $league->cid);
							//$this->user_funds->update_balance($t->user_id,$league->entry_fee,1);
							$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
							$counter=0;
							$bonuscounter=0;
							foreach($locked_funds as $l){
								//only reverse one entry in case of multi-entry
								if($l->amount>0 && $counter == 0){
									$this->user_funds->add_transaction($t->user_id, 7, 1, $l->amount, 'Fee reversal from match removal: '.$league->name, $league->cid,0);
									$this->user_funds->update_balance($t->user_id,$l->amount, 1,0);
									$counter++;
								}
								if($l->bonus_amount>0 && $bonuscounter == 0){
									$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Bonus Fee reversal from match removal: '.$league->name, $league->cid,$l->bonus_amount);
									$this->user_funds->update_balance($t->user_id,0, 1,$l->bonus_amount);
									$bonuscounter++;
								}


							}
						}

						$emaildata['league']=$league;
						$emaildata['user']=$this->users->get_by_id($t->user_id);
						$this->users->send_email($t->user_id,'game_removed_by_admin',$emaildata);

					} elseif($league->entry_fee_type == 2) {
						//refund ticket
						$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
						$counter=0;
						$bonuscounter=0;
						foreach($locked_funds as $l){
							//only reverse one entry in case of multi-entry
							if($l->ticket_amount>0 && $counter == 0){
								//give this user back a ticket
								$insert = array(
											'user_id'=>$t->user_id,
											'ticket_type'=>$league->entry_ticket_type,
											'ticket_value'=>$l->ticket_value,
											'issued'=>time()
										);
								$this->db->insert('user_tickets',$insert);

								$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Ticket reversal from match removal: '.$league->name, $league->cid,0,0,0,1,$league->entry_ticket_type,$l->ticket_value);
								$this->user_funds->update_balance($t->user_id,0,1,0,1,$league->entry_ticket_type);
								$counter++;
							}
						}
						$emaildata['league']=$league;
						$emaildata['user']=$this->users->get_by_id($t->user_id);
						$this->users->send_email($t->user_id,'game_removed_by_admin',$emaildata);

					} elseif($league->entry_fee_type == 3) {
						//could have been either so look at the transaction log
						$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
						$counter=0;
						$bonuscounter=0;
						$ticketcounter=0;
						foreach($locked_funds as $l){
							//only reverse one entry in case of multi-entry
							if($l->amount>0 && $counter == 0){
								$this->user_funds->add_transaction($t->user_id, 7, 1, $l->amount, 'Fee reversal from match removal: '.$league->name, $league->cid,0);
								$this->user_funds->update_balance($t->user_id,$l->amount, 1,0);
								$counter++;
							}
							if($l->bonus_amount>0 && $bonuscounter == 0){
								$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Bonus Fee reversal from match removal: '.$league->name, $league->cid,$l->bonus_amount);
								$this->user_funds->update_balance($t->user_id,0, 1,$l->bonus_amount);
								$bonuscounter++;
							}
							if($l->ticket_amount>0 && $ticketcounter == 0){
									//give this user back a ticket
									$insert = array(
												'user_id'=>$t->user_id,
												'ticket_type'=>$league->entry_ticket_type,
												'ticket_value'=>$l->ticket_value,
												'issued'=>time()
											);
									$this->db->insert('user_tickets',$insert);

									$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Ticket reversal from match removal: '.$league->name, $league->cid,0,0,0,1,$league->entry_ticket_type,$l->ticket_value);
									$this->user_funds->update_balance($t->user_id,0,1,0,1,$league->entry_ticket_type);
									$ticketcounter++;
							}
						}
						$emaildata['league']=$league;
						$emaildata['user']=$this->users->get_by_id($t->user_id);
						$this->users->send_email($t->user_id,'game_removed_by_admin',$emaildata);

					} elseif($league->entry_fee_type == 4) {
						if($league->entry_tourney_dollars>0){
							//give them back their money
							//$this->user_funds->add_transaction($t->user_id, 7, 0, $league->entry_fee, 'Fee reversal from match removal: '.$league->name, $league->cid);
							//$this->user_funds->update_balance($t->user_id,$league->entry_fee,1);
							$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
							$counter=0;

							foreach($locked_funds as $l){
								//only reverse one entry in case of multi-entry
								if($l->tourney_amount>0 && $counter == 0){
									$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Fee reversal from match removal: '.$league->name, $league->cid,0,'',0,0,0,0,$l->tourney_amount);
									$this->user_funds->update_balance($t->user_id,0, 1,0,0,0,$l->tourney_amount);
									$counter++;
								}

							}
						}

						$emaildata['league']=$league;
						$emaildata['user']=$this->users->get_by_id($t->user_id);
						$this->users->send_email($t->user_id,'user_removed_from_game_by_admin',$emaildata);

						//Now delete the team
						$this->db->where('id',$team_id);
						$this->db->delete('league_team');

						echo json_encode(array("status"=>"tiptop"));

					} elseif($league->entry_fee_type == 5) {
						//could have been either $$ or tourney dollars so check
						$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
						$counter=0;
						$bonuscounter=0;
						$tourneycounter=0;
						foreach($locked_funds as $l){

							if($l->amount>0 && $counter == 0){
								$this->user_funds->add_transaction($t->user_id, 7, 1, $l->amount, 'Fee reversal from match removal: '.$league->name, $league->cid,0);
								$this->user_funds->update_balance($t->user_id,$l->amount, 1,0);
								$counter++;
							}
							if($l->bonus_amount>0 && $bonuscounter == 0){
								$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Bonus Fee reversal from match removal: '.$league->name, $league->cid,$l->bonus_amount);
								$this->user_funds->update_balance($t->user_id,0, 1,$l->bonus_amount);
								$bonuscounter++;
							}
							if($l->tourney_amount>0 && $tourneycounter == 0){
								$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Fee reversal from match removal: '.$league->name, $league->cid,0,'',0,0,0,0,$l->tourney_amount);
								$this->user_funds->update_balance($t->user_id,0, 1,0,0,0,$l->tourney_amount);
								$tourneycounter++;
							}

						}
						$emaildata['league']=$league;
						$emaildata['user']=$this->users->get_by_id($t->user_id);

						$this->users->send_email($t->user_id,'user_removed_from_game_by_admin',$emaildata);

						//Now delete the team
						$this->db->where('id',$team_id);
						$this->db->delete('league_team');
						echo json_encode(array("status"=>"tiptop"));


					}

					/*if($league->entry_fee>0){
						$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
						$counter=0;
						$bonuscounter=0;
						foreach($locked_funds as $l){
							//only reverse one entry in case of multi-entry
							if($l->amount>0 && $counter == 0){
								$this->user_funds->add_transaction($t->user_id, 7, 1, $l->amount, 'Fee reversal from match removal: '.$league->name, $league->cid,0);
								$this->user_funds->update_balance($t->user_id,$l->amount, 1,0);
								$counter++;
							}
							if($l->bonus_amount>0 && $bonuscounter == 0){
								$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Bonus Fee reversal from match removal: '.$league->name, $league->cid,$l->bonus_amount);
								$this->user_funds->update_balance($t->user_id,0, 1,$l->bonus_amount);
								$bonuscounter++;
							}

						}
					}
					//echo "<pre>".print_r($league,true)."</pre>";exit;
					$emaildata['league']=$league;
					$emaildata['user']=$this->users->get_by_id($t->user_id);
					$this->users->send_email($t->user_id,'game_removed_by_admin',$emaildata);*/

                    // SENDING EMAILS
//                    if ($this->options->get('send_emails') == 1) {
//                        $userID = $t->user_id;
//                        $user = $this->users->get_by_id($userID);
//
//                        $settings = $this->users->get_email_settings($userID);
//                        if (in_array("game_removal",$settings)){
//                        $email_data = array(
//
//                        );
//                        $subject = "DraftBattles - Contest Removed";
//                        $this->emails->send_mail($user->email, 'game_removed', $email_data,$subject);
//
//                        }
//                    }

				}
				//Now delete the teams
				$this->db->where('league_id',$leagueID);
				$this->db->delete('league_team');
			}
			//Finally delete the league
			$this->db->where('cid',$leagueID);
			$this->db->delete('league');

		}
        $this->Alerts->set('success', $league->name.' has been removed.  Players have been refunded appropriate funds.');
		redirect("admin/contest/manage/upcoming");
	}

	function remove_player($team_id=""){
		if(!empty($team_id)){
			$this->load->model('user_funds');
			$this->load->model('leagues');

			//Send out emails & refund people
			$this->db->where('id',$team_id);
			$this->db->limit(1);
			$team=$this->db->get('league_team');
			if($team->num_rows()>0){
				$t=$team->row();
				$league=$this->leagues->get_by_id($t->league_id);

				//find out if we're refunding money or tickets
				if($league->entry_fee_type == 1) {
					//money entry so carry on as normal

					if($league->entry_fee>0){
						//give them back their money
						//$this->user_funds->add_transaction($t->user_id, 7, 0, $league->entry_fee, 'Fee reversal from match removal: '.$league->name, $league->cid);
						//$this->user_funds->update_balance($t->user_id,$league->entry_fee,1);
						$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
						$counter=0;
						$bonuscounter=0;
						foreach($locked_funds as $l){
							//only reverse one entry in case of multi-entry
							if($l->amount>0 && $counter == 0){
								$this->user_funds->add_transaction($t->user_id, 7, 1, $l->amount, 'Fee reversal from match removal: '.$league->name, $league->cid,0);
								$this->user_funds->update_balance($t->user_id,$l->amount, 1,0);
								$counter++;
							}
							if($l->bonus_amount>0 && $bonuscounter == 0){
								$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Bonus Fee reversal from match removal: '.$league->name, $league->cid,$l->bonus_amount);
								$this->user_funds->update_balance($t->user_id,0, 1,$l->bonus_amount);
								$bonuscounter++;
							}


						}
					}

					$emaildata['league']=$league;
					$emaildata['user']=$this->users->get_by_id($t->user_id);
					$this->users->send_email($t->user_id,'user_removed_from_game_by_admin',$emaildata);

					//Now delete the team
					$this->db->where('id',$team_id);
					$this->db->delete('league_team');

					echo json_encode(array("status"=>"tiptop"));
				} elseif($league->entry_fee_type == 2) {
					//refund ticket
					$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
					$counter=0;
					$bonuscounter=0;
					foreach($locked_funds as $l){
						//only reverse one entry in case of multi-entry
						if($l->ticket_amount>0 && $counter == 0){
							//give this user back a ticket
							$insert = array(
										'user_id'=>$t->user_id,
										'ticket_type'=>$league->entry_ticket_type,
										'ticket_value'=>$l->ticket_value,
										'issued'=>time()
									);
							$this->db->insert('user_tickets',$insert);

							$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Ticket reversal from match removal: '.$league->name, $league->cid,0,0,0,1,$league->entry_ticket_type,$l->ticket_value);
							$this->user_funds->update_balance($t->user_id,0,1,0,1,$league->entry_ticket_type);
							$counter++;
						}
					}
					$emaildata['league']=$league;
					$emaildata['user']=$this->users->get_by_id($t->user_id);
					$this->users->send_email($t->user_id,'user_removed_from_game_by_admin',$emaildata);

					//Now delete the team
					$this->db->where('id',$team_id);
					$this->db->delete('league_team');
					echo json_encode(array("status"=>"tiptop"));
				} elseif($league->entry_fee_type == 3) {
					//could have been either so look at the transaction log
					$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
					$counter=0;
					$bonuscounter=0;
					$ticketcounter=0;
					foreach($locked_funds as $l){

						if($l->amount>0 && $counter == 0){
							$this->user_funds->add_transaction($t->user_id, 7, 1, $l->amount, 'Fee reversal from match removal: '.$league->name, $league->cid,0);
							$this->user_funds->update_balance($t->user_id,$l->amount, 1,0);
							$counter++;
						}
						if($l->bonus_amount>0 && $bonuscounter == 0){
							$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Bonus Fee reversal from match removal: '.$league->name, $league->cid,$l->bonus_amount);
							$this->user_funds->update_balance($t->user_id,0, 1,$l->bonus_amount);
							$bonuscounter++;
						}
						if($l->ticket_amount>0 && $ticketcounter == 0){
								//give this user back a ticket
								$insert = array(
											'user_id'=>$t->user_id,
											'ticket_type'=>$league->entry_ticket_type,
											'ticket_value'=>$l->ticket_value,
											'issued'=>time()
										);
								$this->db->insert('user_tickets',$insert);

								$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Ticket reversal from match removal: '.$league->name, $league->cid,0,0,0,1,$league->entry_ticket_type,$l->ticket_value);
								$this->user_funds->update_balance($t->user_id,0,1,0,1,$league->entry_ticket_type);
								$ticketcounter++;
						}

					}
					$emaildata['league']=$league;
					$emaildata['user']=$this->users->get_by_id($t->user_id);
					$this->users->send_email($t->user_id,'user_removed_from_game_by_admin',$emaildata);

					//Now delete the team
					$this->db->where('id',$team_id);
					$this->db->delete('league_team');
					echo json_encode(array("status"=>"tiptop"));

				} elseif($league->entry_fee_type == 4) {
					if($league->entry_tourney_dollars>0){
						//give them back their money
						//$this->user_funds->add_transaction($t->user_id, 7, 0, $league->entry_fee, 'Fee reversal from match removal: '.$league->name, $league->cid);
						//$this->user_funds->update_balance($t->user_id,$league->entry_fee,1);
						$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
						$counter=0;

						foreach($locked_funds as $l){
							//only reverse one entry in case of multi-entry
							if($l->tourney_amount>0 && $counter == 0){
								$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Fee reversal from match removal: '.$league->name, $league->cid,0,'',0,0,0,0,$l->tourney_amount);
								$this->user_funds->update_balance($t->user_id,0, 1,0,0,0,$l->tourney_amount);
								$counter++;
							}

						}
					}

					$emaildata['league']=$league;
					$emaildata['user']=$this->users->get_by_id($t->user_id);
					$this->users->send_email($t->user_id,'user_removed_from_game_by_admin',$emaildata);

					//Now delete the team
					$this->db->where('id',$team_id);
					$this->db->delete('league_team');

					echo json_encode(array("status"=>"tiptop"));

				} elseif($league->entry_fee_type == 5) {
					//could have been either $$ or tourney dollars so check
					$locked_funds=$this->user_funds->get_locked_by_league_id($t->league_id,$t->user_id);
					$counter=0;
					$bonuscounter=0;
					$tourneycounter=0;
					foreach($locked_funds as $l){

						if($l->amount>0 && $counter == 0){
							$this->user_funds->add_transaction($t->user_id, 7, 1, $l->amount, 'Fee reversal from match removal: '.$league->name, $league->cid,0);
							$this->user_funds->update_balance($t->user_id,$l->amount, 1,0);
							$counter++;
						}
						if($l->bonus_amount>0 && $bonuscounter == 0){
							$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Bonus Fee reversal from match removal: '.$league->name, $league->cid,$l->bonus_amount);
							$this->user_funds->update_balance($t->user_id,0, 1,$l->bonus_amount);
							$bonuscounter++;
						}
						if($l->tourney_amount>0 && $tourneycounter == 0){
							$this->user_funds->add_transaction($t->user_id, 7, 1, 0, 'Fee reversal from match removal: '.$league->name, $league->cid,0,'',0,0,0,0,$l->tourney_amount);
							$this->user_funds->update_balance($t->user_id,0, 1,0,0,0,$l->tourney_amount);
							$tourneycounter++;
						}

					}
					$emaildata['league']=$league;
					$emaildata['user']=$this->users->get_by_id($t->user_id);
					$this->users->send_email($t->user_id,'user_removed_from_game_by_admin',$emaildata);

					//Now delete the team
					$this->db->where('id',$team_id);
					$this->db->delete('league_team');
					echo json_encode(array("status"=>"tiptop"));
				}
			}
		}else{
			echo json_encode(array("status"=>"bogus","message"=>"There was an error retrieving this team's data."));
		}
	}

    public function export_entrants($leagueID) {
        $this->db->select('users.username, users.email, users.first_name, users.last_name, users.email, users.address, users.address2, users.city, users.state, users.zip');
        $this->db->from('users');
        $this->db->join('league_team','users.cid=league_team.user_id');
        $this->db->where('league_team.league_id',$leagueID);
        $q = $this->db->get();
        $users = $q->result();

		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=league_entrants_".$leagueID.".csv");
		header("Pragma: no-cache");
		header("Expires: 0");

		echo '"Username","First","Last","Email","Address","Address2","City","Province","Zip"'.PHP_EOL;

		foreach($users as $user) {
			echo '"'.$user->username.'","'.$user->first_name.'","'.$user->last_name.'","'.$user->email.'","'.$user->address.'","'.$user->address2.'","'.$user->city.'","'.$user->state.'","'.$user->zip.'"'.PHP_EOL;
		}

    }
}
