<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->load->model('Data_feeds');
	}

	public function index(){

	}

    public function execInBackground($cmd) {
        if (substr(php_uname(), 0, 7) == "Windows"){
            pclose(popen("start /B ". $cmd, "r"));
             sleep(1);
        }
        else {
            exec($cmd . " > /dev/null &");
            sleep(1);
        }
    }

    public function initial_load_teams($sport="mlb") {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $this->output->enable_profiler(TRUE);

        $season = $this->Options->get_option('current_'.$sport.'_season');

        $url = "https://api.fantasydata.net/v3/".$sport."/scores/JSON/Standings/".$season;

        $teamData = $this->Data_feeds->get_api_data($url,$sport);

        $teamID_prefix = $this->config->item('sportid_'.$sport);

        $teams = array();

        // These fields will need to be updated based on the data provider and sport
        foreach ($teamData as $team) {
            $teamID = $teamID_prefix . $team->TeamID;

            if ($sport == "nfl") {
                $city = "";
                $name = $team->Name;
                $alias = $team->Team;
            } else {
                $city = $team->City;
                $name = $team->Name;
                $alias = $team->Key;
            }
            $teams[] = array(
                "cid" => $teamID,
                "name" => $name,
                "city" => $city,
                "alias" => $alias,
                "sport" => $sport,
            );
        }

        if (count($teams) > 0) {
            $this->db->insert_batch("sport_team",$teams);
        }
    }

    public function initial_load_schedule($sport="mlb") {
        $debug = false;

        $season = $this->Options->get_option('current_'.$sport.'_season');

        $url = "https://api.fantasydata.net/v3/".$sport."/scores/JSON/Games/".$season;

        $schedData = $this->Data_feeds->get_api_data($url,$sport);

        $schedule = array();

        foreach ($schedData as $game) {
            $homeID = $this->Teams->get_real_id($game->HomeTeamID,$sport);
            $visitingID = $this->Teams->get_real_id($game->AwayTeamID,$sport);

            $final = $postponed = $homeScore = $visitingScore = $winner = 0;

            if ($game->Status == "Final" && !empty($game->isClosed)) {
                $final = 1;
                $homeScore = $game->HomeTeamRuns;
                $awayScore = $game->AwayTeamRuns;
                $winner = $homeScore > $awayScore ? $homeID : $awayID;
            }

            if ($game->Status == "Postponed") {
                $postponed = 1;
            }
            $schedule[] = array(
                "cid" => $game->GameID,
                "gamestart" => strtotime($game->DateTime),
                "sport" => $sport,
                "season" => $game->Season,
                "home_team_id" => $homeID,
                "home_team_score" => $homeScore,
                "visiting_team_id" => $visitingID,
                "visiting_team_score" => $visitingScore,
                "game_type" => $game->SeasonType,
                "final" => $final,
                "postponed" => $postponed,
                "winner" => $winner,
            );
        }

        if (count($schedule) > 0 && !$debug) {
            $this->db->insert_batch("sport_event",$schedule);
        }
    }


    public function add_update_schedule_mlb() {
        $debug = false;

        $season = $this->Options->get_option('current_mlb_season');
        $sport = 'mlb';

        $url = "https://api.fantasydata.net/v3/".$sport."/scores/JSON/Games/".$season;

        $schedData = $this->Data_feeds->get_api_data($url,$sport);

        $mlbGames = $this->Events->get_event_array($sport,$season);

        foreach ($schedData as $game) {
            $test = "";
            $dbGame = $mlbGames[$game->GameID];
            $homeScore = empty($game->HomeTeamRuns) ? 0 : $game->HomeTeamRuns;
            $awayScore = empty($game->AwayTeamRuns) ? 0 : $game->AwayTeamRuns;

            // If Game is already final, no need to update
            if ($dbGame->final == 1 || $dbGame->postponed == 1) {continue;}

            // If nothing has changed with the game, no need to update
            if ($dbGame->gamestart == strtotime($game->DateTime) && $dbGame->home_team_score == $homeScore && $dbGame->visiting_team_score == $awayScore) {continue;}

            $homeID = $this->Teams->get_real_id($game->HomeTeamID,$sport);
            $visitingID = $this->Teams->get_real_id($game->AwayTeamID,$sport);

            $final = $postponed = $homeScore = $visitingScore = $winner = 0;

            if ($game->Status == "Final" && !empty($game->isClosed)) {
                $final = 1;
                $winner = $homeScore > $awayScore ? $homeID : $awayID;
            }

            if ($game->Status == "Postponed") {
                $postponed = 1;
            }
            $update = array(
                "gamestart" => strtotime($game->DateTime),
                "sport" => $sport,
                "season" => $game->Season,
                "home_team_id" => $homeID,
                "home_team_score" => $homeScore,
                "visiting_team_id" => $visitingID,
                "visiting_team_score" => $visitingScore,
                "game_type" => $game->SeasonType,
                "final" => $final,
                "postponed" => $postponed,
                "winner" => $winner,
            );

            $this->db->where('cid',$game->GameID);
            $this->db->update('sport_event',$update);
        }
    }
}
