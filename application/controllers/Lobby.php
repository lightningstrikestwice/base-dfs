<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lobby extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->load->model('Sliders');
	}

	public function index(){
        $this->Users->is_loggedin(1);

        // Load Extra Scripts/Styles
        $head['scripts'] = array(
            base_url('assets/'.THEME.'/js/jquery.countdown.min.js'),
            base_url('assets/'.THEME.'/js/lobby.js'),
        );
        $head['styles'] = array();

        // Get Standard Options
        $siteOptions = $this->Options->get_group(1);
        $genOptions = $this->Options->get_group(3);
        $components = $this->Options->get_group(5);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;
        $data['components'] = $components;

        // Get Components
        // Slider
        $data['slides'] = $this->Sliders->get('',array("field"=>"show_slide","value"=>1));

        // Sports
        $activeSports = json_decode($genOptions['active_sports']);
        $sports = array();
        foreach ($activeSports as $a) {
            $sports[$a] = $this->Misc->get_sport_icon($a);
        }
        $data['sports'] = $sports;

        // Game Types
        $data['types'] = $types = $this->config->item('game_types');


        // Get The Content
        $data['leagues'] = $leagues = $this->Lobbies->get_full_lobby();
        $data['durations'] = $this->config->item('durations');
		$data['today']= mktime(0, 0, 0, date('n'), date('j'));
		$data['tomorrow']= mktime(0, 0, 0, date('n'), date('j')+1);

        $this->load->view(THEME.'/header', $head);
        $this->load->view(THEME.'/lobby/lobby', $data);
        $this->load->view(THEME.'/footer');
	}

    public function time_info(){
        echo $this->Options->get_option('offset');
	}

    public function filter_rows($min,$max,$sports,$types) {
        $data['siteOptions'] = $this->Options->get_group(1);
        $data['durations'] = $this->config->item('durations');
		$data['today']= mktime(0, 0, 0, date('n'), date('j'));
		$data['tomorrow']= mktime(0, 0, 0, date('n'), date('j')+1);

        $leagues = $this->Lobbies->get_filtered_rows($min,$max,$sports,$types);

        $html = "";

        foreach ($leagues as $league) {
            $data['league'] = $league;
            $html .= $this->load->view(THEME.'/lobby/lobby_row', $data, TRUE);
        }

        echo $html;
    }
}
