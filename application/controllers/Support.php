<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->load->model('Support_tickets');
	}

	public function index(){
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $this->output->enable_profiler(TRUE);



        $siteOptions = $this->Options->get_group(1);

        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        $data['user'] = $this->Users->get_by_id($this->Users->id());

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|max_length[128]');
		$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email|max_length[256]');
		$this->form_validation->set_rules('phone', 'Phone #', 'required|xss_clean|max_length[50]');
		$this->form_validation->set_rules('subject', 'Subject','required|xss_clean|max_length[256]');
		$this->form_validation->set_rules('message', 'Message', 'required|xss_clean|max_length[2000]');

        if ($this->form_validation->run() == false) {
		} else {
            $post = $this->input->post();

            $this->load->model('Emails');

			//email message
            $emailData = array(
                "name" => $post['name'],
                "email" => $post['email'],
                "phone" => $post['phone'],
                "message" => $post['message'],
            );

            $subject = 'Support: '.$post['subject'];

            $dbData = $emailData;
            $dbData['message'] = htmlspecialchars($post['message']);
			$dbData['subject'] = htmlspecialchars($post['subject']);

            $this->Support_tickets->add_update($dbData);

            $siteEmail = $siteOptions['main_email'];

            $this->Emails->send_mail($siteEmail, 'support_ticket', $emailData, $subject);

            $this->Alerts->set('success', 'Thank you, your support ticket has been submitted. <br /><br />Someone from our team will review it and will be back in touch as needed.');
            redirect('support');
		}

        $this->load->view(THEME.'/header', $head);
		$this->load->view(THEME.'/support', $data);
        $this->load->view(THEME.'/footer');
	}
}
