<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->form_validation->set_error_delimiters('<p class="alert alert-danger p-1 mt-1">', '</p>');
	}

	public function index(){
        $siteOptions = $this->Options->get_group(1);
        $genOptions = $this->Options->get_group(3);

        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;
        $data['provinces'] = $this->Misc->province_list();
        $data['sources'] = $this->config->item('usersources');
        $data['months'] = $this->Misc->months();

        $passon = array(
			'username' => '',
		    'email' => '',
		    'password' => '',
		    'password_confirm' => '',
		    'first_name' => '',
		    'last_name' => '',
		    'phone' => '',
		    'address' => '',
		    'city' => '',
		    'state' => '',
            'country' => 'CA',
		    'zip' => '',
            'month' => '',
            'date' => '',
            'year' => '',
		    'usersource' => 1,
		);

        // load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set validation rules
		$this->form_validation->set_rules('email', 'Email', 'strip_tags|trim|required|valid_email|max_length[256]|min_length[4]|xss_clean|is_unique[users.email]', array('is_unique' => 'This email address already exists. Please choose another one.'));
		$this->form_validation->set_rules('username', 'Username', 'strip_tags|trim|required|max_length[14]|min_length[2]|xss_clean|alpha_dash|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean|max_length['.$genOptions['max_password_length'].']|min_length['.$genOptions['min_password_length'].']');
		$this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required|xss_clean|matches[password]|max_length['.$genOptions['max_password_length'].']|min_length['.$genOptions['min_password_length'].']');
		$this->form_validation->set_rules('first_name', 'First Name', 'strip_tags|trim|required|max_length[64]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'strip_tags|trim|required|max_length[64]|xss_clean');

        $this->form_validation->set_rules('state', 'Province', 'strip_tags|required|trim|xss_clean');
		$this->form_validation->set_rules('country', 'Country', 'strip_tags|required|trim|xss_clean');
		$this->form_validation->set_rules('usersource', 'Where did you hear from us?', 'strip_tags|trim|required|xss_clean');
		$this->form_validation->set_rules('tos', 'Terms of Service', 'strip_tags|required|xss_clean');

		if ($this->form_validation->run() === false) {
			if(!empty($_POST)){
				$passon = $_POST;
			}
		} else {
            $post = $this->input->post();

            $dob = array(
                "month" => $post['month'],
                "date" => $post['date'],
                "year" => $post['year']
            );

            $post['dob'] = json_encode($dob);

            unset($post['tos']);
			unset($post['password_confirm']);
            unset($post['month']);
            unset($post['date']);
            unset($post['year']);

			$post['join_date'] = time();
            $post['reg_ip'] = $post['ip'] = $this->Misc->get_ip();
			$post['password'] = $this->Users->hash_password($post['password']);

			$post['active'] = 1;
            $post['level'] = 0;

            // Getting location by IP
            $loc = $this->Misc->location_lookup($post['ip']);
            $post['lat'] = $loc['lat'];
            $post['lng'] = $loc['lon'];

            $post['last_login'] = time();

            if ($this->Users->create_user($post)) {

                // Email information
                $email_data = array(

                );

                // Send Register Email to User and an email to Admin
                $this->Emails->send_mail($this->input->post('email'), 'register', $email_data,$subject);
                $email_data = array(
                    "email" => $this->input->post('email'),
                    "name" => $this->input->post('first_name') . ' ' . $this->input->post('last_name')
                );
                $this->Emails->send_mail($this->Options->get_option('main_email'), 'register_admin', $email_data,$subject);

                $this->session->set_flashdata('success','Your account has successfully been created');
                redirect('register/upload_avatar');
			} else {

				// user creation failed, this should never happen
				$this->session->set_flashdata('error', 'There was a problem creating your new account. Please try again.');

			}
        }

        $data['passon'] = $passon;

        $this->load->view(THEME.'/header', $head);
        $this->load->view(THEME.'/registration/register_form', $data);
        $this->load->view(THEME.'/footer');
	}

    public function upload_avatar() {
        $siteOptions = $this->Options->get_group(1);
        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;

        // Add Scripts/Styles
        $ftd = $this->config->item('ftd_version');

        $scripts = array(
            site_url('assets/'.GLOBAL_CONTENT.'/js/jcrop/js/jquery.Jcrop.js?v='.$ftd),
            site_url('assets/'.GLOBAL_CONTENT.'/js/plupload/js/plupload.full.min.js?v='.$ftd),
            site_url('assets/'.THEME.'/js/picture_upload.js?v='.$ftd),
        );
        $head['scripts'] = $scripts;

        $styles = array(
            site_url('assets/'.GLOBAL_CONTENT.'/js/jcrop/css/jquery.Jcrop.css?v='.$ftd)
        );
        $head['styles'] = $styles;

        $userID = $this->Users->id();
        $user = $this->Users->get_by_id($userID);

        $data['user'] = $user;

		$this->load->view(THEME.'/header', $head);
		$this->load->view(THEME.'/registration/upload',$data);
		$this->load->view(THEME.'/footer');
    }

	/*----------------------------------------------------------------------
		ADD AVATAR
			This function is called via ajax from plupload and drops the image
			into the temp folder and returns the url so that it can be cropped.

     * Maker sure directories are created and have write access...
     * /uploads/user_avatar
     * /uploads/tmp

	-----------------------------------------------------------------------*/
	function add_avatar(){
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

		$this->load->model('Images');
		$img_name='file';

		//Upload Config settings
		$config['upload_path'] = './uploads/tmp';
		$config['allowed_types'] = 'png|jpg';

		//Load imgs upload library
		$this->load->library('upload');

		//Upload file and return data
		$this->upload->initialize($config);

		if ($this->upload->do_upload($img_name)) {
			$img_data = $this->upload->data();
			$new_imgname = md5(rand(0, 1000000).$fileName).$img_data['file_ext'];
			$new_imgpath = $img_data['file_path'].$new_imgname;
			rename($img_data['full_path'], $new_imgpath);

			$final_width = $this->Options->get_option('avatar_width');
			$final_height = $this->Options->get_option('avatar_height');

            $aspectratio = $final_width/$final_height;
			$return = array(
				"status" => 1,
				"url" => '../uploads/tmp/'.$new_imgname,
				"filename" => $new_imgname,
				"width" => $img_data['image_width'],
				"height" => $img_data['image_height'],
				"final_width" => $final_width,
				"final_height" => $final_height,
				"aspectratio" => $aspectratio
			);
		} else {
			$return=array(
				"status"=>0,
				"error"=>$this->upload->display_errors()
			);
		}

		echo json_encode($return);
	}

	/*----------------------------------------------------------------------
		CROP AVATAR
			This function deals with the avatar after the user has cropped it

	-----------------------------------------------------------------------*/
	function crop_avatar($reg=1){
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $this->output->enable_profiler(TRUE);


        $this->load->model('Registrations');

        $temp_image = $this->input->post('temp_image');
        $x = $this->input->post('x');
        $y = $this->input->post('y');
        $w = $this->input->post('w');
        $h = $this->input->post('h');

        $edit = $this->input->post('edit');

        if ($reg == 1) {
            $redirect = "register/upload_avatar";
            $success = "lobby";
        } else {
            $redirect = $success = "my_account/edit_photo";
        }

        if (is_numeric($x) && is_numeric($y) && is_numeric($w) && is_numeric($h)) {
            $this->load->library('image_lib');
            $reg_data = $this->session->userdata('reg_data');
            $config['image_library'] = 'gd2';
            $config['source_image'] = './uploads/tmp/'.$temp_image;
            $config['x_axis'] = $x;
            $config['y_axis'] = $y;
            $config['width'] = $w;
            $config['height'] = $h;
            $config['maintain_ratio'] = FALSE;
            $this->image_lib->initialize($config);

            if ( !$this->image_lib->crop()) {
                echo "<pre>";
                print_r("1");
                echo "</pre>";
                echo "<pre>";
                print_r($this->image_lib->display_errors());
                echo "</pre>";
//                $this->Alerts->set('error',$this->image_lib->display_errors());
//                redirect($redirect);
            } else {
                //cropped image ok, now resize it to meet big_avatar size
                $this->image_lib->clear();
                $config['image_library'] = 'gd2';
                $config['source_image'] = './uploads/tmp/'.$temp_image;
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['master_dim'] = 'width';
                $config['width'] = $this->Options->get_option('avatar_width');
                $config['height'] = $this->Options->get_option('avatar_height');
                $this->image_lib->initialize($config);
                if ( ! $this->image_lib->resize()){
                    echo "<pre>";
                    print_r("2");
                    echo "</pre>";
                    echo "<pre>";
                    print_r($this->image_lib->display_errors());
                    echo "</pre>";
//                    $this->Alerts->set('error',$this->image_lib->display_errors());
//                    redirect($redirect);
                } else {
                    echo "<pre>";
                    print_r("3");
                    echo "</pre>";
                    echo "<pre>";
                    print_r($this->session->userdata('user')->cid);
                    echo "</pre>";
                    echo "<pre>";
                    print_r($success);
                    echo "</pre>";
                    $this->Registrations->save_avatar($this->session->userdata('user')->cid, $temp_image);
//                    $this->Alerts->set('success','Successfully Changed Avatar');
//                    redirect($success);
                }
            }
        } else {
            $this->Alerts->set('error','Error processing image');
            redirect($redirect);
        }

	}
}
