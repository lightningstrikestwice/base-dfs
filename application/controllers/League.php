<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class League extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index(){

	}

    public function modal_contest_info($leagueID) {
        $league = $this->Leagues->get_by_id($leagueID);

        $data['league'] = $league;
        $data['siteOptions'] = $this->Options->get_group(1);
		$data['today']= mktime(0, 0, 0, date('n'), date('j'));
		$data['tomorrow']= mktime(0, 0, 0, date('n'), date('j')+1);

        $this->load->view(THEME.'/league/modal_contest_info',$data);
    }

    public function modal_contest_entries($leagueID) {
        $data['entries'] = $entries = $this->Leagues->get_league_entries($leagueID);

        $this->load->view(THEME.'/league/modal_entries', $data);
    }

    public function modal_contest_prizes($leagueID) {
        $league = $this->Leagues->get_by_id($leagueID);

        $data['league'] = $league;
        $data['siteOptions'] = $this->Options->get_group(1);
        $this->load->view(THEME.'/league/modal_prizes', $data);
    }
}
