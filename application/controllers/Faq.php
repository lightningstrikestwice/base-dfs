<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	function __construct(){
		parent::__construct();

        $this->load->model('Faqs');
	}

	public function index(){
        $siteOptions = $this->Options->get_group(1);

        $head['siteOptions'] = $data['siteOptions'] = $siteOptions;
        $head['title'] = $data['pageTitle'] = "Frequently Asked Questions";

        $data['faqs'] = $faqs = $this->Faqs->get_all();
        
        $this->load->view(THEME.'/header', $head);
        $this->load->view(THEME.'/faqs', $data);
        $this->load->view(THEME.'/footer');
	}
}
