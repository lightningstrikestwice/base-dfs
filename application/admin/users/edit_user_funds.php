<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$title?></h1>
            <a href="<?= base_url('admin/user/list_all') ?>" class="float-right">Go Back</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class='col-12'>
        <? $this->load->view(THEME.'/alerts'); ?>
    </div>
    <div class="col-12">
        <div class="card default">
            <div class="card-header">
                <i class="fas fa-info-circle pr-2"></i>Instructions
            </div>
            <div class="card-body">
                Manually override the user's balance here. Each transaction will record along with your message in the transaction log.
            </div>
        </div>
    </div>
</div>
<h3><span class="pr-5"><?= $user->username ?>'s Balance:</span> <span><?=$siteOptions['currency']. number_format($userBalance, 2, ".", ",")?></span></h3>

<div class="card default">
    <div class="card-header">
        Edit User Funds
    </div>
    <div class="card-body">
        <?=form_open('',array('class'=>'form-horizontal')) ?>
            <div class="form-group row">
                <label for="debitcredit" class="col-sm-2 col-form-label">Action:</label>
                <div class="col-sm-10">
                    <select id="debitcredit" class="form-control" name="debitcredit">
                        <option value="1">Add Funds</option>
                        <option value="0">Remove Funds</option>
                    </select>
                </div>
                <?= form_error('debitcredit') ?>
            </div>
            <input type="hidden" name="type" value="0" />
            <div class="form-group row d-none">
                <label for="type" class="col-sm-2 col-form-label">Type:</label>
                <div class="col-sm-10">
                    <select id="type" class="form-control" name="type">
                        <option value="0">Money</option>
                        <option value="1">Bonus Money</option>
                        <option value="2">Ticket</option>
                    </select>
                </div>
                <?= form_error('type') ?>
            </div>
            <div class="form-group row d-none">
                <label for="type" class="col-sm-2 col-form-label">User Paid Cash:</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" name="paid" id="paid_cash">
                        <label class="form-check-label" for="paid_cash">
                            By checking this box you are citing that the reason you are adding funds to this user’s account is because they paid cash. Checking this off will add the cash to the website’s revenue, thus balancing out the outstanding cash liability reported on the dashboard.
                        </label>
                    </div>

                </div>
            </div>
            <div class="form-group row">
                <label for="amount" class="col-sm-2 col-form-label">Amount:</label>
                <div class="col-sm-10">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><?=$siteOptions['currency'] ?></div>
                        </div>
                        <input type="text" class="form-control" id="amount" placeholder="" name="amount">
                    </div>
                </div>
                <?= form_error('amount') ?>
            </div>
            <div class="form-group row">
                <label for="message" class="col-sm-2 col-form-label">Message:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="message" placeholder="" name="message">
                </div>
                <?= form_error('message') ?>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>

        <?=form_close() ?>
    </div>
</div>
