<div class="row">
    <div class="col-xl-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left">Tickers</h1>
            <a href="<?=base_url()?>admin/content/edit_ticker" class="btn btn-primary float-right">Create New Ticker</a>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<? $this->load->view(THEME.'/alerts');  ?>

<div class="card default">
    <div class="card-header">
        <i class="fas fa-info-circle pr-2"></i>Instructions
    </div>
    <div class="card-body">
        <p>Add a new ticker by clicking on button at the top right</p>
        <p>Adjust the Sort Order by dragging and dropping in the section below</p>
        <p>Edit a ticker but using the section at the bottom</p>
    </div>
</div>

<?  if (count($sortOrder) > 1): ?>
        <div class="card default">
            <div class="card-header">Sort Order</div>
            <div class="card-body">
                <div class="dd">
                    <ol class="dd-list">
                <?  foreach($sortOrder as $ticker):   ?>
                        <li class="dd-item" data-id="<?=$ticker->cid ?>">
                            <div class="dd-handle">
                                <?=$ticker->content ?>
                        <?  if (strlen($ticker->link) > 0): ?>
                                <span class="d-inline-block pl-3">(<?=$ticker->link ?>)</span>
                        <?  endif;  ?>
                            </div>
                        </li>
                <?  endforeach; ?>
                    </ol>
                </div>
            </div>
        </div>
<?  endif;  ?>

<div class="card default">
    <div class='card-header'>Tickers</div>
    <div class="card-body">
        <div class="table-responsive-lg">
            <table class="table table-striped table-sm table-hover" id="table_list">
                <thead>
                    <tr>
                        <th class="w-50">Content</th>
                        <th class="w-25">Link</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
            <?  foreach ($tickers as $ticker):
                ?>
                    <tr>
                        <td><a href="<?=base_url()?>admin/content/edit_ticker/<?=$ticker->cid?>" title="Edit Ticker" class=""><?= $ticker->content ?></a></td>
                        <td><a href="<?=base_url()?>admin/content/edit_ticker/<?=$ticker->cid?>" title="Edit Ticker" class=""><?= $ticker->link ?></a></td>
                        <td class="text-center">
                            <a href="<?=base_url()?>admin/content/edit_ticker/<?=$ticker->cid?>" title="Edit Ticker" class="pr-1 btn btn-link"><i class="fas fa-edit"></i></a>
                            <button data-slug="<?=$ticker->content?>" data-id="<?=$ticker->cid?>" class="removeButton btn btn-link"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
            <?  endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="deleteConfirm" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-danger text-white">
				<h4 class="modal-title" id="myModalLabel">Delete Page</h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
                <p>Are you sure you want to remove the "<span id='remove_name' class="font-weight-bold"></span>" Page?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<a id='actionURL' href="" class="btn btn-danger">Yes</a>
			</div>
		</div>
	</div>
</div>

<script>
jQuery(document).ready(function($){
    $('.dd').nestable({ maxDepth:1 });

    $('.removeButton').on('click',function(){
        var slug = $(this).data('slug');
        var id = $(this).data('id');

        $('#remove_name').text(slug);
        $('#actionURL').prop('href',HOST_NAME+'admin/content/delete_ticker/'+id);

        $('#deleteConfirm').modal('show');
    });

    $('.dd').on('change',function(){

        var tickers = $('.dd').nestable('serialize');
        var tickersArray = JSON.stringify(tickers);

//        jQuery.each(tickers, function(i, val){
//            tickersArray.push(val.id);
//        });

        $.ajax({
            type: "POST",
            url:HOST_NAME+'admin/content/save_ticker_sort_order/',
            data: {'sort_order': tickersArray},
            success: function (msg) {
                console.log(msg);
            }
        });
    });
});
</script>