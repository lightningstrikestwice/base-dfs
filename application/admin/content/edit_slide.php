
<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle ?></h1>
            <a href="javascript:history.go(-1)" class="float-right">Go Back</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class='col-12'>
        <? $this->load->view(THEME.'/alerts'); ?>
    </div>
    <div class="col-12">
        <div class="card default">
            <div class="card-header">
                <i class="fas fa-info-circle pr-2"></i>Instructions
            </div>
            <div class="card-body">
                Images used should be 350px by 250px in order to look correct.
            </div>
        </div>
    </div>
    <div class="col-12">
        <?= form_open_multipart() ?>
        <input type="hidden" name="cid" value="<?=$slide->cid?>" />
        <div class="card default">
            <div class="card-header">Slide Details</div>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-12 col-sm-4 pr-3">
                        <label for="image">Image URL</label>
                        <input type="text" class="form-control" id="image" name="image" value="<?=$slide->image?>" placeholder="URL of Image">
                        <div class="text-center">-- OR --</div>
                        <label for="upload" class="">Upload Image </label>
                        <input type="file" id="upload" name="upload" class="form-control">
                        <p class="help-block">.jpg or .png only</p>
                    </div>
                    <div class="form-group col-12 col-sm-4">
                        <label for="link">Link</label>
                        <input type="text" class="form-control" id="link" name="link" value="<?=$slide->link?>">
                        <p class="help-block">Leave blank if slide is just an image</p>
                        <?= form_error('link') ?>
                        <div class="row">
                            <div class="form-group col-12 col-md-6">
                                <div class="form-check">
                                <?  if (!isset($slide->show_slide)) {
                                        $chk = " checked";
                                    } else {
                                        $chk = $slide->show_slide == 1 ? " checked" : "";
                                    }   ?>
                                    <input class="form-check-input" type="checkbox" value="1" name="show_slide" id="show_slide"<?=$chk?>>
                                    <label class="form-check-label" for="show_slide">
                                        Show Slide in Lobby
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <div class="form-check">
                                <?  $chk = $slide->new_window == 1 ? " checked" : "";  ?>
                                    <input class="form-check-input" type="checkbox" value="1" name="new_window" id="new_window"<?=$chk?>>
                                    <label class="form-check-label" for="new_window">
                                        Open in a New Window
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
            <?  if (isset($slide->image)):  ?>
                    <div class="form-group col-12 col-sm-4">
                        <img src="<?=base_url() ?>uploads/slides/<?=$slide->image?>" class="preview_image" />
                    </div>
            <?  endif;  ?>
                </div>
                <div class="form-row">
                </div>

            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save Slide</button>
        <?= form_close() ?>
    </div>
</div>

