
<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle ?></h1>
            <a href="javascript:history.go(-1)" class="float-right">Go Back</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class='col-12'>
        <? $this->load->view(THEME.'/alerts'); ?>
    </div>
    <div class="col-12">
        <?= form_open() ?>
        <input type="hidden" name="cid" value="<?=$faq->cid?>" />
        <div class="card default">
            <div class="card-header">FAQ Details</div>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-12 col-sm-6">
                        <label for="question">Question</label>
                        <textarea class="form-control" rows="3" name="question" id="question"><?=$faq->question?></textarea>
                        <?= form_error('question') ?>
                    </div>
                    <div class="form-group col-12 col-sm-6">
                        <label for="title">Answer</label>
                        <textarea class="form-control" rows="3" name="answer" id="answer"><?=$faq->answer?></textarea>
                        <?= form_error('answer') ?>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save FAQ</button>
        <?= form_close() ?>
    </div>
</div>

