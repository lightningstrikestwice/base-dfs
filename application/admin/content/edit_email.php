
<? $this->load->view(ADMIN_THEME. '/tinymce');    ?>
<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle ?></h1>
            <a href="javascript:history.go(-1)" class="float-right">Go Back</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class='col-12'>
        <? $this->load->view(THEME.'/alerts'); ?>
    </div>
    <div class="col-12">
        <?= form_open() ?>
            <input type="hidden" name="cid" value="<?=$email->cid?>" />
            <div class="card default">
                <div class="card-header">Email Details</div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-12 col-sm-6">
                            <label for="title">Slug</label>
                            <input type="text" class="form-control" id="slug" name="slug" value="<?=$email->slug?>">
                            <?= form_error('slug') ?>
                        </div>
                        <div class="form-group col-12 col-sm-6">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" id="subject" name="subject" value="<?=$email->subject?>">
                            <?= form_error('subject') ?>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label for="title">Body</label>
                            <textarea id="body_text" name="body" class="form-control"><?=$email->body?></textarea>
                            <?=  form_error('body');?>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save Email</button>
        <?= form_close() ?>
        </div>
    </div>
</div>

