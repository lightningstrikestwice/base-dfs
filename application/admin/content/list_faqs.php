<div class="row">
    <div class="col-xl-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left">FAQ's</h1>
            <a href="<?=base_url()?>admin/content/edit_faq" class="btn btn-primary float-right">Create New FAQ</a>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<? $this->load->view(THEME.'/alerts');  ?>

<div class="card default">
    <div class="card-header">
        <i class="fas fa-info-circle pr-2"></i>Instructions
    </div>
    <div class="card-body">
        FAQs should be used for everything that involves a commonly asked question and answer.
    </div>
</div>

<div class="card default p-2">
    <div class="card-body">
        <div class="table-responsive-lg">
            <table class="table table-striped table-sm table-hover" id="table_list">
                <thead>
                    <tr>
                        <th class="w-25">Question</th>
                        <th>Answer Excerpt</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
            <?  foreach ($faqs as $faq):
                    $answer = strlen($faq->answer) > 199 ? substr($faq->answer, 0, 200)."..." : $faq->answer;
                ?>
                    <tr>
                        <td><a href="<?=base_url()?>admin/content/edit_faq/<?=$faq->cid?>" title="Edit Faq"><?= $faq->question ?></a></td>
                        <td><a href="<?=base_url()?>admin/content/edit_faq/<?=$faq->cid?>" title="Edit Faq"><?= $answer ?></a></td>
                        <td class="text-center">
                            <a href="<?=base_url()?>admin/content/edit_faq/<?=$faq->cid?>" title="Edit Faq" class="pr-1 btn btn-link"><i class="fas fa-edit"></i></a>
                            <button data-slug="<?=$faq->question?>" data-id="<?=$faq->cid?>" class="removeButton btn btn-link"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
            <?  endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="deleteConfirm" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-danger text-white">
				<h4 class="modal-title" id="myModalLabel">Delete FAQ</h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
                <p>Are you sure you want to remove the "<span id='remove_name' class="font-weight-bold"></span>" FAQ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<a id='actionURL' href="" class="btn btn-danger">Yes</a>
			</div>
		</div>
	</div>
</div>

<script>
jQuery(document).ready(function($){
    $('#table_list').DataTable({
        "order": [[ 0,"asc"]],  // Set Column # for Default Sort/Dir (zero based)
        "paging": false,
        "pageLength": 50,
        "language": {
            "searchPlaceholder": "Search FAQ's",
            "search": "",
            "lengthMenu": "Show _MENU_ FAQ's"
        },
        "columnDefs": [ {
            "targets": [2],     // Column Number to exclude from Sort (zero based)
            "orderable": false
            }]
    });

    $('.removeButton').on('click',function(){
        var slug = $(this).data('slug');
        var id = $(this).data('id');

        $('#remove_name').text(slug);
        $('#actionURL').prop('href',HOST_NAME+'admin/content/delete_faq/'+id);

        $('#deleteConfirm').modal('show');
    });
});
</script>