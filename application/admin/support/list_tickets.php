<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle?></h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-12">
        <? $this->load->view(THEME.'/alerts');  ?>
    </div>
    <div class="col-12">
        <div class="card default">
            <div class="card-header">
                Instructions
            </div>
            <div class="card-body">
                Use this system to see all the tickets that come in via the support tab on the website. You should call, email, or act upon tickets as soon as possible. Tickets can also optionally be assigned to multiple admins should you choose to do so and if you have multiple admin accounts.
            </div>
        </div>
    </div>
    <div class="col-12">
        <ul class="nav nav-pills default ml-2 mb-3">
    <?  $all = $show == "all" ? " active" : "";
        $my = $show == "my" ? " active" : "";   ?>
            <li class="nav-item">
                <a class="nav-link<?=$all?>" id="all_tickets" href="<?= base_url('admin/support/browse')?>">All Tickets</a>
            </li>
            <li class="nav-item">
                <a class="nav-link<?=$my?>" id="my_tickets" href="<?= base_url('admin/support/browse/my')?>">My Tickets</a>
            </li>
        </ul>
    </div>
    <div class="col-12">
        <ul class="nav nav-pills default ml-2 mb-1">
    <?  $a = 0;
        foreach($statuses as $k=>$status):
            $a++;
            $activeClass = $a==1 ? " active" : "";
            $tab = strtolower($status['status']);
            ?>
            <li class="nav-item">
                <a class="nav-link<?=$activeClass?>" id="<?=$tab?>-tab" data-toggle="tab" href="#<?=$tab?>" role="tab" aria-controls="<?=$tab?>" aria-selected="true"><?=$status['status'] ?></a>
            </li>
    <?  endforeach; ?>
        </ul>
        <div class="tab-content" id="pageInfo">
    <?  $t = 0;
        foreach($statuses as $k=>$status):
            $t++;
            $activeClass = $t==1 ? " active" : "";
        ?>
            <div class="tab-pane fade show<?=$activeClass?>" id="<?=strtolower($status['status']) ?>">
                <div class="card default">
                    <div class="card-header">Browse Support Tickets</div>
                    <div class="card-body">
                        <table class="table table-striped table-sm table-hover tickets_table" id="table_<?=strtolower($status['status'])?>">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Assigned</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Subject</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?  foreach ($tickets[strtolower($status['status'])] as $ticket):
                                $assignedTo = $ticket->assigned_user_id == 0 ? "Unassigned" : $admins[$ticket->assigned_user_id]->username;
                                $status = '<span class="badge '.$statuses[$ticket->status]['badge'].'">'.$statuses[$ticket->status]['status'].'</span>';
                                $ticketID = $ticket->cid;
                            ?>
                                <tr>
                                    <td><?= $ticketID ?></td>
                                    <td><?= $assignedTo ?></td>
                                    <td><?= $ticket->name ?></td>
                                    <td><?= $ticket->email ?></td>
                                    <td><?= $ticket->phone ?></td>
                                    <td><?= $ticket->subject ?></td>
                                    <td><?= date("m/d/y g:iA", $ticket->timestamp) ?></td>
                                    <td><?= $status ?></td>
                                    <td>
                                        <a href="<?=base_url()?>admin/support/view_ticket/<?=$ticketID?>" title="Edit Ticket" class="pr-1 btn btn-link"><i class="fas fa-edit"></i></a>
                                <?  if ($ticket->status != 3):  ?>
                                        <a href="<?=base_url()?>admin/support/complete_ticket/<?=$ticketID?>/<?=$show?>" title="Complete Ticket" class="pr-1 btn btn-link"><i class="fas fa-check"></i></a>
                                <?  endif;  ?>
                                        <button data-id="<?=$ticketID?>" class="removeButton btn btn-link"><i class="fas fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                        <?  endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
    <?  endforeach; ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="deleteConfirm" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-danger text-white">
				<h4 class="modal-title" id="myModalLabel">Delete Ticket</h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
                <p>Are you sure you want to remove this ticket?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<a id='actionURL' href="" class="btn btn-danger">Yes</a>
			</div>
		</div>
	</div>
</div>

<script>
jQuery(document).ready(function($){
    $('.tickets_table').DataTable({
        "order": [[ 0,"desc"]],  // Set Column # for Default Sort/Dir (zero based)
        "paging": true,
        "pageLength": 50,
        "language": {
            "searchPlaceholder": "Search Tickets",
            "search": "",
            "lengthMenu": "Show _MENU_ Tickets"
        },
        "columnDefs": [ {
            "targets": [8],     // Column Number to exclude from Sort (zero based)
            "orderable": false
            }]
    });

    $('.removeButton').on('click',function(){
        var id = $(this).data('id');

       $('#actionURL').prop('href',HOST_NAME+'admin/support/delete_ticket/'+id);

        $('#deleteConfirm').modal('show');
    });
});
</script>