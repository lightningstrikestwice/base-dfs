<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle?></h1>
            <a href="<?=base_url()?>admin/payment_info/edit_product" class="btn btn-primary float-right">Create New Product</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-12">
        <? $this->load->view(THEME.'/alerts');  ?>
    </div>
    <div class="col-12">
        <div class="card default">
            <div class="card-header">
                Instructions
            </div>
            <div class="card-body">
                Manage the deposit packages listed on the add funds page from this section. These probably will not change often at all.
            </div>
        </div>
    </div>
    <div class="col-12">
        <?  $this->load->view(ADMIN_THEME.'/payment_info/subnav') ?>
    </div>
    <div class="col-12">
        <div class="card default">
            <div class="card-body">
                <div class="table-responsive-lg">
                    <table class="table table-striped table-sm table-hover" id="table_list">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                    <?  foreach ($products as $product):
                            $productID = $product->cid;
                            ?>
                            <tr>
                                <td><?= $productID ?></td>
                                <td><?= $product->name ?></td>
                                <td><?= $product->description ?></td>
                                <td>$<?= $product->price ?></td>
                                <td class="text-center">
                                    <a href="<?=base_url()?>admin/payment_info/edit_product/<?=$productID?>" title="Edit Slide" class="pr-1 btn btn-link"><i class="fas fa-edit"></i></a>
                                    <button data-slug="<?=$product->name ?>" data-id="<?=$productID?>" class="removeButton btn btn-link"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                    <?  endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="deleteConfirm" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-danger text-white">
				<h4 class="modal-title" id="myModalLabel">Delete Product</h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
                <p>Are you sure you want to remove this ticket?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<a id='actionURL' href="" class="btn btn-danger">Yes</a>
			</div>
		</div>
	</div>
</div>

<script>
jQuery(document).ready(function($){
    $('#table_list').DataTable({
        "order": [[ 0,"desc"]],  // Set Column # for Default Sort/Dir (zero based)
        "paging": true,
        "pageLength": 50,
        "language": {
            "searchPlaceholder": "Search Products",
            "search": "",
            "lengthMenu": "Show _MENU_ Products"
        },
        "columnDefs": [ {
            "targets": [3],     // Column Number to exclude from Sort (zero based)
            "orderable": false
            }]
    });

    $('.removeButton').on('click',function(){
        var slug = $(this).data('slug');
        var id = $(this).data('id');

        $('#remove_name').text(slug);
        $('#actionURL').prop('href',HOST_NAME+'admin/payment_info/delete_product/'+id);

        $('#deleteConfirm').modal('show');
    });
});
</script>