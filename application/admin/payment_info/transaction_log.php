<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle?></h1>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="card default">
    <div class="card-header">
        Instructions
    </div>
    <div class="card-body">
        Use this log to explore all of the financial transactions on the website including deposits, widthdrawals, fund reversals for unfilled contests, promo code use, and so on.
    </div>
</div>

<div class="card default">
    <div class="card-body">
        <div class="table-responsive-lg">
            <table class="table table-striped table-sm table-hover" id="table_list">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>User</th>
                        <th>Amount</th>
                        <th>Comment</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
            <?  foreach ($transactions as $transaction):
                    $color = $transaction->debit == 1 ? "text-success" : "text-danger";
                    $ind = $transaction->debit == 1 ? "+" : "-";
                ?>
                    <tr>
                        <td><?= date("m/d/y g:iA", $transaction->timestamp) ?></td>
                        <td><?= $transaction->user->username ?></td>
                        <td class="<?=$color ?>"><?= $ind ?> <?=$siteOptions['currency'].number_format($transaction->amount,2,'.',',') ?></td>
                        <td><?= $transaction->comment ?></td>
                        <td><?= $siteOptions['currency'].$transaction->new_balance ?></td>
                    </tr>
            <?  endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function($){
    $('#table_list').DataTable({
        "order": [[ 0,"desc"]],  // Set Column # for Default Sort/Dir (zero based)
        "paging": true,
        "pageLength": 50,
        "language": {
            "searchPlaceholder": "Search Transactions",
            "search": "",
            "lengthMenu": "Show _MENU_ Transactions"
        }
    });
});
</script>