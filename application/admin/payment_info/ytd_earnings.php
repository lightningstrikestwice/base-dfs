<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle?></h1>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="card default">
    <div class="card-header">
        Instructions
    </div>
    <div class="card-body">
        This report gives you information on an annual basis of the earnings made by users year by year as the key indicates. Remember that since the website is looking at <strong>all of the user's withdrawals (minus) their deposits</strong>, you are not taking into account their "winnings" on the website unless they actually cash it out. Therefore, the user can or may have kept funds on the website (and just used it for gameplay) without ever taking it out and therefore, it is not considered profit that has been paid to the user. How the cash is retained by <?=$siteOptions['website_name'] ?> as a liability for tax purposes is up to <?=$siteOptions['website_name'] ?>'s CPA however it should be considered taxable income (it is a cash on hand liability that you are holding).
    </div>
</div>

<div class="card default">
    <div class="card-body">
        <div class="table-responsive-lg">
            <table class="table table-striped table-sm table-hover" id="table_list">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Withdrawals</th>
                        <th>Earnings</th>
                    </tr>
                </thead>
                <tbody>
            <?  foreach ($earnings as $earning):
                    $user = isset($users[$earning->user_id]) ? $users[$earning->user_id] : array("username"=>"User Not Found", "first_name"=>"", "last_name"=>"", "address"=>"");
                    $user = $this->Misc->convert_to_object($user);
                ?>
                    <tr>
                        <td><?= $earning->user_id ?></td>
                        <td><?= $user->username ?></td>
                        <td><?= $user->first_name ?></td>
                        <td><?= $user->last_name ?></td>
                        <td><?= $user->address ?></td>
                        <td><?= $earning->withdrawals ?></td>
                        <td><?= $earning->total_earnings ?></td>
                    </tr>
            <?  endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function($){
    $('#table_list').DataTable({
        "order": [[ 1,"asc"]],  // Set Column # for Default Sort/Dir (zero based)
        "paging": true,
        "pageLength": 50,
        "language": {
            "searchPlaceholder": "Search Users",
            "search": "",
            "lengthMenu": "Show _MENU_ Users",
            "zeroRecords": "No YTD Earnings Yet",
        },
        "columnDefs": [ {
            "targets": [],     // Column Number to exclude from Sort (zero based)
            "orderable": false
            }]
    });
});
</script>