<?
$values = array(
    "type" => "",
    "sport" => "",
    "duration" => "",
    "extreme" => 0,
    "extreme_spread" => "",
    "size" => 2,
    "multi_entry_limit" => 1,
    "entry_fee" => 0,
    "prize" => 0,
    "guaranteed" => 0,
    "featured" => 0
);

if (isset($template)){
    $tSports = json_decode($template->sport,true);
    $c = 0;
    $sportDesc = "";

    foreach ($tSports as $tSport) {
        $c++;
        $sep = $c > 1 ? "_" : "";
        $sportDesc .= $sep.$tSport;
    }

    $values = array(
        "type" => $template->type,
        "sport" => $sportDesc,
        "duration" => $template->duration,
        "extreme" => $template->extreme,
        "extreme_spread" => $template->extreme_spread,
        "size" => $template->size,
        "multi_entry_limit" => $template->multi_entry_limit,
        "entry_fee" => $template->entry_fee,
        "prize" => $template->total_prize,
        "guaranteed" => $template->guaranteed,
        "featured" => $template->feat
    );
}
?>

<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle ?></h1>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<? $this->load->view(THEME.'/alerts'); ?>

<div class="card default">
    <div class="card-header">
        <i class="fas fa-info-circle pr-2"></i>Instructions
    </div>
    <div class="card-body">
        Fill out the form below to create a new Featured Contest.  Choosing a Contest type will determine the sports available.  Choosing a Sport will determine the Duration options for the contest.
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= form_open() ?>
            <input type="hidden" id="templateSport" value="<?=$values['sport'] ?>" />
            <input type="hidden" id="templateDuration" value="<?=$values['duration'] ?>" />
            <div class="card default">
                <div class="card-header">New Contest</div>
                <div class="card-body">
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Contest Type:</label>
                        <div class="col-sm-8">
                            <select class="form-control form-control-sm" id="contestTypeList" name="type">
                                <option value="999" disabled selected>-- Choose Contest Type --</option>
                        <?  foreach ($gameTypes as $k=>$type):
                                $sel = $k == $values['type'] ? " selected" : "";    ?>
                                <option value="<?=$k?>"<?=$sel?>><?=$type ?></option>
                        <?  endforeach; ?>
                            </select>
                            <?= form_error('type') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="sport" class="col-sm-4">Sport:</label>
                        <div class="col-sm-8" id="sportsList">
                            <div class="checked-state sport-type">
                    <?  foreach ($sports as $sport):
                            $icon = $this->Misc->get_sport_icon($sport);    ?>
                            <input type="checkbox" id="radio_<?=$sport ?>" name="sport[]" value="<?=$sport?>" disabled/>
                            <label for="radio_<?=$sport ?>">
                                <span class="option-box"><i class="<?=$icon?>"></i></span>
                                <span class="inner-text"><?=$sport?></span>
                            </label>
                    <?  endforeach; ?>
                            </div>
                            <?= form_error('sport') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Duration:</label>
                        <div class="col-sm-8" id="durationList">
                            <select class="form-control form-control-sm" id="durationTypeList" name="duration" disabled>
                                <option value="999">-- Choose Duration --</option>
                            </select>
                            <?= form_error('duration') ?>
                        </div>
                    </div>
                    <div class="form-group row" id="startDate">
                        <label for="game_type" class="col-sm-4">Start Date:</label>
                        <div class="col-sm-8">
                            <div class="input-group input-group-sm date" id="datetimepicker4" data-target-input="nearest">
                                <input type="text" name="start_date" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                                <div class="input-group-append" id="dateTime" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                </div>
                            </div>
                            <?= form_error('start_date') ?>
                        </div>
                    </div>
                    <div class="form-group row d-none" id="extreme">
                        <label for="game_type" class="col-sm-4">Extreme:</label>
                        <div class="col-sm-8">
                            <div class="row">
                            <?  $sel = $values['extreme'] == 1 ? " checked" : "";   ?>
                                <div class="col-sm-2"><input type="checkbox" id="extremeChk" name="extreme" value="1"<?=$sel?>/></div>
                                <div class="col">
                                    <label for="extremeSpread" class="d-inline">Point Spread:</label>
                                    <input class="d-inline w-25" type="text" disabled class="form-control form-control-sm" id="extremeSpread" name="extreme_spread" value="<?=$values['extreme_spread'] ?>" />
                                </div>
                                <div class="col-sm-2 float-right"><i class="fas fa-question-circle fa-lg pull-right mt-1" data-toggle="tooltip" data-placement="right" title="If checked, set the point spread for all games." ></i></div>
                            </div>
                            <?= form_error('extreme') ?>
                            <?= form_error('extreme_spread') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4"># of Entries:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm w-75 d-inline-block" name="size_val" value="<?=$values['size'] ?>" />
                            <?= form_error('size_val') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Entry Limit:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm w-75 d-inline-block" name="multi_entry_limit" value="<?=$values['multi_entry_limit'] ?>" /><i class="fas fa-question-circle fa-lg pull-right mt-1" data-toggle="tooltip" data-placement="right" title="Entering a number higher than one will designate the game multi-entry" ></i>
                            <?= form_error('multi_entry_limit') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Buy In:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm w-75 d-inline-block" name="entry_fee" value="<?=$values['entry_fee'] ?>" /><i class="fas fa-question-circle fa-lg pull-right mt-1" data-toggle="tooltip" data-placement="right" title="Leave as '0' if Free" ></i>
                            <?= form_error('entry_fee') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Prize:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm w-75 d-inline-block" name="prize" value="<?=$values['prize'] ?>" />
                            <?= form_error('prize') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Name:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm w-75 d-inline-block" name="name" value="" />
                            <?= form_error('name') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Guaranteed:</label>
                        <div class="col-sm-8">
                            <?  $sel = $values['guaranteed'] == 1 ? " checked" : "";   ?>
                            <input type="checkbox" id="guaranteed" name="guaranteed" value="1"<?=$sel?>/><i class="fas fa-question-circle fa-lg pull-right mt-1" data-toggle="tooltip" data-placement="right" title="Guaranteed Contests run regardless if they are full or not." ></i>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Featured:</label>
                        <div class="col-sm-8">
                            <?  $sel = $values['featured'] == 1 ? " checked" : "";   ?>
                            <input type="checkbox" id="featured" name="featured" value="1"<?=$sel?>/><i class="fas fa-question-circle fa-lg pull-right mt-1" data-toggle="tooltip" data-placement="right" title="If checked, this contest will float to the top of the list in the lobby" ></i>
                        </div>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary">Create Contest</button>
                        <span class="float-right">
                            <span class="mr-2">Save as Template</span> <input type="checkbox" id="createTemplate" name="createTemplate" value="1"/>
                        </span>
                    </div>
                </div>
            </div>
        <?= form_close() ?>
    </div>
    <div class="col-md-12">
        <div class="card default">
            <div class="card-header">Contest Templates</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Sport</th>
                                <th>Duration</th>
                                <th># Entries</th>
                                <th>Entry Limit</th>
                                <th>Buy In</th>
                                <th>Prize</th>
                                <th>Guaranteed</th>
                                <th>Featured</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                    <?  foreach ($templates as $template):
                            $type = $gameTypes[$template->type];
                            $sportArray = json_decode($template->sport,true);
                            $duration = $durations[$template->duration];
                            $guaranteed = $template->guaranteed == 0 ? '' : '<i class="fas fa-check-circle"></i>';
                            $featured = $template->feat == 0 ? '' : '<i class="fas fa-check-circle"></i>';

                            if (count($sportArray) == 1) {
                                $sportDesc = $sportArray[0];
                            } else {
                                $c = 0;

                                foreach ($sportArray as $sport){
                                    $c++;
                                    $sep = $c > 1 ? ", " : "";

                                    $sportDesc = $sep.$sport;
                                }
                            }
                        ?>
                            <tr>
                                <td><?= $type?></td>
                                <td><?= $sportDesc?></td>
                                <td><?= $duration ?></td>
                                <td><?= $template->size ?></td>
                                <td><?= $template->multi_entry_limit ?></td>
                                <td><?= $siteOptions['currency'].$template->entry_fee ?></td>
                                <td><?= $template->total_prize ?></td>
                                <td><?= $guaranteed ?></td>
                                <td><?= $featured ?></td>
                                <td>
                                    <a href="<?=base_url()?>admin/contest/featured_contest/<?=$template->cid?>" title="Use Template" class="pr-1 btn btn-link"><i class="fas fa-clone"></i></a>
                                    <button data-id="<?=$template->cid?>" class="removeButton btn btn-link"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                    <?  endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="deleteConfirm" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-danger text-white">
				<h4 class="modal-title" id="myModalLabel">Delete Template</h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
                <p>Are you sure you want to remove this template?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<a id='actionURL' href="" class="btn btn-danger">Yes</a>
			</div>
		</div>
	</div>
</div>


<script>
    function show_sports(cType, firstLoad=0) {
        if (cType != null && cType != 999) {
            $('#sportsList').load(HOST_NAME+'admin/contest/show_sport_list/'+cType+'/'+$('#templateSport').val());

            if (cType == 12){
                $('#extreme').removeClass('d-none');
            } else {
                $('#extreme').addClass('d-none');
            }
        } else {
            $('#sportsList').load(HOST_NAME+'admin/contest/show_sport_list/'+cType);
            $('#durationList').load(HOST_NAME+'admin/contest/show_duration_list/ALL/'+cType);
        }

        if (firstLoad == 1) {
            show_durations($('#templateSport').val(),$('#templateDuration').val());
        }
    }

    function show_durations(sportList="", selDuration="") {
        var sports = sportList == "" ? "" : sportList;
        var sep = "";
        var c = 0;
        var contestType = $('#contestTypeList').val();

        if (sportList == "") {
            $('input[name="sport[]"]:checked').each(function(){
                c++;
                sep = c > 1 ? "_" : "";
                sports += sep + $(this).val();

            });
        }

        $('#durationList').load(HOST_NAME+'admin/contest/show_duration_list/'+sports+"/"+contestType+"/"+selDuration);
    }

    $(function(){
        show_sports($('#contestTypeList').val(),1);

        // Show Sports
        $(document.body).on('change','#contestTypeList',function(e){
            var thisType = $(this).val();
            show_sports(thisType);
        });

        // Show Duration List
        $(document.body).on('change','#sportsList',function(e){
            show_durations();
        });


        // Extreme Point Spread
        $(document.body).on('change','#extremeChk',function(e) {
            if ($('#extremeChk').prop('checked') == true) {
                $('#extremeSpread').prop('disabled',false).focus();
            } else {
                $('#extremeSpread').prop('disabled',true);
            }
        });

        let today = new Date().toLocaleDateString();
        $('#datetimepicker4').datetimepicker({
            format: 'L',
            minDate: today
        });

        $('.removeButton').on('click',function(){
            var id = $(this).data('id');

            $('#actionURL').prop('href',HOST_NAME+'admin/contest/delete_template/'+id);

            $('#deleteConfirm').modal('show');
        });

    });
</script>