    <select class="form-control form-control-sm" id="durationTypeList" name="duration">
        <option value="999" disabled selected>-- Choose Duration --</option>
<?  foreach ($durationList as $k=>$type):
        $sel = $k == $selDuration ? " selected" : "";   ?>
        <option value="<?=$k?>"<?=$sel?>><?=$type ?></option>
<?  endforeach; ?>
    </select>
