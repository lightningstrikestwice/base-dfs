<div class="row">
    <div class="col-xl-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left">Manage Contests</h1>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

    <? $this->load->view(THEME.'/alerts');  ?>

<div class="card default">
    <div class="card-header">
        Instructions
    </div>
    <div class="card-body">
        <p>Upcoming = contests that have not yet begun<br>Live = contests already locked and underway<br>Completed = completed contest archive</p>
        <p>Deleting contests prior to the start time will automatically remove all entrants and will refund them. You can also remove individual users from the entries column. The Export Entrants option will give you a CSV / Excel file of all the entrants if you need to obtain their information to provide them with a customer service email or other notice regarding the contest.</p>
    </div>
</div>
<ul class="nav nav-pills default ml-2 mb-3">
<?
$upco = $type == "upcoming" ? " active" : "";
$live = $type == "live" ? " active" : "";
$comp = $type == "complete" ? " active" : "";
?>

    <li class="nav-item">
        <a class="nav-link<?=$upco?>" id="upcoming_contests" href="<?= base_url('admin/contest/manage/upcoming')?>">Upcoming</a>
    </li>
    <li class="nav-item">
        <a class="nav-link<?=$live?>" id="live_contests" href="<?= base_url('admin/contest/manage/live')?>">Live</a>
    </li>
    <li class="nav-item">
        <a class="nav-link<?=$comp?>" id="complete_contests" href="<?= base_url('admin/contest/manage/complete')?>">Completed</a>
    </li>
</ul>
<?  if ($type == "complete"):   ?>
<div class="row mb-3">
    <div class="col-2">
        <label>Start Date:</label>
        <div class="input-group input-group-sm date" id="startDate" data-target-input="nearest">
            <input type="text" name="start_date" class="form-control datetimepicker-input" data-target="#startDate" value="<?= date($genOptions['date_format'],strtotime('-7 days')) ?>"/>
            <div class="input-group-append" id="start" data-target="#startDate" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
            </div>
        </div>
    </div>
    <div class="col-2">
        <label>End Date:</label>
        <div class="input-group input-group-sm date" id="endDate" data-target-input="nearest">
            <input type="text" name="end_date" class="form-control datetimepicker-input" data-target="#endDate" value="<?= date($genOptions['date_format'],time()) ?>" />
            <div class="input-group-append" id="start" data-target="#endDate" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
            </div>
        </div>
    </div>
    <div class="col-2">
        <button class="btn btn-primary btn-sm mt-4"><i class="fas fa-filter"></i> Filter</button>
    </div>
</div>

<?  endif;  ?>

<div class="table-responsive-lg">
    <div class="card default py-2">
        <div class="card-body">
            <table class="table table-striped table-sm table-hover" id="table_list">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Sport</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Duration</th>
                        <th>Entries</th>
                        <th>Size</th>
                        <th>Entry Fee</th>
                        <th>Created Date</th>
                <?  if ($type != "complete"):   ?>
                        <th>Actions</th>
                <?  endif;  ?>
                    </tr>
                </thead>
                <tbody>
            <?  foreach ($leagues as $league):
                    $sports = $this->Misc->stringify_array($league->sport,", ");

                ?>
                    <tr>
                        <td><?= $league->cid ?></td>
                        <td><?= $sports ?></td>
                        <td><?= $league->name ?></td>
                        <td><?= $types[$league->type] ?></td>
                        <td><?= $durations[$league->duration] ?></td>
                        <td><?= $league->numEntries ?></td>
                        <td><?= $league->size ?></td>
                        <td><?= $league->entry_fee ?></td>
                        <td data-sort="<?=$league->created_date?>"><?= date($genOptions['date_format'], $league->created_date) ?></td>
                <?  if ($type != "complete"):   ?>
                        <td>
                    <?  if ($type == "upcoming"):   ?>
                            <button data-id="<?=$league->cid?>" class="showEntrants btn btn-link"><i class="fas fa-list-alt"></i></button>
                    <?  endif;
                        if ($type == "upcoming" || $type == "live"):    ?>
                            <button data-id="<?=$league->cid?>" data-leaguename="<?=$league->name?>" class="removeButton btn btn-link" data-toggle="tooltip" data-placement="top" title="Remove Contest"><i class="fas fa-trash-alt"></i></button>
                        <?  if ($league->numEntries > 0):   ?>
                            <a href="<?=base_url('admin/contest/export_entrants/'.$league->cid) ?>" class="exportButton btn btn-link" data-toggle="tooltip" data-placement="top" title="Export Contest Entrants"><i class="fas fa-file-export"></i></a>
                    <?      endif;
                        endif;  ?>
                        </td>
                <?  endif;  ?>
                    </tr>
            <?  endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="deleteConfirm" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-danger text-white">
				<h4 class="modal-title" id="myModalLabel">Delete Contest</h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
                <p>Are you sure you want to remove the <span id="leagueName" class="font-weight-bold"></span> Contest?</p>
                <p>All entries that have joined will have appropriate funds refunded.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<a id='actionURL' href="" class="btn btn-danger">Yes</a>
			</div>
		</div>
	</div>
</div>


<script>
jQuery(document).ready(function($){
    $('#table_list').DataTable({
        "order": [[ 0,"asc"]],  // Set Column # for Default Sort/Dir (zero based)
        "paging": true,
        "pageLength": 50,
        "language": {
            "searchPlaceholder": "Search Contests",
            "search": "",
            "lengthMenu": "Show _MENU_ Contests"
        }
    });

    $('#startDate').datetimepicker({
        format: 'L',
    });
    $('#endDate').datetimepicker({
        format: 'L',
    });

    $('.removeButton').on('click',function(){
        var id = $(this).data('id');
        var leagueName = $(this).data('leaguename');

        $('#leagueName').text(leagueName);
        $('#actionURL').prop('href',HOST_NAME+'admin/contest/remove_game/'+id);

        $('#deleteConfirm').modal('show');
    });

});
</script>