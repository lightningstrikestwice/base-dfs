                            <div class="checked-state sport-type">
                    <?  foreach ($sports as $sport):
                            $sel = in_array($sport, $selSports) ? " checked" : "";

                            $icon = $this->Misc->get_sport_icon($sport);    ?>
                            <input type="<?=$fieldType ?>" id="radio_<?=$sport ?>" name="sport[]" value="<?=$sport?>"<?=$sel?>/>
                            <label for="radio_<?=$sport ?>">
                                <span class="option-box"><i class="<?=$icon?>"></i></span>
                                <span class="inner-text"><?=$sport?></span>
                            </label>
                    <?  endforeach; ?>
                            </div>
