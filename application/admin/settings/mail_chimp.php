<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle?></h1>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<? $this->load->view(THEME.'/alerts');  ?>

<div class="card default">
    <div class="card-header">
        Instructions
    </div>
    <div class="card-body">
        This information will likely not change. However, you can set the API key details as well as the list option to use directly from mailchimp.com using this tool.
    </div>
</div>

<div class="card default">
    <div class="card-body">
    <?= form_open() ?>
    <?  foreach ($settings as $setting):    ?>
        <div class="form-group row">
            <label for="<?= $setting->name ?>" class="col-12 col-sm-4 col-md-2"><?= $setting->nice_name ?></label>
            <div class="col-12 col-sm-8 col-md-10">
            <?  if ($setting->name == "mc_active_lists"):
                    $lists = json_decode($setting->value,true);
                    $c = 0;
                    foreach ($lists as $listID=>$listName):
                        $c++;   ?>
                        <div class="form-group row">
                            <div class="col-12 col-sm-3">
                            <?  if ($c == 1):   ?>
                                    <label>List ID</label>
                            <?  endif;  ?>
                                <input id="<?= $setting->name ?>_<?=$listID?>" class="form-control" type="text" name="list_id[]" value="<?= $listID ?>" />
                            </div>
                            <div class="col-12 col-sm-9">
                            <?  if ($c == 1):   ?>
                                    <label>List Name</label>
                            <?  endif;  ?>
                                <input id="<?= $setting->name ?>_<?=$listID?>_name" class="form-control" type="text" name="list_name[]" value="<?= $listName ?>" />
                            </div>
                        </div>
                <?  endforeach;
                    if ($lists):    ?>
                        <p class="help-block">To remove a list, just empty the fields</p>
                <?  endif;  ?>
                    <div class="form-group row">
                        <div class="col-12 col-sm-3">
                            <label>New List ID</label>
                            <input id="<?= $setting->name ?>_newID1" class="form-control" type="text" name="list_id[]" value="" />
                        </div>
                        <div class="col-12 col-sm-9">
                            <label>New List Name</label>
                            <input id="<?= $setting->name ?>_newID1_name" class="form-control" type="text" name="list_name[]" value="" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-sm-3">
                            <input id="<?= $setting->name ?>_newID2" class="form-control" type="text" name="list_id[]" value="" />
                        </div>
                        <div class="col-12 col-sm-9">
                            <input id="<?= $setting->name ?>_newID2_name" class="form-control" type="text" name="list_name[]" value="" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-sm-3">
                            <input id="<?= $setting->name ?>_newID3" class="form-control" type="text" name="list_id[]" value="" />
                        </div>
                        <div class="col-12 col-sm-9">
                            <input id="<?= $setting->name ?>_newID3_name" class="form-control" type="text" name="list_name[]" value="" />
                        </div>
                    </div>
                    <p class="help-block mb-0">Users will be able to subscribe to and unsubscribe from these lists from their email settings page.</p>
                    <p class="help-block">Leave New List fields blank, if you aren't adding any new lists</p>
            <?  elseif ($setting->name == "mc_main_list"): ?>
					<select class="form-control" name="<?= $setting->name ?>">
                        <option selected disabled>-- Choose a List --</option>
                    <?  foreach($activeLists as $id=>$name):
                            $sel = $id == $setting->value ? " selected" : "";     ?>
							<option value="<?=$id?>"<?= $sel ?>><?=$name?></option>
					<?  endforeach;?>
					</select>
                    <p class="help-block">This is the list users will be asked about on the registration page</p>
            <?  else:  ?>
                    <input id="<?= $setting->name ?>" class="form-control" type="text" name="<?= $setting->name ?>" value="<?= $setting->value ?>" />
                    <p class="help-block">Set this once and leave it alone</p>
            <?  endif;  ?>
            </div>
        </div>
    <?  endforeach; ?>
        <button type="submit" class="btn btn-primary">Save Changes</button>
    <?= form_close() ?>
    </div>
</div>



