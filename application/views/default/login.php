<section class="content-wrap">

	<div class="container">

		<div class="row">

			<div class="col-md-6 login">

				<h1 class="title">Login to <?= $siteOptions['website_name'] ?></h1>
                <? $this->load->view(THEME.'/alerts') ?>

				<p>Login using your profile details below.</p>
                <?= validation_errors() ?>
				<?=form_open() ?>
					<div class="form-group">
						<label for="username">Username or Email</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username or Email">
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="Password">
					</div>
					<div class="text-center">
						<button type="submit" class="btn-primary btn">Login</button>
						<a href="#" class="btn btn-default">Forgot Password</a>
					</div>
				<?=form_close() ?>

			</div>

            <div class="col-md-6 register bgImage" style="background-image: url(<?=site_url('assets/'.THEME.'/img/field_red.jpg') ?>);">

				<div class="text-center">
					<img class="logo" src="<?=site_url('assets/'.THEME.'/img/'.$siteOptions['logo']) ?>">
				</div>

				<h1>New to <?= $siteOptions['website_name'] ?>?</h1>
				<p>Creating an account is fast, free, and easy. <br> Learn <a href="#">How it Works</a> or register below.</p>

				<div class="text-center">
					<a href="<?= base_url('register') ?>" class="btn-default btn">Sign Me Up</a>
				</div>

			</div>

		</div>

	</div>

</section>
