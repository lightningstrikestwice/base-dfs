<?
$disabled = $user->avatar == "" ? " disabled" : "";
?>
<section class="content-wrap">
	<div class="container registration">
		<h1 class="title">Withdrawal Request</h1>
        <? $this->load->view(THEME.'/alerts'); ?>

        <div class="row">
			<div class="col-md-3">
				<? $this->load->view(THEME.'/my_account/sidebar'); ?>
			</div>

			<div class="col-md-9">
				<h3><strong>User Funds: <?=$siteOptions['currency'].$userBalance ?></strong></h3>
                <?= validation_errors() ?>
            <?  if ($userBalance < $genOptions['min_withdrawal_amount']):   ?>
                    <div class="alert alert-danger"><i class="fas fa-exclamation-triangle"></i>&nbsp;&nbsp;Your balance does not meet the minimum requirement of <?=$siteOptions['currency'].number_format($genOptions['min_withdrawal_amount'],2,".",",") ?> to request a withdrawal.</div>
            <?  endif;  ?>
				<form method="post" action="">

					<div class="form-group ">
						<label><strong>Withdraw Amount</strong></label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><?=$siteOptions['currency'] ?></span>
							</div>
							<input type="text" name="amount" class="form-control" placeholder="Ex. 100"  value="">
						</div>
					</div>

					<div class="form-group ">
                        <label class="d-block mb-0"><strong>Payment Method</strong></label>
                <?  $c = 0;
                    foreach ($methods as $id=>$method):
                        $c++;
                        $chk = $c == 1 ? " checked" : "";   ?>
						<div class="form-check form-check-inline mr-4">
							<input class="form-check-input payment_method" type="radio" name="payment_method" id="inlineRadio<?=$id?>" value="<?=$id?>"<?=$chk ?>>
                            <label class="form-check-label withdrawal" for="inlineRadio<?=$id?>"><?= ucfirst($method)?></label>
						</div>
                <?  endforeach; ?>
					</div>

					<div id="extra_info_0" class="form-group extra_info ">
						<div class="form-group ">
							<label><strong>Paypal Email</strong></label>
							<input type="text" class="form-control" name="paypal_email" placeholder="" value=''>
							<span class="help-block">Email address that we'll send the money to via PayPal.</span>
						</div>
					</div>

                    <div id="extra_info_1" class="extra_info" style="display:none;">
						<div class="form-group ">
							<label>Check Made Out To:</label>
							<input type="text" class="form-control" name="check_name" placeholder="" value="<?=$user->first_name ?> <?=$user->last_name ?>" placeholder="Name" />
						</div>
						<div class="form-group ">
							<label>Address to Mail To:</label>
							<div class="form-row">
								<div class="form-group col-md-12">
									<input type="text" class="form-control" name="check_address[address]" value="<?=$user->address?>" placeholder="Address"/>
								</div>
								<div class="form-group col-md-7">
									<label>City</label>
									<input type="text" class="form-control" name="check_address[city]" value="<?=$user->city ?>" placeholder="City"/>
								</div>
								<div class="form-group col-md-5">
									<label for="inputZip">Zip/Postal Code</label>
									<input type="text" class="form-control" name="check_address[zip]" value="<?=$user->zip ?>" placeholder="Zip/Postal Code"/>
								</div>
							</div>
						</div>
					</div>

                    <div id="extra_info_2" class="form-group extra_info" style="display:none;">
						<div class="form-group ">
							<label><strong>Bitcoin Address</strong></label>
							<input type="text" class="form-control" name="bitcoin_address" placeholder="" value=''>
							<span class="help-block">Address that we'll send the money to via Bitcoin</span>
						</div>
					</div>

					<div class="text-center">
                    <?  $disabled = $genOptions['min_withdrawal_amount'] > $userBalance ? " disabled" : ""; ?>
						<button type="submit" class="btn btn-primary<?=$disabled ?>">Submit</button>
					</div>

				</form>
			</div>
		</div>
        <div class="mt-5">
            <h5><strong>Your Recent Withdrawals</strong></h5>
            <div class="withdraw-table table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Method</th>
                            <th scope="col">Status</th>
                            <th scope="col">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                <?  foreach ($requests as $request):
                        $class = $statuses[$request->status]['class'];
                        $status = $statuses[$request->status]['desc']; ?>
                        <tr>
                            <td><?= date($genOptions['date_format'],$request->req_timestamp) ?></td>
                            <td>
                                <div><?= ucfirst($methods[$request->method]) ?></div>
                                <div><?= $request->info ?></div>
                            </td>
                            <td class="text-<?=$class?> font-weight-bold"><?=$status ?></td>
                            <td><?=$siteOptions['currency'].number_format($request->amount,2,".",",") ?></td>
                        </tr>
                <?  endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="withdraw-text mt-5">
            <p><strong>Minimum Withdrawal Amount: <?=$siteOptions['currency'].number_format($genOptions['min_withdrawal_amount'],2,".",",") ?></strong></p>
            <p>Please do not place requests for amounts lower than the value above, as per our terms and conditions. Your request may be ignored.</p>
            <p>Choose if you would like to be paid by PayPal (if you are a user with a PayPal Account) or by check. All withdrawal requests will be processed within 48 hours or the next business day if the request is received over the weekend. We may perform additional checks if the PayPal account is not one you have deposited from.</p>
            <p>Furthermore, all funds are paid out in CAD.</p>
            <p>For withdrawals of <?=$siteOptions['currency'] ?>250 or more we may request that you provide your social security number before the withdrawal is processed - this helps in the event that your annual net winnings may exceed $600 and we are required to file a 1099-MISC tax form. </p>
            <p>We conduct anti-fraud checks on playing patterns and deposits prior to processing all withdrawals so may request additional information before your withdrawal is approved. </p>
        </div>

	</div>
</section>

<script type="text/javascript">
$(function(){
	$('input[name="payment_method"]').on('change',function(e){
        var method = $(this).val();
		$(".extra_info").hide();
		$("#extra_info_"+method).fadeIn("fast");
	});

    var cMethod = $('input[name="payment_method"]:checked').val();
    $(".extra_info").hide();
    $("#extra_info_"+cMethod).fadeIn("fast");
});
</script>