<?
$disabled = $user->avatar == "" ? " disabled" : "";
?>
<section class="content-wrap">
	<div class="container registration">
		<h1 class="title">Add/Update Profile Photo</h1>
        <? $this->load->view(THEME.'/alerts'); ?>

        <div class="row">
			<div class="col-md-3">
				<? $this->load->view(THEME.'/my_account/sidebar'); ?>
			</div>

			<div class="col-md-9">
                <div>
                    <h3>Current Profile Photo</h3>
                    <img src="<?= $this->Misc->get_avatar_url($user->cid) ?>" />
                    <a href="<?=base_url('my_account/delete_avatar')?>" class="btn btn-default float-right<?=$disabled ?>"<?=$disabled ?>>Delete Existing Picture</a>
                </div>
                <hr />
                <div class="full-fix image-upload">
                    <div class="clearfix">
                        <div id="avatar_upload_wrap">
                            <div id="avatar_image_zone" class="clearfix">
                                <div id="avatar_drag_drop">
                                    Drop Image File Here<br />
                                    <em>Or</em><br />
                                    <button id="browse_files" class="btn btn-primary btn-large">Browse Files</button>
                                </div>
                            </div>
                            <div id="media-items"></div>
                        </div>
                    </div>
                    <?= form_open('register/crop_avatar/0',array('id'=>'cropform'))   ?>
                        <input type="hidden" id="temp_image" name="temp_image" />
                        <input type="hidden" id="edit" name="edit" value="1"/>
                        <input type="hidden" id="x" name="x" />
                        <input type="hidden" id="y" name="y" />
                        <input type="hidden" id="w" name="w" />
                        <input type="hidden" id="h" name="h" />
                        <div class="form-actions mt-3">
                            <input type='submit' id="avatar_save" name='submit' class="btn btn-primary submit" value='Save Image' />
                        </div>
                    <?= form_close() ?>

                </div>
                <div id="avatar_preview_wrapper">
                    <h3>Preview</h3>
                    <p>Drop an image in the area to the right (above if on mobile) and crop it to set your primary picture. File must be .jpg or .png and must be smaller than 2mb.</p>
                    <div id="avatar_preview2" style="width:200px;height:200px;">
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>
