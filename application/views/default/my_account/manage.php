<section class="content-wrap">

	<div class="container registration">

		<h1 class="title">Manage Account</h1>
        <? $this->load->view(THEME.'/alerts'); ?>
		<div class="row">

			<div class="col-md-3">

				<? $this->load->view(THEME.'/my_account/sidebar'); ?>

			</div>

			<div class="col-md-9">
			<?= form_open() ?>
                    <input type="hidden" name="cid" value="<?=$user->cid ?>" />
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="email">Email</label>
							<input type="email" name="email" value="<?= $user->email ?>" class="form-control" id="email">
                            <?= form_error('email') ?>
						</div>
						<div class="form-group col-md-6">
							<label for="phone">Phone Number</label>
							<input type="tel" name="phone" value="<?= $user->phone ?>" class="form-control" id="phone" aria-describedby="phoneHelp">
                            <?= form_error('phone') ?>
							<small id="phoneHelp" class="form-text text-muted">100% Confidential. Necessary to verify cash withdrawals.</small>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="password">Password</label>
							<input type="password" name="password" class="form-control" id="password">
                            <?= form_error('password') ?>
                            <small id="pwHelp" class="form-text text-muted">Leave blank if you don't want to change your password</small>
						</div>
						<div class="form-group col-md-6">
							<label for="confirmPassword">Confirm Password</label>
                            <input type="password" name="password_confirm" class="form-control" id="confirmPassword">
                            <?= form_error('password_confirm') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="firstName">First Name</label>
							<input type="text" name="first_name" value="<?= $user->first_name ?>" class="form-control" id="firstName">
                            <?= form_error('first_name') ?>
						</div>
						<div class="form-group col-md-6">
							<label for="lastName">Last Name</label>
							<input type="text" name="last_name" value="<?= $user->last_name ?>" class="form-control" id="lastName">
                            <?= form_error('last_name') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="address">Address</label>
							<input type="text" name="address" value="<?= $user->address ?>" class="form-control" id="address">
                            <?= form_error('address') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12">
							<small id="provinceHelp" class="form-text text-muted">Contest participation is 100% restricted to residents of Canada at this time.</small>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="city">City</label>
							<input type="text" name="city" value="<?= $user->city ?>" class="form-control" id="city">
                            <?= form_error('city') ?>
						</div>
						<div class="form-group col-md-6">
							<label for="province">Province</label>
							<select class="form-control" name="state" id="province" aria-describedby="provinceHelp">
								<option value="">Select One</option>
                        <?  foreach ($provinces as $abbr=>$name):
                                $sel = $abbr == $user->state ? " selected" : "";   ?>
                                <option value="<?=$abbr?>"<?=$sel?>><?=$name ?></option>
                        <?  endforeach; ?>
							</select>
                            <?= form_error('state') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="postalCode">Postal Code</label>
							<input type="text" name="zip" value="<?= $user->zip ?>" class="form-control" id="postalCode">
						</div>
						<div class="form-group col-md-6">
							<label for="country">Country</label>
							<select class="form-control" name="country" id="country">
								<option value="">Select One</option>
                        <?  foreach ($countries as $abbr=>$name):
                                $sel = $abbr == $user->country ? " selected" : "";   ?>
                                <option value="<?=$abbr?>"<?=$sel?>><?=$name ?></option>
                        <?  endforeach; ?>
							</select>
                            <?= form_error('country') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12">
							<label for="month">Date of Birth</label>
						</div>
                    <?  $dob = json_decode($user->dob,true);    ?>
						<div class="form-group col-md-4">
							<select class="form-control" name="month" id="month">
                        <?  foreach ($months as $k=>$mon):
                                $sel = $k == $dob['month'] ? " selected" : "";   ?>
                                <option value="<?=$k?>"<?=$sel?>><?=$mon?></option>
                        <?  endforeach; ?>
							</select>
						</div>
						<div class="form-group col-md-4">
							<select class="form-control col" name="date" id="day">
						<?  for($d=1; $d<=31; $d++):
                                $sel = $d == $dob['date'] ? " selected" : "";   ?>
								<option<?=$sel?>><?=$d;?></option>
                        <?  endfor; ?>
							</select>
						</div>
						<div class="form-group col-md-4">
							<?php
							$year = date('Y');
							$me = strtotime($year.' -18 years');
							$y = date('Y',$me);
							$old = strtotime($year.' -100 years');
							$o = date('Y',$old);?>

                            <select class="form-control col" name="year" id="year">
						<?  for($yr=$y; $yr>=$o; $yr--):
                                $sel = $yr == $dob['year'] ? " selected" : "";?>
								<option<?=$sel?>><?= $yr?></option>
						<?  endfor; ?>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="gender">Gender</label>
							<select class="form-control" name="gender" id="gender">
                                <option value="0">Choose One</option>
                        <?  $genders = array("M" => "Male", "F"=>"Female");

                            foreach ($genders as $abbr=>$gen):
                                $sel = $abbr == $user->gender ? " selected" : "";   ?>
                                <option value="<?=$abbr?>"<?=$sel?>><?=$gen?></option>
                        <?  endforeach; ?>
							</select>
						</div>
					</div>

					<div class="text-center">
						<button type="submit" class="btn-primary btn">Save</button>
					</div>
			<?= form_close() ?>

			</div>

		</div>

	</div>

</section>
