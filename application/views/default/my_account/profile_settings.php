
<section class="content-wrap">

	<div class="container registration">

		<h1 class="title">Profile Settings</h1>
        <? $this->load->view(THEME.'/alerts'); ?>
		<div class="row">

			<div class="col-md-3">

				<? $this->load->view(THEME.'/my_account/sidebar'); ?>

			</div>

			<div class="col-md-9">
			<?= form_open() ?>
                    <input type="hidden" name="cid" value="<?=$user->cid ?>" />
                    <input type="hidden" name="profile_settings[]" value="system" />

					<div class="form-check">
                    <?  $chk = in_array("request_confirmation", $profileSettings) ? " checked" : "";    ?>
						<input class="form-check-input" type="checkbox" name="profile_settings[]" value="request_confirmation" id="check1"<?= $chk ?>>
						<label class="form-check-label" for="check1">
							Email me withdrawal request confirmations.
						</label>
					</div>
					<div class="form-check">
                    <?  $chk = in_array("request_fulfilled", $profileSettings) ? " checked" : "";    ?>
						<input class="form-check-input" type="checkbox" name="profile_settings[]" value="request_fulfilled" id="check2"<?= $chk ?>>
						<label class="form-check-label" for="check2">
							Email me when the admin marks withdrawals fulfilled.
						</label>
					</div>
					<div class="form-check">
                    <?  $chk = in_array("game_results", $profileSettings) ? " checked" : "";    ?>
						<input class="form-check-input" type="checkbox" name="profile_settings[]" value="game_results" id="check3"<?= $chk ?>>
						<label class="form-check-label" for="check3">
							Email me about contest results when a contest concludes.
						</label>
					</div>
					<div class="form-check">
                    <?  $chk = in_array("expired_games", $profileSettings) ? " checked" : "";    ?>
						<input class="form-check-input" type="checkbox" name="profile_settings[]" value="expired_games" id="check4"<?= $chk ?>>
						<label class="form-check-label" for="check4">
							Email me about expired contests that did not fill.
						</label>
					</div>
					<div class="form-check">
                    <?  $chk = in_array("game_removal", $profileSettings) ? " checked" : "";    ?>
						<input class="form-check-input" type="checkbox" name="profile_settings[]" value="game_removal" id="check5"<?= $chk ?>>
						<label class="form-check-label" for="check5">
							Email me if the admin deletes a contest I am part of.
						</label>
					</div>

                <?  foreach(json_decode($mcLists,true) as $id=>$name):
                        $mcInfo = $this->Mailchimp->lists_member_info($id,$user->email);
                        $status = isset($mcInfo->data[0]->status) ? $mcInfo->data[0]->status : "";
                        $chk = "";
                        if (isset($mcInfo->data[0]->status) && $mcInfo->data[0]->status == "subscribed") {$chk = " checked";}
                        ?>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="mc_sett[]" value="<?=$id?>" id="check6"<?=$chk?>>
                            <label class="form-check-label" for="check6">
                                Include me in the <strong><?=$name?></strong> Newsletter.
                            </label>
                <?      if ($status == "pending"):  ?>
                            <p class="help-block">Your subscription is still processing.  This message will disappear once it has been finalized.</p>
                <?      endif;      ?>
                        </div>

                <?  endforeach;?>

					<div class="text-center">
						<button type="submit" class="btn-primary btn">Save Settings</button>
					</div>
			<?= form_close() ?>

			</div>

		</div>

	</div>

</section>
