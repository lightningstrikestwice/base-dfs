<section class="content-wrap">

	<div class="container">

        <h1 class="title"><?= $pageTitle ?></h1>

        <?= $body ?>

	</div>

</section>