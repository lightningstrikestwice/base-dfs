<section class="content-wrap">

	<div class="container">

		<h1 class="title">Support</h1>

        <? $this->load->view(THEME.'/alerts'); ?>

        <div class="row">

			<div class="col-sm-5">
				<p>Need support from the <?=$siteOptions['website_name'] ?> team?</p>

				<p>Submit a support ticket now.</p>

				<p>Questions, suggestions, or support requirements can be handled by filling out the form on this page. Please be as specific as possible so we can investigate your concerns.</p>

				<p>Thank you for playing on <?=$siteOptions['website_name'] ?>.</p>

				<a href="<?=base_url('faq') ?>" class="btn btn-primary">View FAQs</a>
			</div>

			<div class="col-sm-7">

				<form action="" method="post" class="form-horizontal support-form">
					<div class="form-group">
						<label>Your Username</label>
						<input type="text" name="username" class="form-control" value="<?=$user->username ?>" />
					</div>
					<div class="form-group">
						<label>Your Name</label>
						<input type="text" name="name" class="form-control" value="<?=$user->first_name ?> <?=$user->last_name ?>" />
					</div>
					<div class="form-group">
						<label>Your Email</label>
						<input type="text" name="email" class="form-control" value="<?=$user->email ?>" />
					</div>
					<div class="form-group">
						<label>Your Phone</label>
						<input type="text" name="phone" class="form-control" value="<?=$user->phone ?>" />
					</div>
					<div class="form-group">
						<label>Subject</label>
						<select name="subject" class="form-control">
<!--                            <option value="Contest Creation" >Contest Creation</option>
                            <option value="Withdrawals">Withdrawals</option>
                            <option value="Refunds">Refunds</option>-->
                            <option value="Gameplay Feedback &amp; Bugs" selected>Gameplay Feedback &amp; Bugs</option>
                            <option value="Member Abuse">Member Abuse</option>
                            <!--<option value="Contest Cancellation">Contest Cancellation</option>-->
                            <option value="Other">Other</option>
						</select>
					</div>
					<div class="form-group">
						<label>Your Message</label>
						<textarea class="form-control" name="message"></textarea>
					</div>

					<div class="text-center">
						<input type="submit" class="btn btn-primary" value="Send Ticket" />
					</div>
				</form>

			</div>

		</div>

	</div>

</section>
