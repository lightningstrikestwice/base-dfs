            <div class="top-nav">
                <div class="navbar-wrap">
                    <ul class="nav justify-content-end">
                        <li class="nav-item">
                            <a href="<?= base_url('register') ?>" class="btn btn-default">Sign Up</a>
                        </li>
                    </ul>
                </div>
            </div>

            <nav class="navbar navbar-expand-md overlay-nav">
                <a class="navbar-brand" href="<?= base_url() ?>">
                    <img src="<?=site_url('assets/'.THEME.'/img/'.$siteOptions['logo']) ?>">
                </a>
                <div class="navbar-wrap">
                    <ul class="nav justify-content-end">
                        <li class="nav-item">
                            <a href="<?= base_url() ?>" class="nav-link active">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url() ?>/login" class="nav-link"><i class="fas fa-sign-in"></i> Log In</a>
                        </li>
                    </ul>
                </div>
                <button class="toggle-menu menu-left ml-auto" role="button" type="button"><i class="far fa-bars"></i></button>
            </nav>

            <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">
                <a class="navbar-brand" href="<?= base_url() ?>">
                    <img src="https://via.placeholder.com/180x120.png?text=Site+Logo" alt="<?=$siteOptions['website_name'] ?>" />
                </a>
                <ul>
                    <li>
                        <a href="<?= base_url() ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>/register">Sign Up</a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>/login">Log In</a>
                    </li>
                </ul>
            </nav>
