<?php
	//show error/success messages
	$icon = $color = "";

    if ($this->session->flashdata('success') != '') {
        $icon = 'thumbs-up';
        $color = 'success';
        $alert = $this->session->flashdata('success');
    } else if ($this->session->flashdata('error') != '') {
        $icon = 'exclamation-triangle';
        $color = 'danger';
        $alert = $this->session->flashdata('error');
    }

    if ($this->session->flashdata('success') != '' || $this->session->flashdata('error') != ''):    ?>
        <div class="alert alert-<?= $color ?>">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            <i class="fas fa-<?= $icon ?>"></i>&nbsp;&nbsp;<?= $alert ?>
        </div>
<?  endif;  ?>
