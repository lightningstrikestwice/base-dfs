        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" crossorigin="anonymous">
        <!--<link rel="stylesheet" href="<?=site_url('assets/'.GLOBAL_CONTENT.'/font-awesome/css/fa-svg-with-js.css') ?>">-->
        <link rel="stylesheet" href="<?=site_url('assets/'.GLOBAL_CONTENT.'/flaticon/flaticon.css') ?>">
        <link rel="stylesheet" href="<?=site_url('assets/'.GLOBAL_CONTENT.'/css/jPushMenu.css') ?>" />
        <link rel="stylesheet" href="<?=site_url('assets/'.GLOBAL_CONTENT.'/slick/slick.css') ?>">

        <link rel="stylesheet" href="<?=site_url('assets/'.THEME.'/css/style.css') ?>">
        <link rel="stylesheet" href="<?=site_url('assets/'.THEME.'/css/responsive.css') ?>">

    <?  if(isset($styles) && !empty($styles)):
            foreach ($styles as $style):  ?>
                <link href="<?=$style?>" rel="stylesheet">
    <?      endforeach;
        endif;  ?>
