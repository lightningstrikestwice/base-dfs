<section class="content-wrap pb-2">

	<div class="container">
        <h1 class="title"><?= $pageTitle ?></h1>
	</div>

</section>


<?  $gray = "";
    foreach ($faqs as $faq):
        $gray = $gray == "" ? " gray-bg" : "";  ?>

        <section class="content-wrap<?=$gray?>">

            <div class="container">

                <h4><?= $faq->question ?></h4>

                <p><?= $faq->answer ?></p>
            </div>

        </section>

<?  endforeach; ?>
