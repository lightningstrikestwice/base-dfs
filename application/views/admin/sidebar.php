<?
/*
Add class "active" to the <a> for obvious reason.

Adding Single Menu Items
<li class="submenu">
    <a class="" href=""><i class="[fa icon]"></i><span class="pl-1"> [Menu Item] </span> </a>
</li>

Adding Multi Level Menu Items
<li class="submenu">
    <a href="#" class="<?= $sel[Menu]?>"><i class="[fa icon]"></i> <span class="pl-1"> [Menu Label] </span> <span class="menu-arrow fas fa-angle-right"></span></a>
    <ul class="list-unstyled">
        <li><a href="">[SubMenu Item #1]</a></li>
        <li><a href="">[SubMenu Item #2]</a></li>
    </ul>
</li>
*/


// set the link open
$url = $this->uri->uri_string;


$activeMenu = " active";
$selDashboard = $selUsers = $selCMS = $selContest = $selPayment = $selSupport = $selSettings = $selSalary = $selGames = "";

// Dashboard
if (strpos($url,'admin/dashboard') !== false || $url == "admin") {$selDashboard = $activeMenu;}
// Users
if (strpos($url,'admin/user') !== false) {$selUsers = $activeMenu;}
// CMS
if (strpos($url,'admin/content') !== false) {$selCMS = $activeMenu;}
// Games
if (strpos($url,'admin/contest') !== false) {$selGames = $activeMenu;}
// Salaries
if (strpos($url,'admin/salary') !== false) {$selSalary = $activeMenu;}
// Payments
if (strpos($url,'admin/payment') !== false) {$selPayment = $activeMenu;}
// Support
if (strpos($url,'admin/support') !== false) {$selSupport = $activeMenu;}
// Settings
if (strpos($url,'admin/setting') !== false) {$selSettings = $activeMenu;}


$components = $this->Options->get_group(5);
?>

    <!-- Left Sidebar -->
	<div class="left main-sidebar">
		<div class="sidebar-inner leftscroll">
			<div id="sidebar-menu">
    			<ul>
					<li class="submenu">
						<a class="<?=$selDashboard?>" href="<?=site_url('admin') ?>"><i class="fas fa-home"></i><span class="pl-1"> Dashboard </span> </a>
                    </li>
                    <li class="submenu">
                        <a class="<?=$selUsers?>" href="#"><i class="fas fa-users"></i> <span class="pl-1"> Users </span> <span class="menu-arrow fas fa-angle-right"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="<?=base_url() ?>admin/user/list_all">List All</a></li>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a class="<?=$selCMS?>" href="#"><i class="fas fa-edit"></i> <span class="pl-1"> CMS / Site Content </span> <span class="menu-arrow fas fa-angle-right"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="<?=base_url('admin/content/list_pages')?>">Pages</a></li>
                            <li><a href="<?=base_url('admin/content/list_emails')?>">Emails</a></li>
                            <li><a href="<?=base_url('admin/content/list_faqs')?>">FAQ's</a></li>
                        <?  if ($components['use_ticker'] == 1):   ?>
                            <li><a href="<?=base_url('admin/content/list_tickers')?>">News Ticker</a></li>
                        <?  endif;
                            if ($components['use_slider'] == 1):    ?>
                            <li><a href="<?=base_url('admin/content/list_slides')?>">Lobby Slider</a></li>
                        <?  endif;  ?>
                        </ul>
                    </li>
                    <li class="submenu">
                        <a class="<?=$selGames?>" href="#"><i class="fas fa-gamepad"></i> <span class="pl-1"> Contests </span> <span class="menu-arrow fas fa-angle-right"></span></a>
                        <ul class="list-unstyled">
                        <?  if ($components['use_auto_gen'] == 1):   ?>
                            <li><a href="<?=base_url('admin/contest/auto_contest') ?>">Auto Contest Generator</a></li>
                        <?  endif;  ?>
                            <li><a href="<?=base_url('admin/contest/featured_contest') ?>">Featured Contest Generator</a></li>
                            <li><a href="<?=base_url('admin/contest/manage') ?>">Manage Contests</a></li>
                        </ul>
                    </li>
                <?  if ($components['use_ecommerce'] == 1):    ?>
					<li class="submenu">
						<a class="<?=$selSalary?>" class="" href=""><i class="fas fa-money-bill"></i><span class="pl-1"> Manage Salaries </span> </a>
                    </li>
                <?  endif;  ?>
                    <li class="submenu">
                        <a class="<?=$selPayment?>" href="#"><i class="far fa-credit-card"></i> <span class="pl-1"> Payments </span> <span class="menu-arrow fas fa-angle-right"></span></a>
                        <ul class="list-unstyled">
                        <?  if ($components['use_ecommerce'] == 1):    ?>
                            <li><a href="<?=base_url('admin/payment_info/list_products') ?>">Products</a></li>
                            <li><a href="">Payment Method</a></li>
                            <li><a href="<?=base_url('admin/payment_info/view_log') ?>">Payment Log</a></li>
                        <?  endif;  ?>
                            <li><a href="<?=base_url('admin/payment_info/transaction_log') ?>">Transactions</a></li>
                            <li><a href="<?=base_url('admin/payment_info/withdrawal_requests') ?>">Withdrawal Requests</a></li>
                            <li><a href="<?=base_url('admin/payment_info/ytd_earnings') ?>">YTD Earnings</a></li>
                        </ul>
                    </li>
					<li class="submenu">
						<a class="<?=$selSupport?>" class="" href="<?=base_url('admin/support')?>"><i class="fas fa-ticket-alt"></i><span class="pl-1"> Support </span> </a>
                    </li>
                    <li class="submenu">
                        <a class="<?=$selSettings?>" href="#"><i class="fas fa-cogs"></i> <span class="pl-1"> Settings </span> <span class="menu-arrow fas fa-angle-right"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="<?=base_url('admin/setting') ?>">Site Settings</a></li>
                            <li><a href="<?=base_url('admin/setting/mail_chimp') ?>">Mailchimp Settings</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- End Sidebar -->
