<?
    $siteOptions = $this->Options->get_group(1);
?>

                    </div>
                    <!-- END container-fluid -->
                </div>
                <!-- END content -->
            </div>
            <!-- END content-page -->

            <footer class="footer">
                <span class="text-right">
                    Copyright <?=date("Y",time()) ?> <a target="_blank" href="<?=site_url() ?>"><?=$siteOptions['website_name'] ?></a>
                </span>
                <span class="design">
                    <a target="_blank" href="https://www.dreamcodesign.com">Web Design</a> by <a target="_blank" href="https://www.dreamcodesign.com"><strong>DreamCo Design</strong></a>
                </span>
            </footer>

        </div>
    </body>
</html>
