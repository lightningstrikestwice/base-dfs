<?
$version=$this->config->item('ftd_version');
$siteOptions = $this->Options->get_group(1);


$title = $pageTitle == "" ? "Admin" : $pageTitle;
$title .= " - " . $siteOptions['website_name']
?>
<!doctype html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="-1">

        <title><?= $title ?></title>
        <meta name='description' content='<?= $seoDesc ?>' />
        <meta name='keywords' content='<?= $seoKeywords ?>' />

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">

        <?
        $this->load->view(ADMIN_THEME.'/styles');
        $this->load->view(ADMIN_THEME.'/scripts');
        ?>
    </head>
    <body class="adminbody">
        <div id="main">
            <div class="headerbar">
                <div class="headerbar-left">
                    <a href="<?=base_url() ?>" class="logo"><img alt="Logo" src="<?=site_url('assets/'.THEME.'/img/'.$siteOptions['logo']) ?>" /></a>
                </div>

                <nav class="navbar-custom">
                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left pt-2">
                                <i class="fa fa-fw fa-bars"></i>
                            </button>
                        </li>
                    </ul>
                </nav>
            </div>

            <? $this->load->view(ADMIN_THEME.'/sidebar') ?>

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

        			<div class="container-fluid">




