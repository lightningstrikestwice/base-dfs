<script>
tinymce.init({
  selector: '#body_text',
  height: 400,
  theme: 'modern',
  plugins: 'autolink directionality codesample link hr pagebreak nonbreaking anchor lists textcolor wordcount colorpicker textpattern',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css']

});

    $(function(){
        $("form").on('submit',function(){
            tinymce.get('body_text').save();
        });
    });
</script>
