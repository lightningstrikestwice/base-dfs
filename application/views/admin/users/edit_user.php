<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left">Edit <?=$user->username ?></h1>
            <a href="javascript:history.go(-1)" class="float-right">Go Back</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class='col-12'>
        <? $this->load->view(THEME.'/alerts'); ?>
    </div>
    <div class="col-12 col-md-6">
        <div class="card default">
            <div class="card-header">
                <i class="fas fa-user pr-2"></i>User Info
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <img class="avatar" src="<?= $this->Misc->get_avatar_url($user->cid)?>" />
                    </div>
                    <div class="col-12 col-sm-8">
                        <div class="mb-1"><strong>Created:</strong> <?= date("m/d/Y g:iA",$user->join_date) ?></div>
                        <div class="mb-1"><strong>Reg IP:</strong> <?= $user->reg_ip ?></div>
                        <div class="mb-1"><strong>Last Login:</strong> <?= date("m/d/Y g:iA",$user->last_login) ?></div>
                        <div class="mb-1"><strong>Last IP:</strong> <?= $user->ip ?></div>
                        <div class="mb-1"><strong>Source:</strong> <?= $sources[$user->usersource] ?></div>
                        <div class="mb-1"><strong>Profits:</strong></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <h3>User Details</h3>
        <?= form_open('') ?>
        <input type="hidden" name="cid" value='<?=$user->cid?>' />
        <div class="form-row">
            <div class="form-group col-12 col-md-6">
            <?  $errorClass = !empty(form_error('username')) ? ' error' : '';   ?>
                <label for="username" class="<?= $errorClass ?>">Username:</label>
                <input type="text" class="form-control<?= $errorClass ?>" id="username" name="username" value="<?= $user->username ?>" placeholder="Username">
                <input name="existing_username" type="hidden" value="<?= $user->username ?>" />
                <?=form_error('username') ?>
            </div>
            <div class="form-group col-12 col-md-6">
            <?  $errorClass = !empty(form_error('email')) ? ' error' : '';   ?>
                <label for="email" class="<?= $errorClass ?>">Email:</label>
                <input type="email" class="form-control<?= $errorClass ?>" id="email" name="email" value="<?= $user->email ?>" placeholder="Email">
                <?=form_error('email') ?>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col">
            <?  $errorClass = !empty(form_error('password')) ? ' error' : '';   ?>
                <label for="password" class="<?= $errorClass ?>">Password:</label>
                <input type="text" class="form-control<?= $errorClass ?>" id="password" name="password" value="" placeholder="">
                <p class="help_block mb-0">(Leave blank to keep existing password)</p>
                <?=form_error('password') ?>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-12 col-md-6">
            <?  $errorClass = !empty(form_error('first_name')) ? ' error' : '';   ?>
                <label for="first_name" class="<?= $errorClass ?>">First Name:</label>
                <input class="form-control<?= $errorClass ?>" name="first_name" id="first_name" type="text" value="<?= $user->first_name ?>" />
                <?=form_error('first_name');?>
            </div>
            <div class="form-group col-12 col-md-6">
            <?  $errorClass = !empty(form_error('last_name')) ? ' error' : '';   ?>
                <label for="last_name" class="<?= $errorClass ?>">Last Name:</label>
                <input class="form-control<?= $errorClass ?>" name="last_name" id="last_name" type="text" value="<?= $user->last_name ?>" />
                <?=form_error('last_name');?>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-12 col-md-6">
            <?  $errorClass = !empty(form_error('address')) ? ' error' : '';   ?>
                <label for="address" class="<?= $errorClass ?>">Address:</label>
                <input class="form-control<?= $errorClass ?>" name="address" id="address" type="text" value="<?= $user->address ?>" />
                <?=form_error('address');?>
            </div>
            <div class="form-group col-12 col-md-6">
            <?  $errorClass = !empty(form_error('address2')) ? ' error' : '';   ?>
                <label for="address2" class="<?= $errorClass ?>">Address 2:</label>
                <input class="form-control<?= $errorClass ?>" name="address2" id="address2" type="text" value="<?= $user->address2 ?>" />
                <?=form_error('address2');?>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-12 col-md-3">
            <?  $errorClass = !empty(form_error('city')) ? ' error' : '';   ?>
                <label for="city" class="<?= $errorClass ?>">City:</label>
                <input class="form-control<?= $errorClass ?>" name="city" id="city" type="text" value="<?= $user->city ?>" />
                <?=form_error('city');?>
            </div>
            <div class="form-group col-12 col-md-3">
            <?  $errorClass = !empty(form_error('state')) ? ' error' : '';   ?>
                <label for="state" class="<?= $errorClass ?>">State/Province:</label>
                <select id="state" class="form-control<?= $errorClass ?>" name="state">
                    <option >--Choose State/Province</option>
                <?  foreach ($states as $abbr=>$state):
                        $sel = $abbr == $user->state ? " selected" : "";    ?>
                        <option value="<?=$abbr?>"<?=$sel?>><?=$state?></option>
                <?  endforeach; ?>
                </select>
                <?=form_error('state');?>
            </div>
            <div class="form-group col-12 col-md-3">
            <?  $errorClass = !empty(form_error('country')) ? ' error' : '';   ?>
                <label for="country" class="<?= $errorClass ?>">Country:</label>
                <select id="country" class="form-control<?= $errorClass ?>" name="country">
                    <option >--Choose Country --</option>
                <?  foreach ($countries as $abbr=>$country):
                        $sel = $abbr == $user->country ? " selected" : "";    ?>
                        <option value="<?=$abbr?>"<?=$sel?>><?=$country?></option>
                <?  endforeach; ?>
                </select>
                <?=form_error('state');?>
            </div>
            <div class="form-group col-12 col-md-3">
            <?  $errorClass = !empty(form_error('zip')) ? ' error' : '';   ?>
                <label for="zip" class="<?= $errorClass ?>">Zip:</label>
                <input class="form-control<?= $errorClass ?>" name="zip" id="zip" type="text" value="<?= $user->zip ?>" />
                <?=form_error('zip');?>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-12 col-md-6">
            <?  $errorClass = !empty(form_error('address')) ? ' error' : '';   ?>
                <label for="phone" class="<?= $errorClass ?>">Phone:</label>
                <input class="form-control<?= $errorClass ?>" name="phone" id="phone" type="text" value="<?= $user->phone ?>" />
                <?=form_error('phone');?>
            </div>
            <div class="form-group col-12 col-md-2">
                <label for="dob_month" class="<?= $errorClass ?>">Date of Birth:</label>
            <?  $errorClass = !empty(form_error('dob')) ? ' error' : '';
                $dob = json_decode($user->dob,true);    ?>
                <select id="dob_month" class="form-control<?= $errorClass ?>" name="dob[month]">
                    <option></option>
                <?  foreach ($months as $n=>$mon):
                        $sel = $n == $dob['month'] ? " selected" : "";    ?>
                        <option value="<?=$n?>"<?=$sel?>><?=$mon?></option>
                <?  endforeach; ?>
                </select>
                <?=form_error('dob');?>
            </div>
            <div class="form-group col-12 col-md-2">
                <label>&nbsp;</label>
                <select id="dob_day" class="form-control<?= $errorClass ?>" name="dob[date]">
                    <option></option>
                <?  for($i=1;$i<=31;$i++):
                        $sel = $i == $dob['date'] ? " selected" : "";    ?>
                        <option value="<?=$i?>"<?=$sel?>><?=$i?></option>
                <?  endfor; ?>
                </select>
            </div>
            <div class="form-group col-12 col-md-2">
                <label>&nbsp;</label>
                <select id="dob_month" class="form-control<?= $errorClass ?>" name="dob[year]">
                    <option></option>
                <?  for($i=date('Y')-18;$i>=(date('Y')-100);$i--):
                        $sel = $i == $dob['year'] ? " selected" : "";    ?>
                        <option value="<?=$i?>"<?=$sel?>><?=$i?></option>
                <?  endfor; ?>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-12 col-md-4">
                <div class="form-check">
                <?  $chk = $user->is_admin == 1 ? " checked" : "";  ?>
                    <input class="form-check-input" type="checkbox" value="1" name="is_admin" id="is_admin"<?=$chk?>>
                    <label class="form-check-label" for="is_admin">
                        Is a Site Admin
                    </label>
                </div>

            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
        <?= form_close() ?>
    </div>
</div>

