<div class="row">
    <div class="col-xl-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left">Users</h1>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="alert alert-success" role="alert">
    <h5 class="alert-heading">Instructions</h5>
    <p>Search for and sort users as desired. Use the tools beside each user name to edit the user, log in as them, view their transactions, and manage their funds.</p>
</div>
<div class="table-responsive-lg">
    <table class="table table-striped table-sm table-hover" id="table_list">
        <thead>
            <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Bonus Amount</th>
                <th>Promo</th>
                <th>Comment</th>
                <th>Balance</th>
                <th>Bonus</th>
            </tr>
        </thead>
        <tbody>
    <?  foreach ($transactions as $t):  ?>
            <tr>
                <td><?= $user->username ?></td>
                <td><?= $user->first_name ?></td>
                <td><?= $user->last_name ?></td>
                <td><?= $user->email ?></td>
                <td><?= $user->reg_ip ?></td>
                <td><?= date("m/d/y", $user->join_date) ?></td>
                <td><?= $status ?></td>
                <td>
                    <a href="<?=base_url()?>admin/user/user_log/<?=$userID?>" title="User Log" class="pr-1"><i class="fas fa-book"></i></a>
                    <a href="<?=base_url()?>admin/user/edit_user/<?=$userID?>" title="Edit User" class="pr-1"><i class="fas fa-user-edit"></i></a>
                    <a href="<?=base_url()?>admin/user/edit_funds/<?=$userID?>" title="Edit User Funds" class="pr-1"><i class="fas fa-wallet"></i></a>
                    <a href="<?=base_url()?>admin/login/login_as/<?=$userID?>" title="Login as <?=$user->username ?>"><i class="fas fa-sign-in-alt"></i></a>
                <?  if ($user->is_admin != 1):  ?>
                    <a href="javascript: if (confirm('Are you sure you want to delete this user?')) { window.location='<?=base_url?>/user/delete/<?=$userID?>' };" title="Delete <?=$user->username ?>"><i class="fas fa-trash-alt"></i></a>
                <?  endif;  ?>
                </td>
            </tr>
    <?  endforeach; ?>
        </tbody>
    </table>
</div>


<script>
jQuery(document).ready(function($){
    $('#table_list').DataTable({
        "order": [[ 0,"asc"]],  // Set Column # for Default Sort/Dir (zero based)
        "paging": true,
        "pageLength": 50,
        "language": {
            "searchPlaceholder": "Search Transactions",
            "search": "",
            "lengthMenu": "Show _MENU_ Transactions"
        },
        "columnDefs": [ {
            "targets": [7],     // Column Number to exclude from Sort (zero based)
            "orderable": false
            }]
    });
});
</script>