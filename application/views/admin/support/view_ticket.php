<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle ?></h1>
            <a href="<?=base_url('admin/support') ?>" class="float-right">View All Tickets</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class='col-12'>
        <? $this->load->view(THEME.'/alerts'); ?>
    </div>
    <div class="col-12">
        <div class="card default">
            <div class="card-header">
                <i class="fas fa-info-circle pr-2"></i>Instructions
            </div>
            <div class="card-body">
                <p>Manage and monitor user tickets from here</p>
            </div>
        </div>
    </div>
    <?=validation_errors() ?>
    <div class="col-12">
        <input type="hidden" name="cid" value="<?=$ticket->cid?>" />
        <div class="card default">
            <div class="card-header"><i class="fas fa-ticket-alt pr-2"></i><?=$ticket->subject?></div>
            <div class="card-body">
                <?= form_open() ?>
                <div class="row">
                    <div class="col-12 col-sm-6 bg-light">
                        <?=$ticket->message?>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group row mb-0">
                            <label for="ticket_name" class="col-3 col-sm-2 col-form-label font-weight-bold py-0">Name:</label>
                            <div class="col-9 col-sm-10">
                                <input type="text" readonly class="form-control-plaintext py-0" id="ticket_name" value="<?= $ticket->name ?>">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label for="ticket_email" class="col-3 col-sm-2 col-form-label font-weight-bold py-0">Email:</label>
                            <div class="col-9 col-sm-10">
                                <input type="text" readonly class="form-control-plaintext py-0" id="ticket_email" value="<?= $ticket->email ?>">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label for="ticket_phone" class="col-3 col-sm-2 col-form-label font-weight-bold py-0">Phone:</label>
                            <div class="col-9 col-sm-10">
                                <input type="text" readonly class="form-control-plaintext py-0" id="ticket_phone" value="<?= $ticket->phone ?>">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label for="ticket_date" class="col-3 col-sm-2 col-form-label font-weight-bold py-0">Date:</label>
                            <div class="col-9 col-sm-10">
                                <input type="text" readonly class="form-control-plaintext py-0" id="ticket_date" value="<?= date("m/d/Y g:iA",$ticket->timestamp) ?>">
                            </div>
                        </div>

                        <div class="form-group row mb-2 mt-3">
                            <label for="status" class="col-4 col-sm-3 col-form-label font-weight-bold">Status:</label>
                            <div class="col-8 col-sm-9">
                                <select class="form-control" id="status" name="status">
                            <?  for($x=1; $x<=3; $x++):
                                    $sel = $x == $ticket->status ? " selected" : "";    ?>
                                    <option value="<?=$x?>"<?=$sel?>><?= $statuses[$x]['status'] ?></option>
                            <?  endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="assigned" class="col-4 col-sm-3 col-form-label font-weight-bold">Assigned to:</label>
                            <div class="col-8 col-sm-9">
                                <select class="form-control" id="assigned" name="assigned_user_id">
                                    <option value="0" selected>Unassigned</option>
                            <?  foreach($admins as $aID=>$admin):
                                    $sel = $aID == $ticket->assigned_user_id ? " selected" : "";    ?>
                                    <option value="<?=$aID?>"<?=$sel?>><?= $admin->username ?></option>
                            <?  endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Update Ticket</button>
                    </div>
                </div>
                <?= form_close() ?>
                <hr class="border-top border-dark"/>
                <h5>Notes</h5>
                <div class="row">
                <?  if ($notes):    ?>
                    <div class="col-12">
                <?      foreach ($notes as $note):  ?>
                            <div class="media notes">
                                <img src="<?= $this->Misc->get_avatar_url($note->user_id)?>" width="50" class="align-self-start mr-3">
                                <div class="media-body">
                                    <div class="header">
                                        <?=$note->user->username ?> <span><?= date("m/d/Y g:iA", $note->timestamp) ?></span>
                                    </div>
                                    <div class="body">
                                        <?=$note->message ?>
                                    </div>
                                </div>
                            </div>
                <?      endforeach; ?>
                    </div>
                <?  endif;  ?>
                    <div class="col-12">
                    <?= form_open('admin/support/add_note') ?>
                        <input type="hidden" name="ticket_id" value="<?=$ticket->cid?>" />
                        <div class="bg-light mt-2">
                            <div class="form-group">
                                <label for="Add Note">Add Note</label>
                                <textarea class="form-control" rows="2" name="message"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Add Note</button>
                        </div>
                    <?= form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

