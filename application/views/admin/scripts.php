        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <script type="text/javascript" src="<?=site_url('assets/'.GLOBAL_CONTENT.'/font-awesome/js/all.min.js') ?>"></script>
        <script type="text/javascript" src="<?=site_url('assets/'.GLOBAL_CONTENT.'/js/modernizr.min.js') ?>"></script>
        <script type="text/javascript" src="<?=site_url('assets/'.GLOBAL_CONTENT.'/js/moment.min.js') ?>"></script>
        <script type="text/javascript" src="<?=site_url('assets/'.GLOBAL_CONTENT.'/js/detect.js') ?>"></script>
        <script type="text/javascript" src="<?=site_url('assets/'.GLOBAL_CONTENT.'/js/fastclick.js') ?>"></script>
        <script type="text/javascript" src="<?=site_url('assets/'.GLOBAL_CONTENT.'/js/jquery.blockUI.js') ?>"></script>
        <script type="text/javascript" src="<?=site_url('assets/'.GLOBAL_CONTENT.'/js/jquery.nicescroll.js') ?>"></script>

        <script type="text/javascript" src="<?=site_url('assets/'.ADMIN_THEME.'/js/admin.js');?>"></script>

    <?  if(isset($scripts) && !empty($scripts)):
            foreach ($scripts as $script):  ?>
                <script src="<?=$script?>"></script>
    <?      endforeach;
        endif;  ?>

        <script type="text/javascript">
            jQuery(document).ready(function($){
                localStorage.clear('/assets/<?=THEME;?>/');
                $('[data-toggle="tooltip"]').tooltip();
            });
            var HOST_NAME="<?=site_url('/');?>";
        </script>
