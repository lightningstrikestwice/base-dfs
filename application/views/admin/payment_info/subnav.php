<?
// set the link open
$url = $this->uri->uri_string;

$activeMenu = " active";
$selProducts = $selMethods = $selLog = "";

// Products
if (strpos($url,'admin/payment_info/list_products') !== false) {$selProducts = $activeMenu;}
// Methods
if (strpos($url,'admin/payment_info/list_methods') !== false) {$selMethods = $activeMenu;}
// Log
if (strpos($url,'admin/payment_info/view_log') !== false) {$selLog = $activeMenu;}

?>

<ul class="nav nav-pills default ml-2 mb-3">
    <li class="nav-item">
        <a class="nav-link<?=$selProducts?>" id="products" href="<?= base_url('admin/payment_info/list_products')?>">Products</a>
    </li>
    <li class="nav-item">
        <a class="nav-link<?=$selMethods?>" id="payment_method" href="<?= base_url('admin/payment_info/list_methods')?>">Payment Method</a>
    </li>
    <li class="nav-item">
        <a class="nav-link<?=$selLog?>" id="payment_log" href="<?= base_url('admin/payment_info/view_log')?>">Payment Log</a>
    </li>
</ul>
