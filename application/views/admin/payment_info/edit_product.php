
<? $this->load->view(ADMIN_THEME. '/tinymce');    ?>
<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle ?></h1>
            <a href="javascript:history.go(-1)" class="float-right">Go Back</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class='col-12'>
        <? $this->load->view(THEME.'/alerts'); ?>
    </div>
    <div class="col-12">
        <?= form_open() ?>
            <input type="hidden" name="cid" value="<?=$product->cid?>" />
            <div class="card default">
                <div class="card-header">Product Details</div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-12 col-sm-5">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?=$product->name?>">
                            <?= form_error('name') ?>
                        </div>
                        <div class="form-group col-12 col-sm-2">
                            <label for="price">Price</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="text" class="form-control" id="price" name="price" value="<?=$product->price?>">
                            </div>
                            <?= form_error('price') ?>
                        </div>
                        <div class="form-group col-12 col-sm-5">
                            <label for="order">Order</label>
                            <input type="text" class="form-control" id="order" name="order" value="<?=$product->order?>">
                            <p class="help-block mb-0">This should be ordered by 10's to allow for changes in the future.</p>
                            <?= form_error('order') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description" name="description" value="<?=$product->description?>">
                        <?= form_error('description') ?>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save Product</button>
        <?= form_close() ?>
        </div>
    </div>
</div>

