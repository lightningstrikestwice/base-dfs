<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle?></h1>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="card default">
    <div class="card-header">
        Instructions
    </div>
    <div class="card-body">

    </div>
</div>

<?  $this->load->view(ADMIN_THEME.'/payment_info/subnav');  ?>

<div class="card default">
    <div class="card-header">
        <div class="time_window">
            Show Payments From:
            <input type="hidden" id="current_timestamp_limit" value="" />
            <select id="contests_from">
        <?  foreach ($timeWindows as $time=>$desc):
                $sel = $timeWindow == $time ? " selected" : ""; ?>
                <option value="<?=$time?>"<?=$sel?>><?=$desc ?></option>
        <?  endforeach; ?>
            </select>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive-lg">
            <table class="table table-striped table-sm table-hover" id="table_list">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>User ID</th>
                        <th>User</th>
                        <th>Name</th>
                        <th>Product</th>
                        <th>Amount</th>
                        <th>Transaction ID</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
            <?  foreach ($payments as $payment):?>
                    <tr>
                        <td><?= $payment->cid ?></td>
                        <td><?= date("m/d/y g:iA", $payment->time) ?></td>
                        <td><?= $payment->user_id ?></td>
                        <td><?= $payment->user->username ?></td>
                        <td><?= $payment->user->full_name ?></td>
                        <td><?= $payment->product->name ?></td>
                        <td><?= $payment->amount ?></td>
                        <td><?= $payment->trans_id ?></td>
                        <td class="text-center"></td>
                    </tr>
            <?  endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

<script>
jQuery(document).ready(function($){
    $('#table_list').DataTable({
        "order": [[ 0,"desc"]],  // Set Column # for Default Sort/Dir (zero based)
        "paging": true,
        "pageLength": 50,
        "language": {
            "searchPlaceholder": "Search Payments",
            "search": "",
            "lengthMenu": "Show _MENU_ Payments"
        },
        "columnDefs": [ {
            "targets": [8],     // Column Number to exclude from Sort (zero based)
            "orderable": false
            }]
    });
});
</script>