<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle?></h1>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<? $this->load->view(THEME.'/alerts');  ?>

<div class="card default">
    <div class="card-header">
        Instructions
    </div>
    <div class="card-body">
        This site settings tool can be used to set various items for your site. These settings should not be changed often and only as needed.
    </div>
</div>

<div class="card default">
    <div class="card-body">
    <?= form_open() ?>

    <?  foreach ($settings as $setting):    ?>
        <div class="form-group row">
            <label for="<?= $setting->name ?>" class="col-12 col-sm-4 col-md-2"><?= $setting->nice_name ?></label>
            <div class="col-12 col-sm-8 col-md-10">
            <?  if ($setting->type == "text"):  ?>
                    <input id="<?= $setting->name ?>" class="form-control" type="text" name="<?= $setting->name ?>" value="<?= $setting->value ?>" />
            <?  elseif ($setting->type == "select"):    ?>
					<select class="form-control" name="<?= $setting->name ?>">
						<?foreach(json_decode($setting->default_values) as $d):
                            $sel = $d->value == $setting->value ? " selected" : "";     ?>
							<option value="<?=$d->value?>"<?= $sel ?>><?=$d->label?></option>
						<?endforeach;?>
					</select>
            <?  elseif ($setting->type == "textarea"):  ?>
                    <textarea class="form-control" rows="2" name="<?= $setting->name ?>"><?= $setting->value ?></textarea>
            <?  endif;  ?>
            </div>
        </div>
    <?  endforeach; ?>
        <button type="submit" class="btn btn-primary">Save Changes</button>
    <?= form_close() ?>
    </div>
</div>
