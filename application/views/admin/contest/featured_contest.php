<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle ?></h1>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<? $this->load->view(THEME.'/alerts'); ?>

<div class="card default">
    <div class="card-header">
        <i class="fas fa-info-circle pr-2"></i>Instructions
    </div>
    <div class="card-body">
        Fill out the form below to create a new Featured Contest
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <?= form_open() ?>
            <div class="card default">
                <div class="card-header">New Contest</div>
                <div class="card-body">
                    <div class="form-group row">
                        <label for="sport" class="col-sm-4">Sport:</label>
                        <div class="col-sm-8">
                            <div class="checked-state sport-type">
                    <?  foreach ($sports as $sport):
                            $icon = $this->Misc->get_sport_icon($sport);    ?>
                            <input type="checkbox" id="radio_<?=$sport ?>" name="sport" value="<?=$sport?>"/>
                            <label for="radio_<?=$sport ?>">
                                <span class="option-box"><i class="<?=$icon?>"></i></span>
                                <span class="inner-text"><?=$sport?></span>
                            </label>
                    <?  endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Contest Type:</label>
                        <div class="col-sm-8">
                            <select class="form-control form-control-sm w-75" name="type">
                        <?  foreach ($gameTypes as $k=>$type):  ?>
                                <option value="<?=$k?>"><?=$type ?></option>
                        <?  endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Duration:</label>
                        <div class="col-sm-8">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Start Date:</label>
                        <div class="col-sm-8">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4"># of Entries:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm w-75 d-inline-block" name="size_val" value="2" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Entry Limit:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm w-75 d-inline-block" name="multi_entry_limit" value="1" /><i class="fas fa-question-circle fa-lg pull-right mt-1" data-toggle="tooltip" data-placement="right" title="Entering a number higher than one will designate the game multi-entry" ></i>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Buy In:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm w-75 d-inline-block" name="entry_fee" value="0" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Prize:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm w-75 d-inline-block" name="prize" value="0" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Company Take %:</label>
                        <div class="col-sm-8">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Name:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm w-75 d-inline-block" name="name" value="" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="game_type" class="col-sm-4">Featured:</label>
                        <div class="col-sm-8">
                        </div>
                    </div>
                </div>
            </div>
        <?= form_close() ?>
    </div>
    <div class="col-md-7">
        <div class="card default">
            <div class="card-header">Current Contest Settings</div>
            <div class="card-body">

            </div>
        </div>
    </div>
</div>

