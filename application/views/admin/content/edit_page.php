
<? $this->load->view(ADMIN_THEME. '/tinymce');    ?>
<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle ?></h1>
            <a href="javascript:history.go(-1)" class="float-right">Go Back</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class='col-12'>
        <? $this->load->view(THEME.'/alerts'); ?>
    </div>
    <div class="col-12">
        <ul class="nav nav-pills default ml-2 mb-1">
            <li class="nav-item">
                <a class="nav-link active" id="page-tab" data-toggle="tab" href="#page" role="tab" aria-controls="page" aria-selected="true">Page</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="true">SEO</a>
            </li>
        </ul>
        <?= form_open() ?>
        <input type="hidden" name="cid" value="<?=$page->cid?>" />
        <div class="tab-content" id="pageInfo">
            <div class="tab-pane fade show active" id="page">
                <div class="card default">
                    <div class="card-header">Page Details</div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-12 col-sm-6">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title" value="<?=$page->title?>">
                                <?= form_error('title') ?>
                            </div>
                            <div class="form-group col-12 col-sm-6">
                                <label for="title">Slug</label>
                                <input type="text" class="form-control" id="slug" name="slug" value="<?=$page->slug?>">
                                <?= form_error('slug') ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="title">Body</label>
                                <textarea id="body_text" name="body" class="form-control"><?=$page->body?></textarea>
                                <?=  form_error('body');?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="seo">
                <div class="card default">
                    <div class="card-header">SEO</div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="title">Browser Title</label>
                                <input class="form-control"  name="browser_title" id="browser_title" type="text" value="<?= $page->browser_title ?>" />
                                <?=form_error('browser_title');?>
                                <p class="help-block">
                                    This only replaces what is normally between the &lt;title&gt; tags. <br />
                                    Leaving it blank will use the default title style ( page name - site name ).
                                </p>
                            </div>
                            <div class="form-group col-12">
                                <label for="meta_keywords">Meta Keywords</label>
                                <textarea class="form-control" rows="3" name="meta_keywords" id="meta_keywords"><?=$page->meta_keywords?></textarea>
                                <?=form_error('meta_keywords');?>
                                <p class="help-block">(comma separated)</p>
                            </div>
                            <div class="form-group col-12">
                                <label for="meta_keywords">Meta Description</label>
                                <textarea class="form-control" rows="3" name="meta_description" id="meta_description"><?=set_value('meta_description', $page->meta_description);?></textarea>
                                <?=form_error('meta_description');?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <button type="submit" class="btn btn-primary">Save Page</button>
            </div>
        <?= form_close() ?>
        </div>
    </div>
</div>

