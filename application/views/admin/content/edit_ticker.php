
<div class="row">
    <div class="col-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left"><?=$pageTitle ?></h1>
            <a href="javascript:history.go(-1)" class="float-right">Go Back</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class='col-12'>
        <? $this->load->view(THEME.'/alerts'); ?>
    </div>
    <div class="col-12">
        <?= form_open() ?>
    <?  $sort = isset($ticker->sort_order) ? $ticker->sort_order : 0;   ?>
        <input type="hidden" name="cid" value="<?=$ticker->cid?>" />
        <input type="hidden" name="sort_order" value="<?=$sort ?>" />
        <div class="card default">
            <div class="card-header">Ticker Details</div>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-12 col-sm-6">
                        <label for="content">Content</label>
                        <input type="text" class="form-control" id="content" name="content" value="<?=$ticker->content?>">
                        <?= form_error('content') ?>
                    </div>
                    <div class="form-group col-12 col-sm-6">
                        <label for="link">Link</label>
                        <input type="text" class="form-control" id="link" name="link" value="<?=$ticker->link?>">
                        <?= form_error('link') ?>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save Ticker</button>
        <?= form_close() ?>
    </div>
</div>

