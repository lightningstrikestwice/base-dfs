<div class="row">
    <div class="col-xl-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left">Lobby Slider</h1>
            <a href="<?=base_url()?>admin/content/edit_slide" class="btn btn-primary float-right">Create New Slide</a>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<? $this->load->view(THEME.'/alerts');  ?>

<div class="card default">
    <div class="card-header">
        <i class="fas fa-info-circle pr-2"></i>Instructions
    </div>
    <div class="card-body">
        Below are the various slides that will rotate in the lobby. It is best to have at least 3 active slides at all times. You can also add custom images that link to internal or external pages either by uploading images (jpg or png).
    </div>
</div>

<div class="card default">
    <div class="card-body">
        <div class="table-responsive-lg">
            <table class="table table-striped table-sm table-hover" id="table_list">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th class="w-25">Image</th>
                        <th>Link</th>
                        <th>New Window</th>
                        <th>Show Slide</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
            <?  foreach ($slides as $slide):
                    $newWindow = $slide->new_window == 0 ? "No" : "Yes";
                    $showSlide = $slide->show_slide == 0 ? "No" : "Yes";    ?>
                    <tr>
                        <td><?= $slide->cid?></td>
                        <td><img src="<?=base_url() ?>uploads/slides/<?= $slide->image ?>" class="small_image"></td>
                        <td><?= $slide->link ?></td>
                        <td><?= $newWindow ?></td>
                        <td><?= $showSlide ?></td>
                        <td class="text-center">
                            <a href="<?=base_url()?>admin/content/edit_slide/<?=$slide->cid?>" title="Edit Slide" class="pr-1 btn btn-link"><i class="fas fa-edit"></i></a>
                            <button data-id="<?=$slide->cid?>" class="removeButton btn btn-link"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
            <?  endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="deleteConfirm" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-danger text-white">
				<h4 class="modal-title" id="myModalLabel">Delete Slide</h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
                <p>Are you sure you want to remove this slide?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<a id='actionURL' href="" class="btn btn-danger">Yes</a>
			</div>
		</div>
	</div>
</div>

<script>
jQuery(document).ready(function($){
    $('#table_list').DataTable({
        "order": [[ 0,"asc"]],  // Set Column # for Default Sort/Dir (zero based)
        "paging": false,
        "pageLength": 50,
        "language": {
            "searchPlaceholder": "Search Slide's",
            "search": "",
            "lengthMenu": "Show _MENU_ Slide's"
        },
        "columnDefs": [ {
            "targets": [1,5],     // Column Number to exclude from Sort (zero based)
            "orderable": false
            }]
    });

    $('.removeButton').on('click',function(){
        var id = $(this).data('id');

        $('#actionURL').prop('href',HOST_NAME+'admin/content/delete_slide/'+id);

        $('#deleteConfirm').modal('show');
    });
});
</script>