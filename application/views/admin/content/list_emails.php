<div class="row">
    <div class="col-xl-12">
        <div class="breadcrumb-holder">
            <h1 class="main-title float-left">Emails</h1>
            <a href="<?=base_url()?>admin/content/edit_email" class="btn btn-primary float-right">Create New Email</a>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<? $this->load->view(THEME.'/alerts');  ?>

<div class="card default">
    <div class="card-header">
        <i class="fas fa-info-circle pr-2"></i>Instructions
    </div>
    <div class="card-body">
        
    </div>
</div>

<div class="card default p-2">
    <div class="card-body">
        <div class="table-responsive-lg">
            <table class="table table-striped table-sm table-hover" id="table_list">
                <thead>
                    <tr>
                        <th>Slug</th>
                        <th>Subject</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
            <?  foreach ($emails as $email):
                    $subject = json_decode($email->subject,true)[$lang];
                ?>
                    <tr>
                        <td><a href="<?=base_url()?>admin/content/edit_email/<?=$email->cid?>" title="Edit Email"><?= $email->slug ?></a></td>
                        <td><a href="<?=base_url()?>admin/content/edit_email/<?=$email->cid?>" title="Edit Email"><?= $subject ?></a></td>
                        <td class="text-center">
                            <a href="<?=base_url()?>admin/content/edit_email/<?=$email->cid?>" title="Edit Email" class="pr-1 btn btn-link"><i class="fas fa-edit"></i></a>
                            <button data-slug="<?=$email->slug?>" data-id="<?=$email->cid?>" class="removeButton btn btn-link"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
            <?  endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="deleteConfirm" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header bg-danger text-white">
				<h4 class="modal-title" id="myModalLabel">Delete Email</h4>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
                <p>Are you sure you want to remove the "<span id='remove_name' class="font-weight-bold"></span>" Email?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				<a id='actionURL' href="" class="btn btn-danger">Yes</a>
			</div>
		</div>
	</div>
</div>

<script>
jQuery(document).ready(function($){
    $('#table_list').DataTable({
        "order": [[ 0,"asc"]],  // Set Column # for Default Sort/Dir (zero based)
        "paging": false,
        "pageLength": 50,
        "language": {
            "searchPlaceholder": "Search Emails",
            "search": "",
            "lengthMenu": "Show _MENU_ Emails"
        },
        "columnDefs": [ {
            "targets": [2],     // Column Number to exclude from Sort (zero based)
            "orderable": false
            }]
    });

    $('.removeButton').on('click',function(){
        var slug = $(this).data('slug');
        var id = $(this).data('id');

        $('#remove_name').text(slug);
        $('#actionURL').prop('href',HOST_NAME+'admin/content/delete_email/'+id);

        $('#deleteConfirm').modal('show');
    });
});
</script>