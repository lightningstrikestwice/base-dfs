<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {
   public function error_array() {
        return $this->_error_array;
    }
   public function is_unique($str, $field)
   {

      if (substr_count($field, '.')==3)
      {
         list($table,$field,$id_field,$id_val) = explode('.', $field);
         $query = $this->CI->db->limit(1)->where($field,$str)->where($id_field.' != ',$id_val)->get($table);
      } else {
         list($table, $field)=explode('.', $field);
         $query = $this->CI->db->limit(1)->get_where($table, array($field => $str));
      }

      return $query->num_rows() === 0;
    }

    public function password_check($str,$dbData) {
        list($table,$field,$id_field,$id_val) = explode('.', $dbData);
        $this->CI->db->where($field,$str);
        $this->CI->db->where($id_field,$id_val);
        $query = $this->CI->db->get($table);

        return $query->num_rows() === 1;
    }


}
// END MY Form Validation Class

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */