<?php
class Users extends CI_Model {
    public $tableName = 'users';

    function __construct(){
        // Call the Model constructor
        parent::__construct();

        $this->cookie_info = array('pay_method', 'state', 'usersource', 'last_login', 'join_date', 'ip');
    }

    // CRUD Base Setup
	public function get($order_by='', $where=''){
        if ($order_by != '') {
            $this->db->order_by($order_by['field'],$order_by['dir']);
        }
        if ($where != '') {
            $this->db->where($where);
        }

		$q = $this->db->get($this->tableName);
		return $q->result();
	}

	public function get_by_id($id) {
		$this->db->from($this->tableName);
		$this->db->where('cid', $id);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

	public function add_update($data,$id=''){
        $this->db->where('cid',$id);
        $check = $this->db->get($this->tableName);

        if ($check->num_rows() == 0) {
            $this->db->insert($this->tableName,$data);
        } else {
            $this->db->where('cid',$id);
            $this->db->update($this->tableName,$data);
        }
        return true;
    }

	public function delete($id){
		$this->db->where('cid', $id);
		$this->db->delete($this->tableName);
	}
    // END CRUD

	public function get_by_username($username) {
		$this->db->from($this->tableName);
		$this->db->where('username', $username);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

    public function get_user_array() {
        $data = array();
        $q = $this->db->get($this->tableName);

        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach ($q as $r) {
                $r->full_name = $r->first_name.' '.$r->last_name;
                $data[$r->cid] = $r;
            }
        }


        return $data;
    }

    //**************************************
    //
    // Checks if user is logged in and
    // redirects as needed
    //
    //**************************************
    public function is_loggedin($redirect=0){
        if (isset($this->session->user)) {
            if (strlen($this->session->user->email) == 0) {
                if($redirect == 1){
                    $camefrom = uri_string();
                    $_SESSION['camefrom'] = $camefrom;

                    $this->session->set_flashdata('error', 'Previous page is for members only');
                    redirect('/login');
                }
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
	}

    //**************************************
    //
    // Checks if user is a admin
    //
    //**************************************
	public function is_admin($redirect=0) {

		if(isset($this->session->admin['email'])){
			if ($this->session->admin['email'] && $this->session->admin['level'] < 2) {
				if($redirect == 1){
                    $this->session->set_flashdata('error', 'This page is for admins only');
					redirect('/');
				}
				return FALSE;
			}
			else {
				return TRUE;
			}


		}else{
			if($redirect == 1){
                $this->session->set_flashdata('error', 'This page is for admins only');
				redirect('/');
			}
			return FALSE;
		}

	}

	public function hash_password($password) {

		return md5($password.$this->config->item('md5_salt'));

	}

	/**
	 * verify_password_hash function.
	 *
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash) {

        $password = $this->hash_password($password);

        if ($password == $hash) {
            return TRUE;
        } else {
            return FALSE;
        }

		return FALSE;

	}

	/**
	 * create_user function.
	 *
	 * @access public
	 * @param mixed $username
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function create_user($data) {
		$this->db->insert('users', $data);
		$id = $this->db->insert_id();

		$user = $this->get_by_id($id);

        $this->session->set_userdata('user',$user);
        $this->session->set_userdata('email', $user->email);

		return TRUE;

	}

	function id(){
		if ($this->session->userdata('user')->cid != ''){
			return $this->session->userdata('user')->cid;
		} else {
			return '';
		}
	}

	/**
	 * resolve_user_login function.
	 *
	 * @access public
	 * @param mixed $username
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function resolve_user_login($username, $password) {

		$this->db->select('password');
		$this->db->from($this->tableName);
		$this->db->where('username', $username);
		$this->db->or_where('email',$username);
		$hash = $this->db->get()->row('password');

//
//        // This needs to change to a full on rehash, DO NOT use MD5 anymore
//        if (md5($password) == $hash) {
//            return true;
//        } else {
//            return false;
//        }

		return $this->verify_password_hash($password, $hash);

	}

	/**
	 * get_user_from_login function.
	 *
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
	public function get_user_from_login($username) {
		$this->db->from($this->tableName);
		$this->db->where('username', $username);
		$this->db->or_where('email',$username);

		return $this->db->get()->row();

	}

	function forcelogin($userID){
		$q = $this->db->get_where('users', array('cid' => $userID));
		if ($q->num_rows() > 0) {
			//grab any league info before blasting away session
			$user_team = $this->session->userdata('temp_league_team');
			$camefrom = $this->session->userdata('camefrom');
            $this->session->sess_destroy();
            $this->session->sess_create();
    		//set session
			if(!empty($user_team)) {
                $this->session->set_userdata('temp_league_team', $user_team);
			}
			if(!empty($camefrom)) {
                $this->session->set_userdata('camefrom',$camefrom);
			}

            $newuserdata = (array)$q->row();
            $newuserdata = $this->remove_cookie_junk($newuserdata);
            $this->session->set_userdata($newuserdata);
            $this->session->set_userdata('logged_in', true);

			return true;
		} else {
			return false;
		}
	}

	function remove_cookie_info($user = ''){
        foreach($this->cookie_junk as $key){
            if (isset($user->$key)) unset($user->$key);
        }
		return $user;
	}

    function get_admins() {
        $admins = array();

        $this->db->where('is_admin',1);
        $q = $this->db->get('users');

        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach ($q as $r) {
                $admins[$r->cid] = $r;
            }
        }

        return $admins;
    }

	function logout () {
		$this->session->sess_destroy();
	}

}

