<?php
class Payments extends CI_Model {
    public $tableName = 'users';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    // CRUD Base Setup
	public function get($order_by='', $where=''){
        if ($order_by != '') {
            $this->db->order_by($order_by['field'],$order_by['dir']);
        }
        if ($where != '') {
            $this->db->where();
        }

		$q = $this->db->get($this->tableName);
		return $q->result();
	}

	public function get_by_id($id) {
		$this->db->from($this->tableName);
		$this->db->where('cid', $id);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

	public function add_update($data,$id=''){
        $this->db->where('cid',$id);
        $check = $this->db->get($this->tableName);

        if ($check->num_rows() == 0) {
            $this->db->insert($this->tableName,$data);
        } else {
            $this->db->where('cid',$id);
            $this->db->update($this->tableName,$data);
        }
        return true;
    }

	public function delete($id){
		$this->db->where('cid', $id);
		$this->db->delete($this->tableName);
	}
    // END CRUD

	function get_all_payments($when=0){
        $users = $this->Users->get_user_array();
        $products = $this->Products->get_array();

		if($when > 0) {
			$this->db->where('time >',$when);
		}

		$this->db->order_by('cid','desc');
		$q = $this->db->get('payments');

        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach ($q as $r) {
                $r->user = $users[$r->user_id];
                $r->product = $products[$r->product_id];
            }
            return $q;
        }

        return false;
    }
}

