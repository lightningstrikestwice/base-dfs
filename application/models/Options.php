<?php
class Options extends CI_Model {
    public $tableName = 'options';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    // CRUD Base Setup
	public function get($order_by='', $where=''){
        if ($order_by != '') {
            $this->db->order_by($order_by['field'],$order_by['dir']);
        }
        if ($where != '') {
            $this->db->where($where);
        }

		$q = $this->db->get($this->tableName);
		return $q->result();
	}

	public function get_by_id($id) {
		$this->db->from($this->tableName);
		$this->db->where('cid', $id);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

    public function get_option($slug) {
		$this->db->from($this->tableName);
		$this->db->where('name', $slug);
		$q = $this->db->get();
        $q = $q->row();

        return $q->value;
    }

	public function add_update($data,$id=''){
        $this->db->where('name',$id);
        $check = $this->db->get($this->tableName);

        if ($check->num_rows() == 0) {
            $this->db->insert($this->tableName,$data);
        } else {
            $this->db->where('name',$id);
            $this->db->update($this->tableName,$data);
        }
        return true;
    }

	public function delete($id){
		$this->db->where('cid', $id);
		$this->db->delete($this->tableName);
	}
    // END CRUD

    public function get_group($groupID) {
        $this->db->where('group_id',$groupID);
        $q = $this->db->get($this->tableName);

        $groupOptions = array();
        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach ($q as $r) {
                $groupOptions[$r->name] = $r->value;
            }
        }

        return $groupOptions;
    }
}

