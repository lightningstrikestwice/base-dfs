<?php
class Lobbies extends CI_Model {
    public $tableName = 'league';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    // CRUD Base Setup
	public function get($order_by='', $where=''){
        if ($order_by != '') {
            $this->db->order_by($order_by['field'],$order_by['dir']);
        }
        if ($where != '') {
            $this->db->where();
        }

		$q = $this->db->get($this->tableName);
		return $q->result();
	}

	public function get_by_id($id) {
		$this->db->from($this->tableName);
		$this->db->where('cid', $id);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

	public function add_update($data,$id=''){
        $this->db->where('cid',$id);
        $check = $this->db->get($this->tableName);

        if ($check->num_rows() == 0) {
            $this->db->insert($this->tableName,$data);
        } else {
            $this->db->where('cid',$id);
            $this->db->update($this->tableName,$data);
        }
        return true;
    }

	public function delete($id){
		$this->db->where('cid', $id);
		$this->db->delete($this->tableName);
	}
    // END CRUD

	function get_full_lobby(){
        $this->db->select('ci_league.*');
        $this->db->select('count(ci_league_team.cid) AS entries');
        $this->db->join('league_team', 'ci_league.cid = ci_league_team.league_id','left');
		$this->db->where('((retain_lobby=0 AND first_game_cutoff >'.time().') OR retain_lobby=1)',null);
		$this->db->where('status <',2);
		$this->db->where("finalized",0);
		$this->db->where("active",1);
//		$this->db->where("visibility",'public');
//		$this->db->where('feat',1);

        $this->db->group_by('ci_league.cid');
        $this->db->order_by('feat','DESC');
        $this->db->order_by('first_game_cutoff','ASC');
        $this->db->order_by('entries','DESC');
		$leagues = $this->db->get('league');
		$all_leagues = $leagues->result();

        $leagueTeams = $this->Leagues->get_all_user_teams_array($this->session->user_id);
        foreach($all_leagues as $a) {
            $joined = isset($leagueTeams[$a->cid]) ? 1 : 0;
            $a->joined = $joined;
        }

		return $all_leagues;
	}

    public function get_filtered_rows($min,$max,$sports,$types) {
        $leagues = array();

        $sportList = explode('_', $sports);
        $typeList = explode('_', $types);

        $this->db->select('ci_league.*');
        $this->db->select('count(ci_league_team.cid) AS entries');
        $this->db->join('league_team', 'ci_league.cid = ci_league_team.league_id','left');

        if (!in_array("ALL", $typeList)) {
            $this->db->where_in('type',$typeList);
        }
        $this->db->where('entry_fee >=', $min);
        $this->db->where('entry_fee <=', $max);

		$this->db->where('((retain_lobby=0 AND first_game_cutoff >'.time().') OR retain_lobby=1)',null);
		$this->db->where('status <',2);
		$this->db->where("finalized",0);
		$this->db->where("active",1);

        $this->db->group_by('ci_league.cid');
        $this->db->order_by('feat','DESC');
        $this->db->order_by('first_game_cutoff','ASC');
        $this->db->order_by('entries','DESC');

        $q = $this->db->get('league');

        if ($q->num_rows() > 0) {
            $q = $q->result();

            $leagueTeams = $this->Leagues->get_all_user_teams_array($this->session->user_id);

            foreach($q as $r) {
                $leagueSports = json_decode($r->sport);

                $sChk = 0;
                foreach($sportList as $sl) {
                    if ($sl == "ALL") {
                        $sChk = 1;
                    }
                    if (in_array($sl,$leagueSports)) {
                        $sChk = 1;
                    }
                }

                if ($sChk == 1) {
                    $joined = isset($leagueTeams[$r->cid]) ? 1 : 0;
                    $r->joined = $joined;

                    $leagues[$r->cid] = $r;
                }
            }
        }

        return $leagues;
    }
}
