<?php
class Images extends CI_Model {

	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }


	function neatresize($source_image, $bringto_width, $bringto_height, $dest_path, $new_name = '', $watermark = FALSE) {
		$config['image_library'] = 'gd2';
		$config['source_image'] = $source_image;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['thumb_marker'] = '_temp';
		$config['width'] = $bringto_width;
		$config['height'] = $bringto_height;

		//get src img name and ext
		$pathinfo=pathinfo($source_image);

		$src_ext=$pathinfo['extension'];
		$src_name=basename($source_image,'.'.$src_ext);
		$dst_path=dirname($source_image);

		$orig_img_arr=getimagesize($config['source_image']);
		$orig_width=$orig_img_arr[0];
		$orig_height=$orig_img_arr[1];
		$mime=$orig_img_arr['mime'];

		//how to resize
		$orig_rat=$orig_width/$orig_height;
		$new_rat=$bringto_width/$bringto_height;
		$height_res_rat=$bringto_height/$orig_height;
		if ($orig_width*$height_res_rat<$bringto_width) {
			$config['master_dim'] = 'width';
		}
		else {
			$config['master_dim'] = 'height';
		}

		$this->load->library('image_lib');
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$this->image_lib->clear();
		list($temp_width, $temp_height)=getimagesize($dst_path.'/'.$src_name.'_temp.'.$src_ext);
		//

		if (ltrim($mime, 'image/')=='gif') $src = imagecreatefromgif($dst_path.'/'.$src_name.'_temp.'.$src_ext);
		if (ltrim($mime, 'image/')=='png') $src = imagecreatefrompng($dst_path.'/'.$src_name.'_temp.'.$src_ext);
		if (ltrim($mime, 'image/')=='jpeg' || ltrim($mime, 'image/')=='jpg') $src = imagecreatefromjpeg($dst_path.'/'.$src_name.'_temp.'.$src_ext);
		$dest = imagecreatetruecolor($bringto_width, $bringto_height);

		// Copy
		if ($config['master_dim']=='height') {
			$dst_y=0;
			$dst_x=floor(($temp_width-$config['width'])/2);
		} else {
			$dst_x=0;
			$dst_y=floor(($temp_height-$config['height'])/2);
		}

		imagecopy($dest, $src, 0, 0, $dst_x, $dst_y, $temp_width, $temp_height);
		@unlink($dst_path.'/'.$src_name.'_temp.'.$src_ext);

        //save image
		if ($new_name == ''){
			$new_name = $src_name;
		}

        if (ltrim($mime, 'image/')=='jpeg' || ltrim($mime, 'image/')=='jpg') imagejpeg($dest, $dest_path.'/'.$new_name.'.'.$src_ext, 90);
		if (ltrim($mime, 'image/')=='gif') imagegif($dest, $dest_path.'/'.$new_name.'.'.$src_ext, 90);
		if (ltrim($mime, 'image/')=='png') imagepng($dest, $dest_path.'/'.$new_name.'.'.$src_ext, 9);

		$this->image_lib->clear();
		//watermark if neccessary
		if ($this->options->get('use_watermark') == '1' && $watermark){
			$this->watermark($dest_path.'/'.$new_name.'.'.$src_ext, $dest_path.'/'.$new_name.'_temp.'.$src_ext);
		}
	}

	function resize($source_image, $bringto_width, $bringto_height, $dest_path, $new_name = '', $watermark = FALSE){
		$config['image_library'] = 'gd2';
		$config['source_image'] = $source_image;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = TRUE;
		//$config['thumb_marker'] = '_temp';
		$config['width'] = $bringto_width;
		$config['height'] = $bringto_height;

		//get src img name and ext
		$pathinfo=pathinfo($source_image);

		$src_ext=$pathinfo['extension'];
		$src_name=basename($source_image,'.'.$src_ext);
		$dst_path=dirname($source_image);

		//how to resize
		$orig_img_arr=getimagesize($config['source_image']);
		$orig_width=$orig_img_arr[0];
		$orig_height=$orig_img_arr[1];
		$orig_rat=$orig_width/$orig_height;
		if ($orig_rat > 1){
			$config['master_dim'] = 'width';
		} else {
			$config['master_dim'] = 'height';
		}


		$this->load->library('image_lib');
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$this->image_lib->clear();
		copy($source_image, $dest_path.'/'.$new_name.'.'.$src_ext);
		//@unlink($source_image);
		//echo $this->image_lib->display_errors();
		//exit;

		$this->image_lib->clear();
		//watermark if neccessary
		if ($this->options->get('use_watermark') == '1' && $watermark){
			$this->watermark($dest_path.$new_name.'.'.$src_ext, $dest_path.$new_name.'_temp.'.$src_ext);
		}
	}

	function watermark($source_name, $temp_name){
		$config_wmrk['source_image'] = $source_name;
		$config_wmrk['wm_overlay_path'] = 'uploads/watermark/'.$this->options->get('watermark_image');
		$config_wmrk['wm_opacity'] = '70';
		$config_wmrk['wm_type'] = 'overlay';
		$config_wmrk['wm_vrt_alignment'] = 'bottom';
		$config_wmrk['wm_hor_alignment'] = 'right';

		$this->image_lib->initialize($config_wmrk);

		$this->image_lib->watermark();
		//delete original and replace with watermarked one
		if (file_exists($temp_name)) {
			@unlink($source_name);
			rename($temp_name, $source_name);
		}
		//echo $this->image_lib->display_errors();
		//exit;
		$this->image_lib->clear();
	}
}
?>