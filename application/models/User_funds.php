<?php
class User_funds extends CI_Model {
    public $tableName = 'users';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    // CRUD Base Setup
	public function get($order_by='', $where=''){
        if ($order_by != '') {
            $this->db->order_by($order_by['field'],$order_by['dir']);
        }
        if ($where != '') {
            $this->db->where();
        }

		$q = $this->db->get($this->tableName);
		return $q->result();
	}

	public function get_by_id($id) {
		$this->db->from($this->tableName);
		$this->db->where('cid', $id);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

	public function add_update($data,$id=''){
        $this->db->where('cid',$id);
        $check = $this->db->get($this->tableName);

        if ($check->num_rows() == 0) {
            $this->db->insert($this->tableName,$data);
        } else {
            $this->db->where('cid',$id);
            $this->db->update($this->tableName,$data);
        }
        return true;
    }

	public function delete($id){
		$this->db->where('cid', $id);
		$this->db->delete($this->tableName);
	}
    // END CRUD

	public function get_transactions(){
        $data = array();
        $users = $this->Users->get_user_array();

        $this->db->order_by('timestamp', 'desc');
		$q = $this->db->get('user_balance_transactions');

        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach($q as $r) {
                $r->user = $users[$r->user_id];
                $data[] = $r;
            }
        }
		return $data;
	}


    // Old Original File - No Changes Made
	function user_balance($user_id = 0, $available_balance = TRUE, $price_formatted = TRUE){
		$ticket_types = $this->config->item('ticket_types');
		$ticket_balance = array();
		$real_balance = $bonus_balance = $tourney_balance = 0;	//default
		$res = $this->db->limit(1)->get_where('user_balances', array('user_id' => $user_id))->result();
		if ($res !== FALSE && count($res) > 0)
		{
			//return available balance? (available funds - ( $$ pending withdrawal + $$ locked in game)
			if ($available_balance === TRUE)
			{
				$pending_withdrawal = $this->pending_withdrawal_amt($user_id, FALSE);
				$locked_in_games = 0;
				$real_balance = $res[0]->balance - ($pending_withdrawal + $locked_in_games);
				$bonus_balance = $res[0]->bonus_balance;
				$tourney_balance = $res[0]->tourney_balance;
			}
			else
			{
				$real_balance = $res[0]->balance;
				$bonus_balance = $res[0]->bonus_balance;
				$tourney_balance = $res[0]->tourney_balance;
			}
			//get ticket balances
			foreach($ticket_types as $k=>$v) {
				$var = 'ticket_'.$k;
				$ticket_balance[$k] = $res[0]->$var;
			}
		} else {
			//create a balance record for this user
			$this->db->insert('user_balances', array('user_id' => $user_id, 'timestamp' => time()));
			$real_balance = 0;
			$bonus_balance = 0;
			$tourney_balance = 0;
			foreach($ticket_types as $k=>$v) {
				$var = 'ticket_'.$k;
				$ticket_balance[$k] = 0;
			}

		}

		//return formatted amount ex: 20.95	?
		if ($price_formatted === TRUE)
		{
			return array("balance"=>number_format($real_balance, 2, '.', ''),"bonus_balance"=>number_format($bonus_balance, 2, '.', ''),"tourney_balance"=>number_format($tourney_balance,0,'.',''),"ticket_balance"=>$ticket_balance);
		}
		else
		{
			return array("balance"=>$real_balance,"bonus_balance"=>$bonus_balance,"tourney_balance"=>$tourney_balance,"ticket_balance"=>$ticket_balance);
		}
	}

	function user_affiliate_balance($user_id = 0)
	{
		$this->db->select('SUM(amount) as pending');
		$this->db->where('aff_id',$user_id);
		$this->db->where('reconciled',0);
		$inc=$this->db->get('affiliates_transactions')->row();
		if ($inc !== FALSE && count($inc) > 0) {
			$inc_amt=(float)($inc->pending);
			return number_format($inc_amt, 2,'.','');
		} else {
			return 0.00;
		}
	}

	function get_user_payments($user_id=0) {
		$this->db->select('amount, timestamp, source');
		$this->db->where('user_id',$user_id);
		$this->db->where('(source = 9 OR source = 10)');
		$this->db->order_by('timestamp','desc');
		$query=$this->db->get('user_balance_transactions');
		return $query->result();
	}

	function get_last_affiliate_payment($user_id=0){
		$this->db->select('timestamp');
		$this->db->where('user_id',$user_id);
		$this->db->where('source',9);
		$this->db->order_by('timestamp','desc');
		$this->db->limit(1);
		$query=$this->db->get('user_balance_transactions');
		return $query->row();
	}

	function user_affiliate_earnings($user_id = 0)
	{
		$this->db->select('SUM(amount) as pending');
		$this->db->where('aff_id',$user_id);
		$this->db->where('reconciled',1);
		$inc=$this->db->get('affiliates_transactions')->row();
		if ($inc !== FALSE && count($inc) > 0) {
			$inc_amt=(float)($inc->pending);
			return number_format($inc_amt, 2, '.','');
		} else {
			return 0.00;
		}

	}
	function get_affiliate_payouts_by_league($league_id=0){
		if(!empty($league_id)){
			$this->db->select('SUM(amount) as amount');
			$this->db->where('league_id',$league_id);
			$trans=$this->db->get('affiliates_transactions');
			if($trans->num_rows()>0){
				$trans=$trans->row();
				return $trans->amount;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	function user_reward_balance($user_id = 0)
	{
		$this->db->select('SUM(amount) as pending');
		$this->db->where('user_id',$user_id);
		$this->db->where('reconciled',0);
		$inc=$this->db->get('reward_transactions')->row();
		if ($inc !== FALSE && count($inc) > 0) {
			$inc_amt=(float)($inc->pending);
			return number_format($inc_amt, 2,'.','');
		} else {
			return 0.00;
		}
	}

	function user_reward_earnings($user_id = 0)
	{
		$this->db->select('SUM(amount) as pending');
		$this->db->where('user_id',$user_id);
		$this->db->where('reconciled',1);
		$inc=$this->db->get('reward_transactions')->row();
		if ($inc !== FALSE && count($inc) > 0) {
			$inc_amt=(float)($inc->pending);
			return number_format($inc_amt, 2, '.','');
		} else {
			return 0.00;
		}
	}
	function user_spending_this_month($user_id = 0){
		if(!empty($user_id)){
			$this_month=mktime(0,0,0,date('m'),1,date('Y'));
			$this->db->where('user_id',$user_id);
			$this->db->where_in('source',array(4,7));
			$this->db->where('timestamp >',$this_month);
			$trans=$this->db->get('user_balance_transactions');
			if($trans->num_rows()>0){
				$money=$bonus=0;
				foreach($trans->result() as $t){
					if($t->source==4){
						$money+=$t->amount;
						$bonus+=$t->bonus_amount;
					}elseif($t->source==7){
						$money-=$t->amount;
						$bonus-=$t->bonus_amount;
					}
				}
				return $money+$bonus;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	function get_current_rewards_tier($spending=0){
		if($spending==0 || !empty($spending)){
			$tiers=$this->config->item('rake_reward_tiers');
			$last_limit=0;
			$last_percent=0;
			foreach($tiers as $limit=>$percent){
				if($spending < $limit && $spending > $last_limit){
					return array("percent"=>$last_percent,"amount"=>$last_limit);
				}else{
					$last_limit=$limit;
					$last_percent=$percent;
				}
			}
		}else{
			return array("percent"=>0,"amount"=>0);
		}
	}
	function get_next_rewards_tier($spending=0){
		if($spending==0 || !empty($spending)){
			$tiers=$this->config->item('rake_reward_tiers');
			$count=0;
			$last_limit=0;
			foreach($tiers as $limit=>$percent){
				if($spending==0 && $count==0){
					return array("percent"=>$percent,"amount"=>$limit);
				}elseif($spending < $limit && $spending > $last_limit){
					return array("percent"=>$percent,"amount"=>$limit);
				}else{
					$last_limit=$limit;
				}
				$count++;
			}
		}else{
			return array("percent"=>0,"amount"=>0);
		}
	}
	function get_rewards_payouts_by_league($league_id=0){
		if(!empty($league_id)){
			$this->db->select('SUM(amount) as amount');
			$this->db->where('league_id',$league_id);
			$trans=$this->db->get('reward_transactions');
			if($trans->num_rows()>0){
				$trans=$trans->row();
				return $trans->amount;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	function pending_withdrawal_amt($user_id = 0, $price_formatted = TRUE)
	{
		$sql = "SELECT SUM(amount) as total FROM ".$this->db->dbprefix('withdrawals')." WHERE `user_id` = ".$this->db->escape($user_id)." AND `status` IN (0, 1)";
		$res = $this->db->query($sql)->result();
		if (count($res) > 0)
		{
			if ($price_formatted === TRUE)
			{
				return number_format($res[0]->total, 2, '.', '');
			}
			else
			{
				return $res[0]->total;
			}

		}
		if ($price_formatted === TRUE)
		{
			return '0.00';
		}
		else
		{
			return 0;
		}
	}

	function in_play($user_id = 0, $price_formatted = TRUE)
	{
		$sql = "SELECT SUM(amount) as total FROM ".$this->db->dbprefix('user_balance_transactions')." JOIN ci_league on ci_league.cid = custom WHERE `user_id` = ".$this->db->escape($user_id)." AND `source` = '4' AND ci_league.status < 2 AND ci_league.finalized = 0 AND first_game_cutoff < ".time();
		$res = $this->db->query($sql)->result();

		//we don't need reversed any more
		/*$sql_reversed = "SELECT SUM(amount) as total FROM ".$this->db->dbprefix('user_balance_transactions')." WHERE `user_id` = ".$this->db->escape($user_id)." AND `source` = '7'";
		$res_reversed = $this->db->query($sql_reversed)->result();*/

		if (count($res) > 0)
		{
			$total = $res[0]->total;
			/*if (count($res_reversed) > 0)	//substracting reversals of entry fees for unfilled leagues
			{
				$total = $total - $res_reversed[0]->total;
			}*/

			if ($price_formatted === TRUE)
			{

				return number_format($total, 2, '.', '');
			}
			else
			{
				return $total;
			}

		}
		if ($price_formatted === TRUE)
		{
			return '0.00';
		}
		else
		{
			return 0;
		}
	}

	function locked_for_matches($user_id = 0, $price_formatted = TRUE)
	{
		$sql = "SELECT SUM(amount) as total FROM ".$this->db->dbprefix('user_balance_transactions')." JOIN ci_league on ci_league.cid = custom WHERE `user_id` = ".$this->db->escape($user_id)." AND `source` = '4' AND ci_league.status < 2";
		$res = $this->db->query($sql)->result();

		//we don't need reversed any more
		/*$sql_reversed = "SELECT SUM(amount) as total FROM ".$this->db->dbprefix('user_balance_transactions')." WHERE `user_id` = ".$this->db->escape($user_id)." AND `source` = '7'";
		$res_reversed = $this->db->query($sql_reversed)->result();*/

		if (count($res) > 0)
		{
			$total = $res[0]->total;
			/*if (count($res_reversed) > 0)	//substracting reversals of entry fees for unfilled leagues
			{
				$total = $total - $res_reversed[0]->total;
			}*/

			if ($price_formatted === TRUE)
			{

				return number_format($total, 2, '.', '');
			}
			else
			{
				return $total;
			}

		}
		if ($price_formatted === TRUE)
		{
			return '0.00';
		}
		else
		{
			return 0;
		}
	}

	function tickets_locked_for_matches($user_id = 0)
	{
		$sql = "SELECT SUM(ticket_amount) as total FROM ".$this->db->dbprefix('user_balance_transactions')." JOIN ci_league on ci_league.cid = custom WHERE `user_id` = ".$this->db->escape($user_id)." AND `source` = '4' AND ci_league.status < 2";
		$res = $this->db->query($sql)->result();

		if (count($res) > 0)
		{
			$total = round($res[0]->total,0);

			return $total;

		}

		return 0;

	}

	function update_balance($user_id = 0, $amt = 0, $debit_credit = 0, $bonus_balance = 0, $ticket_amt = 0, $ticket_type = 0, $tourney_amount = 0){
		$ticket_types = $this->config->item('ticket_types');
		$ticket_balances = array();
		//user has balance record?
		$res = $this->db->where('user_id', $user_id)->limit(1)->get('user_balances');
		if ($res->num_rows() == 0){
			$this->db->insert('user_balances', array('user_id' => $user_id, 'timestamp' => time()));
			$user_balance = 0;
			$user_bonus_balance = 0;
			$user_tourney_balance = 0;
			foreach($ticket_types as $t=>$v) {
				$var = 'ticket_'.$t;
				$ticket_balances[$var] = 0;
			}
		}else{
			$res=$res->row();
			$user_balance = number_format($res->balance, 2, '.', '');
			$user_bonus_balance = $res->bonus_balance;
			$user_tourney_balance = $res->tourney_balance;
			foreach($ticket_types as $t=>$v) {
				$var = 'ticket_'.$t;
				$ticket_balances[$var] = $res->$var;
			}
		}

		if ($debit_credit == 1)
		{
			$new_balance = $user_balance + $amt;
			$new_bonus_balance = $user_bonus_balance + $bonus_balance;
			$new_tourney_balance = $user_tourney_balance + $tourney_amount;
			if($ticket_type > 0) {
				$new_var = 'ticket_'.$ticket_type;
				$ticket_balances[$new_var] = $ticket_balances[$new_var] + $ticket_amt;
			}
		}
		else
		{
			$new_balance = $user_balance - $amt;
			$new_bonus_balance = $user_bonus_balance - $bonus_balance;
			$new_tourney_balance = $user_tourney_balance - $tourney_amount;

			if($ticket_type > 0) {
				$new_var = 'ticket_'.$ticket_type;
				$ticket_balances[$new_var] = $ticket_balances[$new_var] - $ticket_amt;
			}
		}
		//update
		$update = array(
					'balance' => $new_balance,
					'bonus_balance' => $new_bonus_balance,
					'tourney_balance' => $new_tourney_balance,
				);
		foreach($ticket_balances as $k=>$v) {
			$update[$k] = $v;
		}
		//$this->db->where('user_id', $user_id)->limit(1)->update('user_balances', array('balance' => $new_balance, 'bonus_balance' => $new_bonus_balance));
		$this->db->where('user_id', $user_id)->limit(1)->update('user_balances', $update);

		$this->session->set_userdata("user_balance",$new_balance);
	}

	function get_promo_code_by_id($id = ''){
		$this->db->limit(1);
		$this->db->where(array(
								'cid' => $id
								));
		$this->db->from('promo_codes');
		if ($this->db->count_all_results() > 0){
			$q = $this->db->get_where('promo_codes', array(
													'cid' => $id
													));
			$res = $q->result();
			$promo_code = $res[0];
			return $promo_code;
		}else{
			return false;
		}
	}

	function get_promo_by_id($id = ''){
		$this->db->limit(1);
		$this->db->where(array(
								'cid' => $id
								));
		$this->db->from('promo_codes');
		if ($this->db->count_all_results() > 0){
			$q = $this->db->get_where('promo_codes', array(
													'cid' => $id
													));
			$res = $q->result();
			return $res[0];
		}else{
			return false;
		}
	}

	function get_promo_by_code($code = ''){
		$this->db->limit(1);
		$this->db->where(array(
								'code' => strtolower($code)
								));
		$res = $this->db->get('promo_codes')->result();
		if (count($res) > 0){
			$promo = $res[0];
			return $promo;
		}else{
			return false;
		}
	}

	function get_promo_codes($per_page = '10', $page_num = '0'){
		$this->db->order_by('name', 'asc');
		$this->db->limit($per_page, $page_num);
		$q = $this->db->get('promo_codes');
		return $q->result();
	}

	function get_promo_codes_total()
	{
		return $this->db->count_all_results('promo_codes');
	}

	public function get_referal_promo(){
		$this->db->limit(1);
		$this->db->where('referral',1);
		$code=$this->db->get('promo_codes');
		if($code->num_rows()>0){
			return $code->row();
		}else{
			return false;
		}
	}

	function get_signup_promo($user_id){
		$this->db->limit(1);
		$this->db->where('user_id',$user_id);
		$this->db->where('signup_bonus',1);
		$code=$this->db->get('user_promo_codes');
		if($code->num_rows()>0){
			return $code->row();
		}else{
			return false;
		}
	}

	function update_promo_code($promo_code_id = '', $promo_code)
	{
		$this->db->limit(1);
		$this->db->where('cid', $promo_code_id);
		$this->db->update('promo_codes', $promo_code);
	}

	function create_promo_code($promo_code)
	{
		$this->db->insert('promo_codes', $promo_code);
		return $this->db->insert_id();
	}

	function duplicate_promocode_check($code)
	{
		$this->db->limit(1);
		$this->db->where('code', $code);
		$this->db->from('promo_codes');
		if ($this->db->count_all_results() > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}

	}

	function delete_promo_code($promo_code_id = '')
	{
		$this->db->limit(1);
		$this->db->where('cid', $promo_code_id);
		$this->db->delete('promo_codes');
	}

	function check_user_promocode_validity($user_id='',$promo_code=''){
		if(!empty($user_id) && !empty($promo_code)){
			$promo=$this->get_promo_by_code($promo_code);
			if($promo && $promo->status==1){
				if(($promo->begins==0 || $promo->begins <= time()) && ($promo->expires==0 || $promo->expires >= time())){
					$this->db->where('user_id',$user_id);
					$this->db->where('promo_id',$promo->cid);
					$user_promo=$this->db->get('user_promo_codes');
					if($user_promo->num_rows()>0){
						$user_promo=$user_promo->row();
						if($user_promo->uses < $promo->max_per_user){
							//user has used code before, but it is not used up
							if($promo->requires_min_payment==0) {
								//user doesn't need to deposit, award promo right away
								$apply_it = $this->apply_promo($user_id,$promo_code,0);
								$payment_required = 'N';
							} else {
								$payment_required = 'Y';
							}
							return array("status"=>true,"code"=>$promo_code,"message"=>$promo->description,"payment_required"=>$payment_required);
						}else{
							//user has used all uses of code
							return array("status"=>false,"reason"=>vlang('my_account_add_funds_promo_no_uses'));
						}
					}else{
						//user has not used this code yet
						if($promo->requires_min_payment==0) {
							//user doesn't need to deposit, award promo right away
							$apply_it = $this->apply_promo($user_id,$promo_code,0);
							$payment_required = 'N';
						} else {
							$payment_required = 'Y';
						}

						if($promo->affiliate == "" || empty($promo->affiliate) || !isset($promo->affiliate)){
							return array("status"=>true,"code"=>$promo_code,"message"=>$promo->description,"payment_required"=>$payment_required);
						}
						else{
							$this->db->where("meta_key","new");
							$this->db->where("user_id",$user_id);
							$q = $this->db->get("usermeta");
							if($q->num_rows() > 0){
								return array("status"=>true,"code"=>$promo_code,"message"=>$promo->description,"payment_required"=>$payment_required);
								$this->db->where('user_id', $user_id);
								$this->db->where("meta_key","new");
								$this->db->delete('usermeta');
							}
							else{
								return array("status"=>false,"code"=>$promo_code,"reason"=>"This promo code is for new member only.","payment_required"=>$payment_required);
							}
						}
					}
				}else{
					return array("status"=>false,"reason"=>vlang('my_account_add_funds_promo_expired'));
				}
			}else{
				//promo code doesn't exist
				return array("status"=>false,"reason"=>vlang('my_account_add_funds_promo_no_exist'));
			}
		}else{
			//no data
			return array("status"=>false,"reason"=>vlang('my_account_add_funds_promo_no_data'));
		}
	}

	function apply_promo($user_id='',$promo_code='',$deposit=0){
        //echo $user_id." - ".$promo_code." - ".$deposit."</br>";
        if(!empty($user_id) || !empty($promo_code)){
            $promo=$this->get_promo_by_code($promo_code);

            if($promo && $promo->status==1){
                if(($promo->begins==0 || $promo->begins <= time()) && ($promo->expires==0 || $promo->expires >= time())){
                    $this->db->where('user_id',$user_id);
                    $this->db->where('promo_id',$promo->cid);
                    $user_promo=$this->db->get('user_promo_codes');
                    if($user_promo->num_rows()>0){
                        $user_promo=$user_promo->row();
                        if($user_promo->uses < $promo->max_per_user){
                            //echo "user has used code before, but it is not used up, proceed to next block</br>";
                            if(!empty($promo->affiliate)){
                                $this->db->where('username',$promo->affiliate);
                                $aff=$this->db->get('users')->row();
                                if(count($aff) > 0) {
                                    $aff_id = $aff->cid;
                                }
                                if($aff_id !=  $user_id){
                                    $this->db->insert('affiliates',array('aff_id'=>$aff_id,'user_id'=>$user_id,'timestamp'=>time()));
                                }
                            }
                        }else{
                            //echo "user has used all uses of code</br>";
                            $this->add_transaction($user_id,2,1,0,'Promo Code '.$promo_code.' invalid: '.vlang('my_account_add_funds_promo_no_uses'),'',0,'',$promo->cid);
                            return false;
                        }
                    }else{
                        //echo "user has not used code, proceed to next block</br>";
                        $this->db->where("meta_key","new");
                        $this->db->where("user_id",$user_id);
                        $q = $this->db->get("usermeta");
//                         $q = $q->result();

                        if(!empty($promo->affiliate) || $promo->referral == 1 ) {
                            //this is a signup promo, can't use that here
                            if($q->num_rows() == 0){

                                $this->add_transaction($user_id,2,1,0,'Promo Code '.$promo_code.' invalid: '.vlang('my_account_add_funds_promo_signup'),'',0,'',$promo->cid);
                                return false;
                            }else{
	                            $this->db->where('username',$promo->affiliate);
                                $aff=$this->db->get('users')->row();
                                if(count($aff) > 0) {
                                    $aff_id = $aff->cid;
                                }
                                if($aff_id !=  $user_id){
                                    $this->db->insert('affiliates',array('aff_id'=>$aff_id,'user_id'=>$user_id,'timestamp'=>time()));
                                }
                            }
                        }
                    }
                }else{
                    //echo "time not right</br>";
                    $this->add_transaction($user_id,2,1,0,'Promo Code '.$promo_code.' invalid: '.vlang('my_account_add_funds_promo_expired'),'',0,'',$promo->cid);
                    return false;
                }
            }else{
                //echo "promo inactive or non existant</br>";
                $this->add_transaction($user_id,2,1,0,'Promo Code '.$promo_code.' invalid: '.vlang('my_account_add_funds_promo_no_exist'),'',0,'',$promo->cid);
                return false;
            }
        }else{
            //echo "no data</br>";
            return false;
        }

        if($deposit>=$promo->requires_min_payment){
            $bonus_money=0;
            $cash=0;
            if(!empty($promo->money)) $bonus_money+=$promo->money;
            if(!empty($promo->cash)) $cash+=$promo->cash;
            if(!empty($promo->percentage) && !empty($deposit)) {
                $promo_cap = $this->options->get('promo_cap');
                //if deposit is over promo cap, then cap it
                if($deposit > $promo_cap) {
                    $deposit = $promo_cap;
                }
                $bonus_money+=($deposit * $promo->percentage);
            }
            if(!empty($promo->cash_percentage) && !empty($deposit)) {
                $promo_cap = $this->options->get('promo_cap');
                if($deposit > $promo_cap) {
                    $deposit = $promo_cap;
                }
                $cash+=($deposit * $promo->cash_percentage);
            }
            //increment promo code counter
            //echo "attempting increment promo code counter</br>";
            $this->incrememnt_promo($promo->cid);
            //increment user promo code counter
            //echo "attempting increment user promo code counter</br>";
            $this->incrememnt_user_promo($user_id,$promo->cid);
            //give user money
            //echo "attempting to give user money</br>";
            $this->add_transaction($user_id,2,1,$cash,'Promo Code: '.$promo_code,'',$bonus_money,'',$promo->cid);
            $this->update_balance($user_id,$cash,1,$bonus_money);
            return true;
        }else{
            //echo "Minimum payment not met</br>";
            $this->add_transaction($user_id,2,1,0,'Promo Code '.$promo_code.' invalid: '.vlang('my_account_add_funds_promo_minimum'),'',0,'',$promo->cid);
            return false;
        }
    }

	function apply_sign_up_promos($user_id = 0, $paid_total = 0){
		if(!empty($user_id)){
			$signup_promo = $this->get_signup_promo($user_id);

			if($signup_promo !== FALSE) {
				$promo = $this->get_promo_by_id($signup_promo->promo_id);
				$invalid = 0;

				if($promo->begins > 0 && $promo->begins > time()) $invalid = 1;
				if($promo->expires > 0 && $promo->expires < time()) $invalid = 1;
				if($promo->status == 0) $invalid = 1;

				if($promo !== FALSE && $invalid != 1 && ($promo->money > 0 || $promo->percentage > 0) && $paid_total >= $promo->requires_min_payment){
					$promo_code = $promo->code;
					//user has balance record?
					$res = $this->db->where('user_id', $user_id)->limit(1)->get('user_balances');
					if ($res->num_rows() == 0){
						$this->db->insert('user_balances', array('user_id' => $user_id, 'timestamp' => time()));
						$user_balance = 0;
						$user_points = 0;
					}else{
						$res=$res->row();
						$user_balance = number_format($res->balance, 2, '.', '');
						$user_points = $res->points;
					}

					//if limit on promo uses per user and already used - do not credit user and exit function
					if ($promo->max_per_user != '' && $promo->max_per_user > 0 && $promo->max_per_user <= $this->db->where(array('user_id' => $user_id, 'promo_id' => $promo->cid))->count_all_results('user_balance_transactions')){
						return FALSE;
					}

					//add promo amounts

					$money_added = $promo->money;
					$user_balance = $user_balance + $promo->money;
					if($promo->percentage > 0) {
						$promo_cap = $this->options->get('promo_cap');
						//if deposit is over promo cap, then cap it
						if($paid_total > $promo_cap) {
							$paid_total = $promo_cap;
						}
						//add extra percentage to user's deposit
						$user_balance = $user_balance + (number_format(($paid_total * $promo->percentage),2,'.',''));
						$money_added = $money_added + ($paid_total * $promo->percentage);
					}
					$user_points = $user_points + $promo->points;

					$logcomment='Promo code "'.$promo->code.'" added '.($money_added > 0?'$'.number_format($money_added, 2, '.', ''):'') . ($money_added > 0 && $promo->points > 0 ? ' and ':'') . ($promo->points > 0?$promo->points.' points':'') . ' to user\'s balance.';

					$this->add_transaction($user_id,2,1,0,$logcomment,'',$money_added,'',$promo->cid);
					$this->update_balance($user_id,0,1,$money_added);

					//update promo use count
					$this->incrememnt_user_promo($user_id,$promo->cid);
					$this->incrememnt_promo($promo->cid);
				}
			}
		}
	}

	function incrememnt_promo($promo_id){
		if(!empty($promo_id)){
			$this->db->select('cid, used_times');
			$this->db->where('cid',$promo_id);
			$this->db->limit(1);
			$check=$this->db->get('promo_codes');
			if($check->num_rows()>0){
				$check=$check->row();
				$this->db->where('cid',$check->cid);
				$this->db->update('promo_codes',array("used_times"=>1+$check->used_times));
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function incrememnt_user_promo($user_id,$promo_id){
		if(!empty($user_id) && !empty($promo_id)){
			$this->db->select('id, uses');
			$this->db->where('user_id',$user_id);
			$this->db->where('promo_id',$promo_id);
			$this->db->limit(1);
			$check=$this->db->get('user_promo_codes');
			if($check->num_rows()>0){
				$check=$check->row();
				$this->db->where('id',$check->id);
				$this->db->update('user_promo_codes',array("uses"=>1+$check->uses));
				return true;
			}else{
				$insert=array(
					"user_id"=>$user_id,
					"promo_id"=>$promo_id,
					"uses"=>1
				);
				$this->db->insert("user_promo_codes",$insert);
				return true;
			}
		}else{
			return false;
		}
	}

	function on_user_create($user_id = 0){
		$promo_sess = $this->session->userdata('promo');
		$promo = $this->get_promo_by_code($promo_sess);

		//record last promocode used
		if ($promo !== FALSE && strlen($promo) > 0 )
		{
			$this->db->where('cid', $user_id)->limit(1)->update('users', array('promocode' => $promo));
		}


		if ($promo !== FALSE && strlen($promo->money) > 0 && $promo->money > 0 && in_array($promo->requires_min_payment, array('0', '')))
		{
			//user has balance record?
			$res = $this->db->where('user_id', $user_id)->limit(1)->get('user_balances')->result();
			if (count($res) == 0)
			{
				$this->db->insert('user_balances', array('user_id' => $user_id, 'timestamp' => time()));
				$user_balance = 0;
				$user_points = 0;
			}
			else
			{
				$user_balance = number_format($res[0]->balance, 2, '.', '');
				$user_points = $res[0]->points;
			}

			//add promo amounts
			$user_balance = $user_balance + $promo->money;
			$user_points = $user_points + $promo->points;

			//if limit on promo uses per user and already used - do not credit user and exit function
			if ($promo->max_per_user != '' && $promo->max_per_user > 0 && $promo->max_per_user <= $this->db->where(array('user_id' => $user_id, 'promo_id' => $promo->cid))->count_all_results('user_balance_transactions'))
			{
				return FALSE;
			}

			$user_balance = number_format($user_balance, 2, '.', '');
			$this->db->where('user_id', $user_id)->limit(1)->update('user_balances', array('balance' => $user_balance, 'points' => $user_points));
			$this->session->set_userdata("user_balance",$user_balance);
			//record balance transaction
			$transaction = array(
								'user_id' => $user_id,
								'source' => 2,
								'promo_id' => $promo->cid,
								'debit' => 1,
								'amount' => $promo->money,
								'points' => $promo->points,
								'comment' => 'Promo code "'.$promo->code.'" added '.($promo->money > 0?'$'.number_format($promo->money, 2, '.', ''):'') . ($promo->money > 0 && $promo->points > 0 ? ' and ':'') . ($promo->points > 0?$promo->points.' points':'') . ' to user\'s balance.',
								'timestamp' => time()
								);
			$this->db->insert('user_balance_transactions', $transaction);
			//update promo use count
			$this->db->query("UPDATE ".$this->db->dbprefix('promo_codes')." SET `used_times` = used_times + 1 WHERE code = ".$this->db->escape($promo_sess)." LIMIT 1");

		}
	}

	function on_user_cleanup($user_id = 0){

	}

	function get_locked_by_league_id($league_id='',$user_id=''){
		if(!empty($league_id)){
			if(!empty($user_id)){
				$this->db->where('user_id',$user_id);
			}
			$this->db->where('source',4);
			$this->db->where('custom',$league_id);
			$trans=$this->db->get('user_balance_transactions');
			if($trans->num_rows()>0){
				return $trans->result();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	function get_reversed_by_league_id($league_id='',$user_id=''){
		if(!empty($league_id)){
			if(!empty($user_id)){
				$this->db->where('user_id',$user_id);
			}
			$this->db->where('source',7);
			$this->db->where('custom',$league_id);
			$trans=$this->db->get('user_balance_transactions');
			if($trans->num_rows()>0){
				return $trans->result();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/*
	*  insert_payment
	*
	*  This is tied to the edit user funds.... Be aware this will change the accounting for the site.
	*
	*  @Peram: $uid, ,$type
	*  @Return: nothing
	*
	*  Isaiah Arnold
	*
	*  10/22/14
	*/

	function insert_payment($uid,$type,$amount){
		$data = array(
           'user_id' => $uid,
           'amount' => $amount,
           'type' => $type,
           'trans_id' => time().'_AA_'.$uid,
           'time' => time()
        );
		$this->db->insert('payments', $data);
	}
	/*==========================================================================
		ADD TRAN$ACTION
			This keeps track of any money coming in or out from user's balance.
			Don't forget to udpate user balance with $this->user_funds->update_balance() if needed.
			//source: 0 - n/a, 1 - payment, 2 - promo, 3 - funds withdrawal; 4 - money locked for pending contest;
            //      5 - moneys user won from a contest :D ; 6 - moneys lost in a contest :( ; 7 - reversal of contest entry fee when contest is unfilled;
			//debit: 1 - money/points added to user balance; 0 - money/points withdrawn from user balance
			//payment_id - cid from ci_payment if applicable
			//comment - user can usually see this
			//points not currently used but who knows..
			//promo_id - if promo used - promo id
			//custom - used to connect with other plugins etc;
			//if ( in_array($source, array(4, 5, 6, 7)) ) $custom = $league_id; <== if you know what I mean :)
	==========================================================================*/
	function add_transaction($user_id = 0, $source = 0, $debit_credit = 1, $money = 0, $comment = '', $custom = '', $bonus_amount = 0, $payment_id = '',  $promo_id = 0, $ticket_amount = 0, $ticket_type = 0, $ticket_value = 0, $tourney_amount = 0)
	{
		if($source==3) {
			$bal=$this->user_balance($user_id,false,false);
		} else {
			$bal=$this->user_balance($user_id,true,false);
		}
		if($debit_credit==1){
			$newbalance=round(($bal['balance']+$money),2);
			$new_bonus_balance=round(($bal['bonus_balance']+$bonus_amount),2);
			$new_tourney_balance=round(($bal['tourney_balance']+$tourney_amount),2);
		}else{
			$newbalance=round(($bal['balance']-$money),2);
			$new_bonus_balance=round(($bal['bonus_balance']-$bonus_amount),2);
			$new_tourney_balance=round(($bal['tourney_balance']-$tourney_amount),2);
		}
		$this->db->insert('user_balance_transactions',
							array(
								'user_id' => $user_id,
								'source' => $source,
								'promo_id' => $promo_id,
								'debit' => $debit_credit,
								'amount' => $money,
								'comment' => $comment,
								'bonus_amount' => $bonus_amount,
								'tourney_amount' => $tourney_amount,
								'ticket_amount' => $ticket_amount,
								'ticket_type' => $ticket_type,
								'ticket_value' => $ticket_value,
								'payment_id' => $payment_id,
								'custom' => $custom,
								'timestamp' => time(),
								"new_balance"=>$newbalance,
								"new_bonus_balance"=>$new_bonus_balance,
								"new_tourney_balance"=>$new_tourney_balance,
								));

		return $this->db->insert_id();
	}

	function lock_for_match($user_id = 0, $amount = 0, $league_id = 0, $league_name = '')
	{
		$league_info = $this->leagues->get_by_id($league_id);
		if($league_info->entry_fee_type == 2) {
			//enter with a ticket
			//find oldest ticket and redeem it, also find out what it's worth
			$this->db->where('user_id',$user_id);
			$this->db->where('redeemed',0);
			$this->db->where('ticket_type',$league_info->entry_ticket_type);
			$this->db->order_by('issued','asc');
			$this->db->limit(1);

			$ticket = $this->db->get('user_tickets')->row();

			$this->db->where('id',$ticket->id);
			$this->db->update('user_tickets',array('redeemed'=>1,'redeemed_time'=>time()));

			$this->add_transaction($user_id,4,0,0,'Ticket locked for '.$league_name.' match',$league_id,0,0,0,1,$league_info->entry_ticket_type,$ticket->ticket_value);
			$this->update_balance($user_id, 0, 0, 0,1,$league_info->entry_ticket_type);

		} elseif($league_info->entry_fee_type == 3) {
			$balance=$this->user_balance($user_id,TRUE,FALSE);
			//are we using a ticket or money
			if($balance['ticket_balance'][$league_info->entry_ticket_type] > 0) {
				//if user has a ticket then always use that first
				//find oldest ticket and redeem it, also find out what it's worth
				$this->db->where('user_id',$user_id);
				$this->db->where('redeemed',0);
				$this->db->where('ticket_type',$league_info->entry_ticket_type);
				$this->db->order_by('issued','asc');
				$this->db->limit(1);

				$ticket = $this->db->get('user_tickets')->row();

				$this->db->where('id',$ticket->id);
				$this->db->update('user_tickets',array('redeemed'=>1,'redeemed_time'=>time()));

				$this->add_transaction($user_id,4,0,0,'Ticket locked for '.$league_name.' match',$league_id,0,0,0,1,$league_info->entry_ticket_type,$ticket->ticket_value);
				$this->update_balance($user_id, 0, 0, 0,1,$league_info->entry_ticket_type);
			} else {
				//no tix so use money instead
				$regular_amount=$amount;

				if($balance['bonus_balance']>0){

					$this->db->where("name","bonus_drip_rate");
					$q = $this->db->get("options");
					$q = $q->result();
					$drip_rate = $q[0]->value;
					//$drip_rate=$this->config->item('bonus_drip_rate');

					$drip_amount=$amount*$drip_rate;
					if($balance['bonus_balance']>$drip_amount){
						//Lock the bonus money for $drip_amount
						$regular_amount=$amount-$drip_amount;
						$bonus_amount=$drip_amount;
					}else{
						//there's less than the full amount of drip, so just pull the max and grab the rest from the user's normal funds
						$regular_amount=$amount-$balance['bonus_balance'];
						$bonus_amount=$balance['bonus_balance'];
					}
					$this->add_transaction($user_id, 4, 0, 0, 'Bonus Funds locked for '.$league_name.' match', $league_id, $bonus_amount);
				}else{
					$bonus_amount=0;
				}
				$this->add_transaction($user_id, 4, 0, $regular_amount, 'Funds locked for '.$league_name.' match', $league_id);
				$this->update_balance($user_id, $regular_amount, 0, $bonus_amount);
			}

		} elseif($league_info->entry_fee_type == 4) {
			$this->add_transaction($user_id,4,0,0,'Tourney dollars locked for '.$league_name.' match',$league_id,0,0,0,0,0,0,$league_info->entry_tourney_dollars);
			$this->update_balance($user_id, 0, 0, 0,0,0,$league_info->entry_tourney_dollars);


		} elseif($league_info->entry_fee_type == 5) {
			$balance=$this->user_balance($user_id,TRUE,FALSE);
			//are we using a ticket or money
			if($balance['tourney_balance'] > 0) {
				//if user has tournament dollars then always use that first
				$this->add_transaction($user_id,4,0,0,'Tourney dollars locked for '.$league_name.' match',$league_id,0,0,0,0,0,0,$league_info->entry_tourney_dollars);
				$this->update_balance($user_id, 0, 0, 0,0,0,$league_info->entry_tourney_dollars);
			} else {
				//not enough tourney dollars so use money instead
				$regular_amount=$amount;

				if($balance['bonus_balance']>0){
					$this->db->where("name","bonus_drip_rate");
					$q = $this->db->get("options");
					$q = $q->result();
					$drip_rate = $q[0]->value;
					$drip_amount=$amount*$drip_rate;
					if($balance['bonus_balance']>$drip_amount){
						//Lock the bonus money for $drip_amount
						$regular_amount=$amount-$drip_amount;
						$bonus_amount=$drip_amount;
					}else{
						//there's less than the full amount of drip, so just pull the max and grab the rest from the user's normal funds
						$regular_amount=$amount-$balance['bonus_balance'];
						$bonus_amount=$balance['bonus_balance'];
					}
					$this->add_transaction($user_id, 4, 0, 0, 'Bonus Funds locked for '.$league_name.' match', $league_id, $bonus_amount);
				}else{
					$bonus_amount=0;
				}
				$this->add_transaction($user_id, 4, 0, $regular_amount, 'Funds locked for '.$league_name.' match', $league_id);
				$this->update_balance($user_id, $regular_amount, 0, $bonus_amount);
			}


		} else {
			$regular_amount=$amount;
			$balance=$this->user_balance($user_id,TRUE,FALSE);
			if($balance['bonus_balance']>0){
				$this->db->where("name","bonus_drip_rate");
					$q = $this->db->get("options");
					$q = $q->result();
					$drip_rate = $q[0]->value;
				$drip_amount=round(($amount*$drip_rate),2,PHP_ROUND_HALF_UP);
				if($balance['bonus_balance']>$drip_amount){
					//Lock the bonus money for $drip_amount
					$regular_amount=$amount-$drip_amount;
					$bonus_amount=$drip_amount;
				}else{
					//there's less than the full amount of drip, so just pull the max and grab the rest from the user's normal funds
					$regular_amount=$amount-$balance['bonus_balance'];
					$bonus_amount=$balance['bonus_balance'];
				}
				$this->add_transaction($user_id, 4, 0, 0, 'Bonus Funds locked for '.$league_name.' match', $league_id, $bonus_amount);
			}else{
				$bonus_amount=0;
			}
			$this->add_transaction($user_id, 4, 0, $regular_amount, 'Funds locked for '.$league_name.' match', $league_id);
			$this->update_balance($user_id, $regular_amount, 0, $bonus_amount);
		}


	}

	function add_payment($user_id = 0, $amt = 0, $payment_id = 0)
	{
		$this->load->model('users');
		$user = $this->users->get_by_id($user_id);
		if ($user === FALSE)
		{
			//huh?
			return FALSE;
		}

		//user has balance record?
		$res = $this->db->where('user_id', $user_id)->limit(1)->get('user_balances')->result();
		if (count($res) == 0)
		{
			$this->db->insert('user_balances', array('user_id' => $user_id, 'timestamp' => time()));
			$user_balance = 0;
		}
		else
		{
			$user_balance = number_format($res[0]->balance, 2, '.', '');
		}
		//record transaction
		$this->add_transaction($user_id, 1, 1, $amt, '$'.$amt.' deposit.', $payment_id, 0, $payment_id);
		//Add Money
		$user_balance = $user_balance + $amt;
		$this->db->limit(1)->where(array('user_id' => $user_id))->update('user_balances', array('balance' => $user_balance));
		$this->session->set_userdata("user_balance",$user_balance);



		return TRUE;
	}

	function get_transactions_total($where_arr = array(), $where_in_arr = array())
	{
		if (is_array($where_arr) && count($where_arr) > 0)
		{
			$this->db->where($where_arr);
		}
		if (is_array($where_in_arr) && count($where_in_arr) > 0)
		{
			$this->db->where_in($where_in_arr['column'], $where_in_arr['vals']);
		}
		return $this->db->count_all_results('user_balance_transactions');
	}


	//BOF withdrawals functionality
	function get_withdrawal_by_id($id = '')
	{
		$this->db->limit(1);
		$this->db->where(array(
								'cid' => $id
								));
		$this->db->from('withdrawals');
		if ($this->db->count_all_results() > 0)
		{
			$q = $this->db->get_where('withdrawals', array(
				'cid' => $id
			));
			$res = $q->result();
			$item = $res[0];
			return $item;
		}
		else
		{
			return false;
		}
	}

	function get_withdrawals($where_arr = array()){
        $withdrawals = array();

		if (is_array($where_arr) && count($where_arr) > 0){
			foreach($where_arr as $k=>$v){
				if(is_array($v)){
					$this->db->where_in($k,$v);
				}else{
					$this->db->where($k,$v);
				}
			}
		}
		$this->db->order_by('req_timestamp', 'desc');
		$q = $this->db->get('withdrawals');
        if ($q->num_rows() > 0) {
            $q = $q->result();
            foreach ($q as $r) {
                switch ($r->method) {
                    // Paypal
                    case 0:
                        $info = $r->paypal_email;
                        break;
                    // Check
                    case 1:
                        $info = $r->check_name . "<BR/>".$r->check_address;
                        break;
                    // Bitcoin
                    case 2:
                        $info = $r->bitcoin_address;
                        break;
                }

                $r->info = $info;

                $withdrawals[$r->cid] = $r;
            }
        }
		return $withdrawals;
	}

	function get_withdrawals_total($where_arr = array())
	{
		if (is_array($where_arr) && count($where_arr) > 0)
		{
			$this->db->where($where_arr);
		}
		return $this->db->count_all_results('withdrawals');
	}

	function update_withdrawal($cid = '', $item)
	{
		$this->db->limit(1);
		$this->db->where('cid', $cid);
		$this->db->update('withdrawals', $item);
	}

	function create_withdrawal($item){
		$this->db->insert('withdrawals', $item);

		$curr_user = $this->Users->get_by_id($item['user_id']);
		$email_data = array('user' => $curr_user, 'request' => $item);

        $this->load->model('Emails');
        $adminEmail = $this->Options->get_option('main_email');

        $this->Emails->send_mail($curr_user->email, 'withdrawl_request', $email_data);
        $this->Emails->send_mail($adminEmail, 'withdrawl_request_admin', $email_data);

		return $this->db->insert_id();
	}

	function delete_withdrawal($cid = '')
	{
		$this->db->limit(1);
		$this->db->where('cid', $cid);
		$this->db->delete('withdrawals');
	}
	//EOF withdrawals functionality

	function get_option($option_name = '')
	{
		if ($option_name == 'points_on')
		{
			//TODO: get this value from site options
			return FALSE;
		}
	}
	function total_winnings($user_id = 0)
	{
		//$sql = "SELECT SUM(trans.amount) as total FROM ".$this->db->dbprefix('user_balance_transactions')." as trans, ".$this->db->dbprefix('league')." as l WHERE trans.user_id = ".$this->db->escape($user_id)." AND trans.source = '5' AND trans.custom = l.cid";
		//$res = $this->db->query($sql)->result();

		$this->db->select("SUM(w.amount) as total");
		$this->db->from("league_winners as w");
		$this->db->join("league as l","w.league_id=l.cid");
		$this->db->where("w.user_id",$user_id);
		$res=$this->db->get()->result();

		if (count($res) == 1)
		{
			return $res[0]->total;
		}
		return 0;
	}
	function total_sport_winnings($user_id = 0, $sport = '')
	{
		//$sql = "SELECT SUM(trans.amount) as total FROM ".$this->db->dbprefix('user_balance_transactions')." as trans, ".$this->db->dbprefix('league')." as l WHERE trans.user_id = ".$this->db->escape($user_id)." AND trans.source = '5' AND trans.custom = l.cid";
		//$res = $this->db->query($sql)->result();

		$this->db->select("SUM(w.amount) as total");
		$this->db->from("league_winners as w");
		$this->db->join("league as l","w.league_id=l.cid");
		$this->db->where("w.user_id",$user_id);
		$this->db->where("l.sport",$sport);
		$res=$this->db->get()->result();

		if (count($res) == 1)
		{
			return $res[0]->total;
		}
		return 0;
	}

	function total_sport_wins($user_id = 0, $sport = '')
	{
		//$sql = "SELECT COUNT(trans.amount) as total FROM ".$this->db->dbprefix('user_balance_transactions')." as trans, ".$this->db->dbprefix('league')." as l WHERE trans.user_id = ".$this->db->escape($user_id)." AND trans.source = '5' AND trans.custom = l.cid";
		//$res = $this->db->query($sql)->result();
		$this->db->select("w.cid");
		$this->db->from("league_winners as w");
		$this->db->join("league as l","w.league_id=l.cid");
		$this->db->where("w.user_id",$user_id);
		$this->db->where("l.sport",$sport);

		$res=$this->db->count_all_results();
		return $res;
	}

	function get_reward_level($user_id) {

		$reward_tiers = $this->config->item('rake_reward_tiers');
		$reward_levels = $this->config->item('rake_reward_levels');

			//find out what tier this user is by adding up all the money spent last month
			$startdate = strtotime(date('Y-m-1',strtotime('first day of last month')).' 00:00:00');
			$enddate = strtotime(date('Y-m-t',strtotime('first day of last month')).' 23:59:59');

			//first get total funds locked
			$this->db->select('SUM(amount) as locked_amount, SUM(bonus_amount) as bonus_amount');
			$this->db->where('timestamp >=',$startdate);
			$this->db->where('timestamp <=',$enddate);
			$this->db->where('user_id',$user_id);
			$this->db->where('source',4);
			$la=$this->db->get('user_balance_transactions')->row();
			if(count($la) > 0) {
			$locked_amount=$la->locked_amount;
			$bonus_amount=$la->bonus_amount;
			} else {
			$locked_amount=0;
			$bonus_amount=0;
			}



			//now get total funds reversed
			$this->db->select('SUM(amount) as reversed_amount, SUM(bonus_amount) as reversed_bonus_amount');
			$this->db->where('timestamp >=',$startdate);
			$this->db->where('timestamp <=',$enddate);
			$this->db->where('user_id',$user_id);
			$this->db->where('source',7);
			$ra=$this->db->get('user_balance_transactions')->row();
			if(count($ra) > 0) {
			$reversed_amount=$ra->reversed_amount;
			$reversed_bonus_amount=$ra->reversed_bonus_amount;
			} else {
			$reversed_amount=0;
			$reversed_bonus_amount=0;
			}

			$total_spent = ($locked_amount+$bonus_amount) - ($reversed_amount+$reversed_bonus_amount);

			//what tier are we in
			$reward_percent = 0;
			foreach($reward_tiers as $tier=>$percent) {
				if($total_spent >= $tier) {
					$reward_percent = ($percent*100);
				} else {
					break;
				}
			}

			$reward_level = $reward_levels[$reward_percent];

			$reward_info = array('reward_tier'=>$reward_percent,'reward_level'=>$reward_level);
			//$reward_info = array('reward_tier'=>$total_spent,'reward_level'=>$reward_level);

			return $reward_info;
	}
}

