<?php
class Emails extends CI_Model {
    public $tableName = 'email_templates';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    // CRUD Base Setup
	public function get($order_by='', $where=''){
        if ($order_by != '') {
            $this->db->order_by($order_by['field'],$order_by['dir']);
        }
        if ($where != '') {
            $this->db->where();
        }

		$q = $this->db->get($this->tableName);
		return $q->result();
	}

	public function get_by_id($id) {
		$this->db->from($this->tableName);
		$this->db->where('cid', $id);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

	public function get_all(){
		$pages = $this->db->get('email_templates');
        return $pages->result();
	}

	public function get_template($slug=''){
		if(!empty($slug)){
			$this->db->where('slug',$slug);
            $this->db->limit(1);
            $template = $this->db->get('email_templates');

			if($template->num_rows() > 0){
				return $template->row();
			}
		}

        return false;
	}

	public function add_update($data,$id=''){
        $this->db->where('cid',$id);
        $check = $this->db->get($this->tableName);

        if ($check->num_rows() == 0) {
            $this->db->insert($this->tableName,$data);
        } else {
            $this->db->where('cid',$id);
            $this->db->update($this->tableName,$data);
        }
        return true;
    }

	public function delete($id){
		$this->db->where('cid', $id);
		$this->db->delete($this->tableName);
	}
    // END CRUD


	public function send_mail($to='', $slug='', $emailData=array(), $subject='', $adminEmail=false){
		ini_set('display_errors', 1); // set to 0 for production version
		error_reporting(E_ALL);

        $siteOptions = $this->Options->get_group(1);

		if(!empty($to) && !empty($slug)){
			$template = $this->get_template($slug);

            $language = $this->session->userdata('lang');
            $lang = empty($language) ? $siteOptions['default_language'] : $language;

			if($adminEmail || (!$adminEmail && $template )){
				$body = json_decode($template->body,true);
				$body = $this->render($body[$lang],$emailData);
				$body = $this->load->view(THEME.'/email',array('body'=>$body),true);

				if(empty($subject)){
					$subject = json_decode($template->subject,true);
					$subject = $this->render($subject[$lang],$emailData);
				}

				$adminEmail = $siteOptions['main_email'];
                $mgSettings = $this->Options->get_group(7);

                $this->load->library('email');
				$config['mailtype']             ='html';
				$config['crlf']                 = "\n";
				$config['useragent']            = "CodeIgniter";
                $config['mailpath']             = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
                $config['protocol']             = "smtp";
                $config['smtp_host']            = "smtp.mailgun.org";
                $config['smtp_port']            = 587;
                $config['mailtype']             = 'html';
                $config['charset']              = 'utf-8';
                $config['newline']              = "\r\n";
                $config['wordwrap']             = TRUE;
                $config['smtp_user']            = $mgSettings['mg_smtp_user'];
                $config['smtp_pass']            = $mgSettings['mg_smtp_pass'];

                $this->email->initialize($config);
				$this->email->from($adminEmail);
				$this->email->to($to);
				$this->email->subject($subject);
				$this->email->message($body);

				$headers = "From: ".$adminEmail."" . "\r\n";

                if($this->email->send()){
 					echo "Success<BR>";//exit;
                    echo $this->email->print_debugger();
//                    exit;
					return true;
				}else{
 					echo $this->email->print_debugger();
//                    exit;
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/*--------------------------------------------------------------------------
		RENDER EMAIL BODY
			This is a super simple templating engine that just drops element
			from the data array into the template using {{}} like a simple
			implementation of the mustache engine:
			http://mustache.github.io/

			Later if it needs to be more robust we can grab that library and
			actually use it but this should be good for now.
	--------------------------------------------------------------------------*/
	public function render($template='',$data=''){
		if(!empty($template)){
			if(!empty($data)){
				foreach($data as $k=>$v){
					$template=str_replace('&quot;', '"', $template);
					$template=str_replace('{{'.$k.'}}', $v, $template);
				}
			}
			return $template;
		}else{
			return false;
		}
	}

}

