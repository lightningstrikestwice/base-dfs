<?php
class Leagues extends CI_Model {
    public $tableName = 'league';
    public $templateTable = 'featured_templates';
    public $teamTable = 'league_team';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    // CRUD Base Setup
	public function get($order_by='', $where=''){
        if ($order_by != '') {
            $this->db->order_by($order_by['field'],$order_by['dir']);
        }
        if ($where != '') {
            $this->db->where();
        }

		$q = $this->db->get($this->tableName);
		return $q->result();
	}

    public function get_templates() {
        $data = array();
        $q = $this->db->get($this->templateTable);

        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach ($q as $r) {
                $data[$r->cid] = $r;
            }
        }

        return $data;
    }
	public function get_by_id($id) {
        $this->db->select('ci_league.*');
        $this->db->select('count(ci_league_team.cid) AS entries');
        $this->db->join('league_team', 'ci_league.cid = ci_league_team.league_id','left');

        $this->db->group_by('ci_league.cid');

		$this->db->from($this->tableName);
		$this->db->where($this->tableName.'.cid', $id);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

	public function get_template_by_id($id) {
		$this->db->from($this->templateTable);
		$this->db->where('cid', $id);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

	public function add_update($data,$id=''){
        $this->db->where('cid',$id);
        $check = $this->db->get($this->tableName);

        if ($check->num_rows() == 0) {
            $this->db->insert($this->tableName,$data);
        } else {
            $this->db->where('cid',$id);
            $this->db->update($this->tableName,$data);
        }
        return true;
    }

    public function add_update_template($data) {
        $this->db->where('size',$data['size']);
        $this->db->where('sport',$data['sport']);
        $this->db->where('type',$data['type']);
        $this->db->where('prize_structure',$data['prize_structue']);
        $this->db->where('entry_fee',$data['entry_fee']);
        $this->db->where('total_prize',$data['total_prize']);
        $this->db->where('multi_entry',$data['multi_entry']);
        $this->db->where('multi_entry_limit',$data['multi_entry_limit']);
        $this->db->where('guaranteed',$data['guaranteed']);
        $this->db->where('duration',$data['duration']);
        $this->db->where('feat',$data['feat']);

        $q = $this->db->get($this->templateTable);

        if ($q->num_rows() == 0) {
            $this->db->insert($this->templateTable,$data);
        } else {
            $this->db->where('size',$data['size']);
            $this->db->where('sport',$data['sport']);
            $this->db->where('type',$data['type']);
            $this->db->where('prize_structure',$data['prize_structue']);
            $this->db->where('entry_fee',$data['entry_fee']);
            $this->db->where('total_prize',$data['total_prize']);
            $this->db->where('multi_entry',$data['multi_entry']);
            $this->db->where('multi_entry_limit',$data['multi_entry_limit']);
            $this->db->where('guaranteed',$data['guaranteed']);
            $this->db->where('duration',$data['duration']);
            $this->db->where('feat',$data['feat']);

            $this->db->update($this->templateTable,$data);
        }

        return false;
    }
	public function delete($id){
		$this->db->where('cid', $id);
		$this->db->delete($this->tableName);
	}
    // END CRUD

    public function get_manage_leagues($type="upcoming", $start="", $end="") {
        $leagues = array();
        $leagueTeams = $this->Leagues->get_teams_array();

        if ($type == "upcoming") {
			$this->db->where("start_date >=",time());
			$this->db->where("status !=",2);
			$this->db->where("status !=",3);
			$this->db->where("status !=",4);
			$this->db->where("finalized",0);
        } else if ($type == "live") {
			$this->db->where("start_date <=",time());
			$this->db->where("status !=",2);
			$this->db->where("status !=",3);
			$this->db->where("status !=",4);
			$this->db->where("finalized",0);
        } else if ($type == "complete") {
            if ($start != "" && $end != "") {
                $s = explode('/', $start);
                $e = explode('/', $end);
                $startDate = mktime(0, 0, 0, $s[0], $s[1], $s[2]);
                $endDate = mktime(0, 0, 0, $e[0], $e[1], $e[2]);
            } else {
                $startDate = strtotime('-7 days');
                $endDate = time();
            }
			$this->db->where("status",3);
			$this->db->where("finalized",1);
			$this->db->where("created_date >=",$startDate);
			$this->db->where("created_date <=",$endDate);
        }

        $this->db->order_by("created_date","DESC");
        $q = $this->db->get('league');

        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach ($q as $r) {
                $r->numEntries = isset($leagueTeams[$r->cid]) ? count($leagueTeams[$r->cid]) : 0;
                $leagues[$r->cid] = $r;
            }
        }

        return $leagues;
    }

    public function get_teams_array() {
        $teams = array();

        $q = $this->db->get($this->teamTable);

        if ($q->num_rows() > 0) {
            $q = $q->result();
            foreach ($q as $r) {
                $teams[$r->league_id][] = $r;
            }
        }

        return $teams;
    }

    public function get_all_user_teams_array($userID) {
        $data = array();
        $this->db->where('user_id', $userID);
        $q = $this->db->get($this->teamTable);

        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach ($q as $r) {
                $data[$r->league_id][] = $r;
            }
        }
        return $data;
    }

    public function get_league_entries($leagueID) {
        $users = $this->Users->get_user_array();

        $this->db->where('league_id',$leagueID);
        $q = $this->get($this->teamTable);

        $entries = array();

        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach ($q as $r) {
                $entries[$r->user_id] = $users[$r->user_id];
            }
        }

        return $entries;
    }
}
