<?php
class Data_feeds extends CI_Model {
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    /****************************
    *
    *  Use this cURL function when the API Key is in the URL
    *
    **************************/

    function curl_for_data($url){
		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $output = curl_exec($ch);
	    curl_close($ch);

	    return $output;
	}


    /****************************
    *
    *  Use this cURL function when the API Key is NOT in the URL
    *
    **************************/

    function curl_for_data_auth_api($apiKey, $sport, $url){
        if($apiKey == ''){
            echo "api key for $sport is not set in admin site settings";
            die();
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
              "cache-control: no-cache",
              "ocp-apim-subscription-key: ".$apiKey
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:";
        } else {
          return $response;
        }
    }

    //**********************************************//
    //  This function assumes return of JSON array  //
    //**********************************************//
    public function get_api_data($url,$sport) {
        $options = $this->Options->get_group(3);
        $dataProvider = $options['data_provider'];
        $providerInfo = $this->config->item('data_provider')[$dataProvider];

        $apiKey = $providerInfo['apiKeys'][$sport];

        if($dataProvider == "fantasy_data"){
            $rawData = $this->curl_for_data_auth_api($apiKey, $sport, $url);
            $rawData = json_decode($rawData);
        } else {
            $url = $url."?apiKey=".$apiKey;
            $rawData = $this->curl_for_data($url);
        }

        return $rawData;

    }
}

