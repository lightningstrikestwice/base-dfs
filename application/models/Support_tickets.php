<?php
class Support_tickets extends CI_Model {
    public $tableName = 'support_ticket';
    public $msgTable = 'support_message';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    // CRUD Base Setup
	public function get($order_by='', $where=''){
        if ($order_by != '') {
            $this->db->order_by($order_by['field'],$order_by['dir']);
        }
        if ($where != '') {
            $this->db->where();
        }

		$q = $this->db->get($this->tableName);
		return $q->result();
	}

	public function get_by_id($id) {
		$this->db->from($this->tableName);
		$this->db->where('cid', $id);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

	public function add_update($data,$id=''){
        $this->db->where('cid',$id);
        $check = $this->db->get($this->tableName);

        if ($check->num_rows() == 0) {
            $this->db->insert($this->tableName,$data);
        } else {
            $this->db->where('cid',$id);
            $this->db->update($this->tableName,$data);
        }
        return true;
    }

	public function delete($id){
		$this->db->where('cid', $id);
		$this->db->delete($this->tableName);
	}
    // END CRUD

    public function get_list($type) {
        $tickets = array(
            "incomplete" => array(),
            "pending" => array(),
            "processing" => array(),
            "complete" => array()
        );

        if ($type=="my") {
            $this->db->where('assigned_user_id',$this->Users->id());
        }

        $this->db->order_by('timestamp','desc');
        $q = $this->db->get('support_ticket');

        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach ($q as $r){
                if ($r->status < 3) {
                    $tickets['incomplete'][] = $r;
                }

                if ($r->status == 1) {
                    $tickets['pending'][] = $r;
                } elseif ($r->status == 2) {
                    $tickets['processing'][] = $r;
                } elseif ($r->status == 3) {
                    $tickets['complete'][] = $r;
                }
            }
        }

        return $tickets;
    }

	public function add_note($ticketID='',$userID='',$message=''){
		if(!empty($ticketID) && !empty($userID) && !empty($message)){
			$insert = array(
				"ticket_id" => $ticketID,
				"user_id" => $userID,
				"message" => $message,
				"timestamp" => time()
			);
			$this->db->insert($this->msgTable,$insert);
		}
	}

    public function delete_note($noteID=''){
		if(!empty($noteID)){
			$this->db->where('cid',$noteID);
			$this->db->limit(1);
			$this->db->delete($this->msgTable);
		}
	}

    public function get_notes($ticketID) {
        $users = $this->Users->get_user_array();

        $this->db->where('ticket_id',$ticketID);
        $this->db->order_by('timestamp','desc');
        $q = $this->db->get($this->msgTable);

        if ($q->num_rows() > 0) {
            $q = $q->result();

            foreach ($q as $r) {
                $r->user = $users[$r->user_id];
            }
            return $q;
        }

        return false;
    }
}

