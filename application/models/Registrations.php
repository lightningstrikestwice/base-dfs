<?php
class Registrations extends CI_Model {
    public $tableName = 'users';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    // CRUD Base Setup
	public function get($order_by='', $where=''){
        if ($order_by != '') {
            $this->db->order_by($order_by['field'],$order_by['dir']);
        }
        if ($where != '') {
            $this->db->where();
        }

		$q = $this->db->get($this->tableName);
		return $q->result();
	}

	public function get_by_id($id) {
		$this->db->from($this->tableName);
		$this->db->where('cid', $id);
		$q = $this->db->get();
        $q = $q->row();

        return $q;
	}

	public function add_update($data,$id=''){
        $this->db->where('cid',$id);
        $check = $this->db->get($this->tableName);

        if ($check->num_rows() == 0) {
            $this->db->insert($this->tableName,$data);
        } else {
            $this->db->where('cid',$id);
            $this->db->update($this->tableName,$data);
        }
        return true;
    }

	public function delete($id){
		$this->db->where('cid', $id);
		$this->db->delete($this->tableName);
	}
    // END CRUD

	function save_avatar($user_id, $filename, $new_user = FALSE){
        $pathinfo = pathinfo($filename);

        copy('./uploads/tmp/'.$filename, './uploads/user_avatar/'.$user_id.'.'.$pathinfo['extension']);
        unlink('./uploads/tmp/'.$filename);
        $this->db->where('cid', $user_id);
        $this->db->limit(1);
        $this->db->update('users', array('avatar' => $user_id.'.'.$pathinfo['extension']));
        $this->session->set_userdata('avatar',$user_id.'.'.$pathinfo['extension']);

        //now put these images on all the servers
        $domain = $this->Options->get_option('website_url');

        $fields = array();
        $fields['avatar'] = '@./uploads/user_avatar'.$user_id.'.'.$pathinfo['extension'].';type=image/'.$pathinfo['extension'];

        $curl_handle = curl_init();
        $api_url = '//'.$domain.'/index.php/upload/user_avatar';
        curl_setopt($curl_handle, CURLOPT_URL, $api_url);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_POST, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($curl_handle);
	}

}

