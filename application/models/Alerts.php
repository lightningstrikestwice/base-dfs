<?php
class Alerts extends CI_Model {
    public $tableName = 'email_templates';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    function set($type,$msg) {
        $this->session->set_flashdata($type,$msg);
    }
}

