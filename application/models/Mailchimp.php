<?php
class Mailchimp extends CI_Model {
    public $tableName = 'users';

    function __construct(){
        // Call the Model constructor
        parent::__construct();
		$this->apikey = $this->Options->get_option('mc_api');
		$this->datacenter = $this->Options->get_option('mc_datacenter');
    }

	public function process($action='',$post=''){
		if(!empty($post) && !empty($action)){
			$url="https://".$this->datacenter.".api.mailchimp.com/2.0/".$action;
			//echo "<pre>".print_R($url,true)."</pre>";
			//echo "<pre>".print_R($post,true)."</pre>";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response=curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			//echo "<pre>".print_R($response, true)."</pre>";
			//echo "<pre>".print_R($httpCode, true)."</pre>";
			curl_close($ch);
			/*if($httpCode!=200){
				return false;
			}else{*/
				return $response;
			//}


		}else{
			return false;
		}
	}
	public function lists_list(){
		$post=array(
			"apikey"=>$this->apikey,
		);
		return json_decode($this->process('lists/list.json',json_encode($post)));
	}

	public function lists_subscribe($list_id='',$email='', $email_type='html'){
		if(!empty($list_id) && !empty($email)){
			$post=array(
				"apikey"=>$this->apikey,
				"id"=>$list_id,
				"email"=>array("email"=>$email),
				"email_type"=>in_array($email_type,array('html','text'))?$email_type:'html'
			);
			return json_decode($this->process('lists/subscribe.json',json_encode($post)));
		}else{
			return false;
		}
	}

	public function lists_unsubscribe($list_id='',$email=''){
		if(!empty($list_id) && !empty($email)){
			$post=array(
				"apikey"=>$this->apikey,
				"id"=>$list_id,
				"email"=>array("email"=>$email),
			);
			return json_decode($this->process('lists/unsubscribe.json',json_encode($post)));
		}else{
			return false;
		}
	}

	public function lists_member_info($list_id='',$email=''){
		if(!empty($list_id) && !empty($email) && !empty($email)){
			$post=array(
				"apikey"=>$this->apikey,
				"id"=>$list_id,
			);
			if(is_array($email)){
				foreach($email as $e) $post["emails"][]=array("email"=>$e);
			}else{
				$post["emails"][]=array("email"=>$email);
			}

			return json_decode($this->process('lists/member-info.json',json_encode($post)));
		}else{
			return false;
		}
	}
}
