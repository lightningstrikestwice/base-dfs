<?php
class Misc extends CI_Model {

    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }

    public function multi_sort_array($array, $key, $dir) {
        $sort = array();
        foreach($array as $sorted) {
            $sort[] = $sorted[$key];
        }

        array_multisort($sort, $dir, $array);

        return $array;
    }

    public function convert_to_array($object) {
        $array = json_decode(json_encode($object),true);

        return $array;
    }

    public function convert_to_object($array) {
        $object = json_decode(json_encode($array));

        return $object;
    }

    function ordinal($number) {
        $ends = array('th','st','nd','rd','th','th','th','th','th','th');
        if ((($number % 100) >= 11) && (($number%100) <= 13))
            return $number. 'th';
        else
            return $number. $ends[$number % 10];
    }

    function get_avatar_url($userID) {
        if (!is_array($userID)) {
            $user = $this->Users->get_by_id($userID);
        } else {
            $user = $userID;
        }
        $url = 'uploads/user_avatar/'.$user->avatar;
        $image = base_url().'uploads/user_avatar/'.$user->avatar;

        if(! file_exists(FCPATH.$url)){
            $image = base_url().'assets/default/img/default.jpg';
        }

        return $image;
    }

    function get_headshot_url($id) {
        $url = 'headshots/'.$id.'.jpg';
        $image = base_url().'headshots/'.$id.'.jpg';

        if(! file_exists(FCPATH.$url)){
            $image = base_url().'assets/default/img/default.jpg';
        }

        return $image;
    }

	function states_list(){
		$state_list = array(
//				''=>'',
//				'AB'=>"Alberta",
				'AL'=>"Alabama",
                'AK'=>"Alaska",
                'AZ'=>"Arizona",
                'AR'=>"Arkansas",
//				'BC'=>"British Columbia",
                'CA'=>"California",
                'CO'=>"Colorado",
                'CT'=>"Connecticut",
                'DE'=>"Delaware",
                'DC'=>"District Of Columbia",
                'FL'=>"Florida",
                'GA'=>"Georgia",
                'HI'=>"Hawaii",
                'ID'=>"Idaho",
                'IL'=>"Illinois",
                'IN'=>"Indiana",
                'IA'=>"Iowa",
                'KS'=>"Kansas",
                'KY'=>"Kentucky",
                'LA'=>"Louisiana",
                'ME'=>"Maine",
                'MD'=>"Maryland",
                'MA'=>"Massachusetts",
                'MI'=>"Michigan",
                'MN'=>"Minnesota",
                'MS'=>"Mississippi",
                'MO'=>"Missouri",
                'MT'=>"Montana",
                'NE'=>"Nebraska",
//				'NB'=>"New Brunswick",
//				'NF'=>"Newfoundland",
//				'NS'=>"Nova Scotia",
                'NV'=>"Nevada",
                'NH'=>"New Hampshire",
                'NJ'=>"New Jersey",
                'NM'=>"New Mexico",
                'NY'=>"New York",
                'NC'=>"North Carolina",
                'ND'=>"North Dakota",
//				'NT'=>"Northwest Territories",
//				'MB'=>"Manitoba",
                'OH'=>"Ohio",
                'OK'=>"Oklahoma",
//				'ON'=>"Ontario",
                'OR'=>"Oregon",
                'PA'=>"Pennsylvania",
//				'PE'=>"Prince Edward Island",
//				'QC'=>"Quebec",
                'RI'=>"Rhode Island",
                'SC'=>"South Carolina",
                'SD'=>"South Dakota",
//				'SK'=>"Saskatchewan",
                'TN'=>"Tennessee",
                'TX'=>"Texas",
                'UT'=>"Utah",
                'VT'=>"Vermont",
                'VA'=>"Virginia",
                'WA'=>"Washington",
                'WV'=>"West Virginia",
                'WI'=>"Wisconsin",
                'WY'=>"Wyoming",
//				'YT'=>"Yukon"
				);
		return $state_list;
	}

	function province_list(){
		$province_list = array(
//            ''=>'',
            'AB'=>"Alberta",
            'BC'=>"British Columbia",
            'MB'=>"Manitoba",
            'NB'=>"New Brunswick",
            'NL'=>"Newfoundland and Labrador",
            'NS'=>"Nova Scotia",
            'NT'=>"Northwest Territories",
            'NU'=>"Nanavut",
            'ON'=>"Ontario",
            'PE'=>"Prince Edward Island",
            'QC'=>"Quebec",
            'SK'=>"Saskatchewan",
            'YT'=>"Yukon"
		);

		return $province_list;
	}

    function countries_list(){

		$countries = array(
//			''=>'',
//			"US"=>"UNITED STATES",
//			"AF"=>"AFGHANISTAN",
//			"AX"=>"ALAND ISLANDS",
//			"AL"=>"ALBANIA",
//			"DZ"=>"ALGERIA",
//			"AS"=>"AMERICAN SAMOA",
//			"AD"=>"ANDORRA",
//			"AO"=>"ANGOLA",
//			"AI"=>"ANGUILLA",
//			"AQ"=>"ANTARCTICA",
//			"AG"=>"ANTIGUA AND BARBUDA",
//			"AR"=>"ARGENTINA",
//			"AM"=>"ARMENIA",
//			"AW"=>"ARUBA",
//			"AU"=>"AUSTRALIA",
//			"AT"=>"AUSTRIA",
//			"AZ"=>"AZERBAIJAN",
//			"BS"=>"BAHAMAS",
//			"BH"=>"BAHRAIN",
//			"BD"=>"BANGLADESH",
//			"BB"=>"BARBADOS",
//			"BY"=>"BELARUS",
//			"BE"=>"BELGIUM",
//			"BZ"=>"BELIZE",
//			"BJ"=>"BENIN",
//			"BM"=>"BERMUDA",
//			"BT"=>"BHUTAN",
//			"BO"=>"BOLIVIA",
//			"BA"=>"BOSNIA AND HERZEGOVINA",
//			"BW"=>"BOTSWANA",
//			"BV"=>"BOUVET ISLAND",
//			"BR"=>"BRAZIL",
//			"IO"=>"BRITISH INDIAN OCEAN TERRITORY",
//			"BN"=>"BRUNEI DARUSSALAM",
//			"BG"=>"BULGARIA",
//			"BF"=>"BURKINA FASO",
//			"BI"=>"BURUNDI",
//			"KH"=>"CAMBODIA",
//			"CM"=>"CAMEROON",
			"CA"=>"CANADA",
//			"CV"=>"CAPE VERDE",
//			"CI"=>"C�TE D'IVOIRE",
//			"KY"=>"CAYMAN ISLANDS",
//			"CF"=>"CENTRAL AFRICAN REPUBLIC",
//			"TD"=>"CHAD",
//			"CL"=>"CHILE",
//			"CN"=>"CHINA",
//			"CX"=>"CHRISTMAS ISLAND",
//			"CC"=>"COCOS (KEELING) ISLANDS",
//			"CO"=>"COLOMBIA",
//			"KM"=>"COMOROS",
//			"CG"=>"CONGO",
//			"CD"=>"CONGO, THE DEMOCRATIC REPUBLIC OF THE",
//			"CK"=>"COOK ISLANDS",
//			"CR"=>"COSTA RICA",
//			"HR"=>"CROATIA",
//			"CU"=>"CUBA",
//			"CY"=>"CYPRUS",
//			"CZ"=>"CZECH REPUBLIC",
//			"DK"=>"DENMARK",
//			"DJ"=>"DJIBOUTI",
//			"DM"=>"DOMINICA",
//			"DO"=>"DOMINICAN REPUBLIC",
//			"EC"=>"ECUADOR",
//			"EG"=>"EGYPT",
//			"SV"=>"EL SALVADOR",
//			"GQ"=>"EQUATORIAL GUINEA",
//			"ER"=>"ERITREA",
//			"EE"=>"ESTONIA",
//			"ET"=>"ETHIOPIA",
//			"FK"=>"FALKLAND ISLANDS (MALVINAS)",
//			"FO"=>"FAROE ISLANDS",
//			"FJ"=>"FIJI",
//			"FI"=>"FINLAND",
//			"FR"=>"FRANCE",
//			"GF"=>"FRENCH GUIANA",
//			"PF"=>"FRENCH POLYNESIA",
//			"TF"=>"FRENCH SOUTHERN TERRITORIES",
//			"GA"=>"GABON",
//			"GM"=>"GAMBIA",
//			"GE"=>"GEORGIA",
//			"DE"=>"GERMANY",
//			"GH"=>"GHANA",
//			"GI"=>"GIBRALTAR",
//			"GR"=>"GREECE",
//			"GL"=>"GREENLAND",
//			"GD"=>"GRENADA",
//			"GP"=>"GUADELOUPE",
//			"GU"=>"GUAM",
//			"GT"=>"GUATEMALA",
//			"GN"=>"GUINEA",
//			"GW"=>"GUINEA-BISSAU",
//			"GY"=>"GUYANA",
//			"HT"=>"HAITI",
//			"HM"=>"HEARD ISLAND AND MCDONALD ISLANDS",
//			"VA"=>"HOLY SEE (VATICAN CITY STATE)",
//			"HN"=>"HONDURAS",
//			"HK"=>"HONG KONG",
//			"HU"=>"HUNGARY",
//			"IS"=>"ICELAND",
//			"IN"=>"INDIA",
//			"ID"=>"INDONESIA",
//			"IR"=>"IRAN ISLAMIC REPUBLIC OF",
//			"IQ"=>"IRAQ",
//			"IE"=>"IRELAND",
//			"IL"=>"ISRAEL",
//			"IT"=>"ITALY",
//			"JM"=>"JAMAICA",
//			"JP"=>"JAPAN",
//			"JO"=>"JORDAN",
//			"KZ"=>"KAZAKHSTAN",
//			"KE"=>"KENYA",
//			"KI"=>"KIRIBATI",
//			"KP"=>"KOREA DEMOCRATIC PEOPLE\'S REPUBLIC OF",
//			"KR"=>"KOREA REPUBLIC OF",
//			"KW"=>"KUWAIT",
//			"KG"=>"KYRGYZSTAN",
//			"LA"=>"LAO PEOPLE\'S DEMOCRATIC REPUBLIC",
//			"LV"=>"LATVIA",
//			"LB"=>"LEBANON",
//			"LS"=>"LESOTHO",
//			"LR"=>"LIBERIA",
//			"LY"=>"LIBYAN ARAB JAMAHIRIYA",
//			"LI"=>"LIECHTENSTEIN",
//			"LT"=>"LITHUANIA",
//			"LU"=>"LUXEMBOURG",
//			"MO"=>"MACAO",
//			"MK"=>"MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF",
//			"MG"=>"MADAGASCAR",
//			"MW"=>"MALAWI",
//			"MY"=>"MALAYSIA",
//			"MV"=>"MALDIVES",
//			"ML"=>"MALI",
//			"MT"=>"MALTA",
//			"MH"=>"MARSHALL ISLANDS",
//			"MQ"=>"MARTINIQUE",
//			"MR"=>"MAURITANIA",
//			"MU"=>"MAURITIUS",
//			"YT"=>"MAYOTTE",
//			"MX"=>"MEXICO",
//			"FM"=>"MICRONESIA, FEDERATED STATES OF",
//			"MD"=>"MOLDOVA, REPUBLIC OF",
//			"MC"=>"MONACO",
//			"MN"=>"MONGOLIA",
//			"MS"=>"MONTSERRAT",
//			"MA"=>"MOROCCO",
//			"MZ"=>"MOZAMBIQUE",
//			"MM"=>"MYANMAR",
//			"NA"=>"NAMIBIA",
//			"NR"=>"NAURU",
//			"NP"=>"NEPAL",
//			"NL"=>"NETHERLANDS",
//			"AN"=>"NETHERLANDS ANTILLES",
//			"NC"=>"NEW CALEDONIA",
//			"NZ"=>"NEW ZEALAND",
//			"NI"=>"NICARAGUA",
//			"NE"=>"NIGER",
//			"NG"=>"NIGERIA",
//			"NU"=>"NIUE",
//			"NF"=>"NORFOLK ISLAND",
//			"MP"=>"NORTHERN MARIANA ISLANDS",
//			"NO"=>"NORWAY",
//			"OM"=>"OMAN",
//			"PK"=>"PAKISTAN",
//			"PW"=>"PALAU",
//			"PS"=>"PALESTINIAN TERRITORY, OCCUPIED",
//			"PA"=>"PANAMA",
//			"PG"=>"PAPUA NEW GUINEA",
//			"PY"=>"PARAGUAY",
//			"PE"=>"PERU",
//			"PH"=>"PHILIPPINES",
//			"PN"=>"PITCAIRN",
//			"PL"=>"POLAND",
//			"PT"=>"PORTUGAL",
//			"PR"=>"PUERTO RICO",
//			"QA"=>"QATAR",
//			"RE"=>"REUNION",
//			"RO"=>"ROMANIA",
//			"RU"=>"RUSSIAN FEDERATION",
//			"RW"=>"RWANDA",
//			"SH"=>"SAINT HELENA",
//			"KN"=>"SAINT KITTS AND NEVIS",
//			"LC"=>"SAINT LUCIA",
//			"PM"=>"SAINT PIERRE AND MIQUELON",
//			"VC"=>"SAINT VINCENT AND THE GRENADINES",
//			"WS"=>"SAMOA",
//			"SM"=>"SAN MARINO",
//			"ST"=>"SAO TOME AND PRINCIPE",
//			"SA"=>"SAUDI ARABIA",
//			"SN"=>"SENEGAL",
//			"CS"=>"SERBIA AND MONTENEGRO",
//			"SC"=>"SEYCHELLES",
//			"SL"=>"SIERRA LEONE",
//			"SG"=>"SINGAPORE",
//			"SK"=>"SLOVAKIA",
//			"SI"=>"SLOVENIA",
//			"SB"=>"SOLOMON ISLANDS",
//			"SO"=>"SOMALIA",
//			"ZA"=>"SOUTH AFRICA",
//			"GS"=>"SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS",
//			"ES"=>"SPAIN",
//			"LK"=>"SRI LANKA",
//			"SD"=>"SUDAN",
//			"SR"=>"SURINAME",
//			"SJ"=>"SVALBARD AND JAN MAYEN",
//			"SZ"=>"SWAZILAND",
//			"SE"=>"SWEDEN",
//			"CH"=>"SWITZERLAND",
//			"SY"=>"SYRIAN ARAB REPUBLIC",
//			"TW"=>"TAIWAN PROVINCE OF CHINA",
//			"TJ"=>"TAJIKISTAN",
//			"TZ"=>"TANZANIA UNITED REPUBLIC OF",
//			"TH"=>"THAILAND",
//			"TL"=>"TIMOR-LESTE",
//			"TG"=>"TOGO",
//			"TK"=>"TOKELAU",
//			"TO"=>"TONGA",
//			"TT"=>"TRINIDAD AND TOBAGO",
//			"TN"=>"TUNISIA",
//			"TR"=>"TURKEY",
//			"TM"=>"TURKMENISTAN",
//			"TC"=>"TURKS AND CAICOS ISLANDS",
//			"TV"=>"TUVALU",
//			"UG"=>"UGANDA",
//			"UA"=>"UKRAINE",
//			"AE"=>"UNITED ARAB EMIRATES",
//			"GB"=>"UNITED KINGDOM",
//			"UM"=>"UNITED STATES MINOR OUTLYING ISLANDS",
//			"UY"=>"URUGUAY",
//			"UZ"=>"UZBEKISTAN",
//			"VU"=>"VANUATU",
//			"VE"=>"VENEZUELA",
//			"VN"=>"VIETNAM",
//			"VG"=>"VIRGIN ISLANDS BRITISH",
//			"VI"=>"VIRGIN ISLANDS U.S.",
//			"WF"=>"WALLIS AND FUTUNA",
//			"EH"=>"WESTERN SAHARA",
//			"YE"=>"YEMEN",
//			"ZM"=>"ZAMBIA",
//			"ZW"=>"ZIMBABWE"
		);
		return $countries;
	}

    function location_lookup($ip) {
        $url = "http://ip-api.com/json/".$ip;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch), true);

        return $response;
    }

    function months() {
        $months = array(
            1 => "January",
            2 => "February",
            3 => "March",
            4 => "April",
            5 => "May",
            6 => "June",
            7 => "July",
            8 => "August",
            9 => "September",
            10 => "October",
            11 => "November",
            12 => "December",
        );

        return $months;
    }

    function get_ip() {
        //IP Address
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    function sources() {
        $sources = array(
            '0' => 'None Given',
            '1' => 'From a friend',
            '2' => 'Radio',
            '3' => 'Magazine',
            '4' => 'Search engine (e.g. Google)',
            '5' => 'Online advertisement',
            '6' => 'News article or blog post',
            '7' => 'Television',
            '8' => 'Local event',
            '9' => 'Other'
        );

        return $sources;
    }

    function get_sport_icon($sport) {
        $icons = array(
            "NFL" => "fas fa-football-ball",
            "MLB" => "fas fa-baseball-ball",
            "NBA" => "fas fa-basketball-ball",
            "GOLF" => "fas fa-golf-ball",
            "CFB" => "fas fa-football-ball",
            "CBK" => "fas fa-basketball-ball",
            "NHL" => "fas fa-hockey-sticks"
        );

        return $icons[$sport];
    }

    function stringify_array($array, $delimiter) {
        $c = 0;
        $arrayString = "";
        $arrayList = json_decode($array,true);

        foreach ($arrayList as $item) {
            $c++;
            $sep = $c > 1 ? $delimiter : "";

            $arrayString .= $sep.$item;
        }

        return $arrayString;
    }
}
