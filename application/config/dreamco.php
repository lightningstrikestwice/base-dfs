<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Define Theme Constants
define('THEME', 'default');
define('ADMIN_THEME', 'admin');
define('GLOBAL_CONTENT', 'global');

// Set Timezone
date_default_timezone_set('America/New_York');

// Set Dev Constant
define('DEV', '/dev');

/*******************************
* VARS to add to Options Table
*******************************/
$config['default_email_rules']=array("system","game_creation","request_confirmation","request_fullfilled","game_results","salary_cap_changes","expired_games","game_removal","game_update","challenged");
$config['cutoff_offset']=0;

$config['sport_order']=array("NFL"=>1,"MLB"=>2,"NBA"=>3,"GOLF"=>4,"NHL"=>5,"CFB"=>6,"CBK"=>7,"NASCAR"=>8);

$config['bonus_drip_rate']=0.05;

/*************************
* Common Config Vars
*************************/
$config['maintenance_mode']=false;
$config['version']='b055';
$config['master_ip'] = '10.178.1.82';

// Sport ID prefix for player IDs so no duplicates from data_provider
$config['sportid_nfl'] 	= '10';
$config['sportid_golf'] = '20';
$config['sportid_nba'] 	= '30';
$config['sportid_nhl']	= '40';
$config['sportid_mlb']	= '50';
$config['sportid_soc']  = '60';

$config['sport_label']=array(
	'NBA'   => 'NBA',
	'CBK'   => 'CBB',
	'NHL'   => 'NHL',
	'NFL'   => 'NFL',
	'MLB'   => 'MLB',
	'GOLF'  => 'PGA'
);

$config['game_types']=array(
//		1 => 'Salary Cap',
//		2 => 'Custom Salary Cap',
//		3 => 'Pickem',
//		4 => 'Over/Under',
//		5 => 'Bracket',
    10 => 'Standard',
    11 => 'Over/Under',
    12 => 'Point Spread',
//    13 => 'Extreme Point Spread Survivor',
    14 => 'Perfect Pick',
    15 => 'Confidence',
    111 => 'Pickem ',
);

$config['durations'] = array(
    "one" => "One Day Only",
    "daily" => "Daily",
    "weekly" => "Weekly",
    "single" => "Single Event"
);

$config['salary_cap_options']=array(
    50000   => '50k',
    55000   => '55k',
    60000   => '60k',
    65000   => '65k',
    70000   => '70k',
    75000   => '75k',
    80000   => '80k',
    85000   => '85k',
    100000  => '100k',
    115000  => '115k',
);

$config['custom_salary_cap_options']=array(
    70000   => '70k',
    85000   => '85k',
    100000  => '100k',
    115000  => '115k',
    130000  => '130k',
);

$config['game_type_data'] = array(
    // Standard Survivor
    10 => array(
        "sports" => array(
            "NFL",
            "NBA",
            "MLB",
            "NHL",
            "CBK",
        ),
        "timeframe" => array(
            "daily" => "Daily Contest",
            "weekly" => "Weekly Contest"
        ),
    ),
    // Over/Under Survivor
    11 => array(
        "sports" => array(
            "NFL",
            "NBA",
            "MLB",
            "CBK"
        ),
        "timeframe" => array(
            "daily" => "Daily Contest",
            "weekly" => array("NFL"),
        ),
    ),
    // Point Spread Survivor
    12 => array(
        "sports" => array(
            "NFL",
            "NBA",
            "MLB",
            "NHL",
            "CBK"
        ),
        "timeframe" => array(
            "daily" => "Daily Contest",
            "weekly" => array("NFL"),
        ),
    ),
    // Extreme Point Spread Survivor
//    13 => array(
//        "sports" => array(
//            "NFL",
//            "NBA",
//            "MLB",
//            "NHL",
//            "CBK"
//        ),
//        "timeframe" => array(
//            "daily" => "Daily Contest",
//            "weekly" => array("NFL"),
//        ),
//    ),
    // Perfect Pick Survivor
    14 => array(
        "sports" => array(
            "NFL",
            "NBA",
            "MLB",
            "NHL",
            "CBK"
        ),
        "timeframe" => array(
            "one" => "1 Day only",
        ),
    ),
    // Confidence Pool
    15 => array(
        "sports" => array(
            "NFL",
            "NBA",
            "MLB",
            "NHL",
            "CBK"
        ),
        "timeframe" => array(
            "one" => "1 Day Only",
        ),
    ),
    // Pickem
    111 => array(
        "sports" => array(
            "GOLF",
        ),
        "timeframe" => array(
            "single" => "Single Event"
        ),
    ),
);

$config['linked_account_provider_data']=array(
	"facebook"=>array(
		'icon' => 'icon-facebook',
		'user_table_column'=>'facebook_id',
		'usermeta_key'=>'facebook_connect',
		'id' => '790288281098804',
        'secret' => '75cbe2a2d65a2deab1cd1fe9f9f7132f',
		'scope' => array('email','public_profile','user_friends'),
		'keys' => array(
				'id'=>"id",
				'first_name'=>"first_name",
				'user_friends'=>"summary",
				'last_name'=>"last_name",
				'username'=>"username",
				'birthday'=>"birthday",
				'email'=>"email",
				'gender'=>"gender",
				'location'=>"location",
				'link'=>"link",
				'image'=>"image",
			)
	),
	"twitter"=>array(
		'icon' => 'icon-twitter',
		'secret' => 'ato2qJ4ry6ik6x6jmsxn9TS4zeNMUK8jwrur69wDjKbaPL7K9I',
		'user_table_column'=>'twitter_id',
		'usermeta_key'=>'twitter_connect',
		'id' => 'vYO8YQKdtUfTQ28fXricUX2cE'

	),
	"rotogrinder"=>array(
		'icon' => 'icon-roto',
		'secret' => '',
		'user_table_column'=>'rotogrinder',
		'usermeta_key'=>'roto_connect'

	),
	"google"=>array(
		'icon' => 'icon-google-plus',
		'user_table_column'=>'google_id',
		'usermeta_key'=>'google_connect',
		'id' => '865676584431.apps.googleusercontent.com',
        'secret' => 'NYBH2z8bKt2fKUpcdDWTV5jf',
	),
	"linkedin"=>array(
		'icon' => 'icon-linkedin',
		'user_table_column'=>'linkedin_id',
		'usermeta_key'=>'linkedin_connect',
		'id' => 'q1gvatcveysq',
        'secret' => 'vyusYGnpds7H7b34',
	),
);

$config['usersources']=array(
    '0' => 'None Given',
	'1' => 'From a friend',
	'2' => 'Radio',
	'3' => 'Magazine',
	'4' => 'Search engine (e.g. Google)',
	'5' => 'Online advertisement',
	'6' => 'News article or blog post',
	'7' => 'Television',
	'8' => 'Local event',
	'9' => 'Other'

);

$config['support_statuses'] = array(
    0 => array(
        "status"=>"Incomplete",
        "badge" =>""
    ),
    1 => array(
        "status"=>"Pending",
        "badge" =>"badge-info"
    ),
    2 => array(
        "status"=>"Processing",
        "badge" =>"badge-warning"
    ),
    3 => array(
        "status"=>"Complete",
        "badge" =>"badge-success"
    ),
);

$config['timeWindows'] = array(
    1 => "Yesterday",
    3 => "The Last 3 Days",
    "week" => "The Last 7 Days",
    "month" => "The Last Month",
    "year" => "The Last Year",
    "ALL" => "Ever"
);

$config['ticket_types']=array(
	1=>'Bronze',
	2=>'Silver',
	3=>'Gold',
	4=>'Platinum'
);

$config['withdrawal_statuses'] = array(
    0 => array(
        "class" => "empty",
        "desc" => "Pending"
    ),
    1 => array(
        "class" => "warning",
        "desc" => "Pending Info"
    ),
    2 => array(
        "class" => "danger",
        "desc" => "Declined"
    ),
    3 => array(
        "class" => "success",
        "desc" => "Paid"
    ),
);

$config['sport_icons'] = array(
    "NFL" => "fa-football-ball",
    "MLB" => "fa-baseball-ball",
    "NBA" => "fa-basketball-ball",
    "NHL" => "fa-hockey-sticks",
    "GOLF" => "fa-golf-ball",
    "CBK" => "fa-basketball-ball",
    "CFB" => "fa-football-ball"
);

/********************
* Sport Config Vars
********************/
$config['sport_data']=array (
    'NFL' => array (
        'default_salcap'=>60000,
        'earlygamecutoff' => 1600,
        'mingames' => 2,
        'day_limit' => 7,
        'dow_limit' => 2,
        'gametimes' => array (
            'WEEK' => array (
                'days' => array (
                    0 => 4,
                    1 => 5,
                    2 => 6,
                    3 => 0,
                    4 => 1,
                ),
                'start' => false,
                'end'   => 2359,
            ),
            '2DAY' => array (
                'days' => array (
                    0 => 0,
                    1 => 1,
                ),
                'start' => false,
                'end'   => 2359
            ),
            'ALL' => array (
                'days'  => 1,
                'start' => false,
                'end'   => 2359,
            ),
            'AM' => array(
                'days'  => 1,
                'start' => false,
                'end'   =>1559,
            ),
            'PM' => array (
                'days'  => 1,
                'start' => 1600,
                'end'   => 2359,
            ),
            'PRIME' => array (
                'days'  => array (
                    0 => 0,
                    1 => 1,
                ),
                'start' => 2000,
                'end'   => 2359,
            ),
            'AFTERNOON' => array (
                'days'  => array (
                    0 => 0,
                    1 => 1,
                ),
                'start' => 1600,
                'end'   => 2359,
            ),
        ),
        'total_players' => 9,
        'total_periods' => 4,
        'period_time' => 15,
        'team_makeup' => array (
            'QB'    => array ('limit' => 1),
            'RB'    => array ('limit' => 2),
            'WR'    => array ('limit' => 3),
            'TE'    => array ('limit' => 1),
            'K'     => array('limit' => 1),
            'D'     => array ('limit' => 1),
        ),
        'positions' => array ('QB', 'RB', 'WR', 'TE', 'K', 'D'),
        'pos_breakdown' => array(
            'QB'    => 'QB',
            'RB'    => 'RB',
            'FB'    => 'RB',
            'WR'    => 'WR',
            'TE'    => 'TE',
            'K'     => 'K',
            'D'     => 'D'
        ),
    ),
    'NBA' => array (
        'default_salcap'=>60000,
        'earlygamecutoff' => 2000,
        'mingames' => 2,
        'day_limit' => 2,
        'gametimes' => array (
            'ALL' => array (
                'days'  => 1,
                'start' => false,
                'end'   => 2359,
            ),
            'EVE' => array (
                'days'  => 1,
                'start' => 1700,
                'end'   => 2359,
            ),
            'PM' => array (
                'days'  => 1,
                'start' => 2100,
                'end'   => 2359,
            ),
            'PRIME' => array (
                'days'  => 1,
                'start' => 1900,
                'end'   => 2100,
            ),
        ),
        'total_players' => 9,
        'total_periods' => 4,
        'period_time' => 12,
        'team_makeup' => array (
            'PG'    => array ('limit' => 2),
            'SG'    => array ('limit' => 2),
            'SF'    => array ('limit' => 2),
            'PF'    => array ('limit' => 2),
            'C'     => array ('limit' => 1),
        ),
        'positions' => array ('PG', 'SG', 'SF', 'PF', 'C'),
    ),
    'CBK' => array (
        'default_salcap'=>50000,
        'earlygamecutoff' => 2000,
        'mingames' => 2,
        'day_limit' => 1,
        'gametimes' => array (
            'ALL' => array (
                'days'  => 1,
                'start' => false,
                'end'   => 2359,
            ),
            'AM' => array(
                'days'  => 1,
                'start' => false,
                'end'   => 1859,
            ),
            'EVE' => array (
                'days'  => 1,
                'start' => 1700,
                'end'   => 2359,
            ),
            'PM' => array (
                'days'  => 1,
                'start' => 2100,
                'end'   => 2359,
            ),
            'PRIME' => array (
                'days'  => 1,
                'start' => 1900,
                'end'   => 2100,
            ),
        ),
        'total_players' => 10,
        'total_periods' => 2,
        'period_time' => 20,
        'team_makeup' => array (
            'G' => array ('limit' => 4),
            'F' => array ('limit' => 5),
            'UTIL' => array(
                'limit'=>1,
                'wrapper'=>true,
                'sub_positions'=>array('G','F')
            ),
        ),
    ),
    'CFB' =>array (
        'default_salcap'=>50000,
        'earlygamecutoff' => 1600,
        'mingames' => 2,
        'day_limit' => 7,
        'dow_limit' => 2,
        'gametimes' => array (
            'WEEK' => array (
                'days'  => array (
                    0 => 4,
                    1 => 5,
                    2 => 6,
                    3 => 0,
                    4 => 1,
                ),
                'start' => false,
                'end'   => 2359,
            ),
            'ALL' => array (
                'days'  => 1,
                'start' => false,
                'end'   => 2359,
            ),
            'AM' => array(
                'days'  => 1,
                'start' => 1100,
                'end'   => 1759,
            ),
            'EVE' =>  array (
                'days'  => 1,
                'start' => 1800,
                'end'   => 2359,
            ),
            'PRIME' =>  array (
                'days'  => 1,
                'start' => 1530,
                'end'   => 1930,
            ),
        ),
        'total_players' => 10,
        'total_periods' => 4,
        'period_time' => 15,
        'team_makeup' => array (
			'QB'    => array ('limit' => 1),
			'RB'    => array ('limit' => 3),
			'WR'    => array ('limit' => 2),
			'TE'    => array ('limit' => 1),
			'FLEX'  => array(
				'limit'         => 2,
				'wrapper'       => true,
				'sub_positions' => array('RB','WR','TE')
			),
			'UTIL'  => array(
				'limit'         => 1,
				'wrapper'       => true,
				'sub_positions' => array('QB','RB','WR','TE')
			),
		),
    ),
    'MLB' => array (
        'default_salcap' => 50000,
        'mingames' => 2,
        'day_limit' => 2,
        'gametimes' => array (
            'ALL' => array (
                'days'  => 1,
                'start' => false,
                'end'   => 2359,
            ),
            'AM' => array(
                'days'  => 1,
                'start' => false,
                'end'   => 1859,
            ),
            'EVE' => array (
                'days'  => 1,
                'start' => 1900,
                'end'   => 2359,
            ),
            'PM' => array (
                'days'  => 1,
                'start' => 2030,
                'end'   => 2359,
            ),
            'PRIME' => array (
                'days'  => 1,
                'start' => 1900,
                'end'   => 2030,
            ),
            'AFTERNOON' => array (
                'days'  => 1,
                'start' => 1500,
                'end'   => 2400,
            ),
        ),
        'total_players' => 9,
        'total_periods' => 9,
        'period_time' => 1,
        'team_makeup' => array (
            '1B'    => array ('limit' => 1),
            '2B'    => array ('limit' => 1),
            'SS'    => array ('limit' => 1),
            '3B'    => array ('limit' => 1),
            'C'     => array ('limit' => 1),
            'OF'    => array ('limit' => 3),
            'P'     => array ('limit' => 1),
        ),
        'positions' => array ('1B', '2B', 'SS', '3B', 'C', 'OF', 'P'),
        'pos_breakdown' => array(
            '1B'    => '1B',
            '2B'    => '2B',
            '3B'    => '3B',
            'SS'    => 'SS',
            'C'     => 'C',
            'CF'    => 'OF',
            'LF'    => 'OF',
            'RF'    => 'OF',
            'RP'    => 'P',
            'SP'    => 'P',
            'P'     => 'P'
        ),
    ),
    'NHL' => array (
        'default_salcap'=>55000,
        'mingames' => 2,
        'day_limit' => 2,
        'gametimes' => array (
            'ALL' => array (
                'days'  => 1,
                'start' => false,
                'end'   => 2359,
            ),
            'AM' => array(
                'days'  => 1,
                'start' => false,
                'end'   => 1859,
            ),
            'EVE' => array (
                'days'  => 1,
                'start' => 1700,
                'end'   => 2359,
            ),
            'PRIME' => array (
                'days'  => 1,
                'start' => 1900,
                'end'   => 2100,
            ),
        ),
        'total_players' => 10,
        'total_periods' => 3,
        'period_time' => 20,
        'team_makeup' => array (
            'LW' => array ('limit' => 2),
            'RW' => array ('limit' => 2),
            'C' => array ('limit' => 2),
            'D' => array ('limit' => 2),
            'G' => array ('limit' => 1),
        ),
    ),
    'GOLF' => array (
        'default_salcap'=>50000,
        'mingames' => 1,
        'day_limit' => 3,
        'gametimes' => array (
            'ALL' => array (
                'days'  => 1,
                'start' => false,
                'end'   => 2359,
            ),
        ),
        'total_players' => 6,
        'total_periods' => 4,
        'period_time' => 18,
        'team_makeup' => array (
          'GO' => array ('limit' => 6),
        ),
    ),
);

$config['games_generator']='a:1:{s:10:"settingone";s:10:"some value";}';



/******************************
* Scoring Vars for each sport
******************************/
$config['scoring']=array (
    'nfl' => array (
        'box_score' => array (
            'player_stats'  => array (
                'rushing_yards'             => 0.1,
                'rushing_tds'               => 6,
                'passing_yards'             => 0.04,
                'passing_tds'               => 4,
                'passing_interceptions'     => -1,
                'receiving_yards'           => 0.1,
                'receiving_tds'             => 6,
                'receiving_receptions'      => 1,
                'kick_returning_tds'        => 6,
                'punt_returning_tds'        => 6,
                'fumbles_lost_number'       => -2,
                'own_fumbles_recovered_tds' => 6,
                'two_points_scores'         => 2,
                'two_points_passes'         => 2,
                'PAT_made'                  => 1,
            ),
            'team_stats' => array (
                'defense_sacks' => 1,
                'opponent_fumbles_recovered'    => 2,
                'punt_returns_tds'              => 6,
                'kick_returns_tds'              => 6,
                'interception_returns_tds'      => 6,
                'opponent_fumbles_tds'          => 6,
                'safeties_safeties'             => 2,
                'field_goals_blocked'           => 2,
                'interception_returns_attempts' => 2,
                'points_allowed' => array (
                    0 => array ('min' => 0,  'max' => 0,     'score' => 10,),
                    1 => array ('min' => 1,  'max' => 6,     'score' => 7,),
                    2 => array ('min' => 7,  'max' => 13,    'score' => 4,),
                    3 => array ('min' => 14, 'max' => 20,    'score' => 1,),
                    4 => array ('min' => 28, 'max' => 34,    'score' => -1,),
                    5 => array ('min' => 35, 'max' => false, 'score' => -4,),
                ),
            ),
        ),
    ),
    'nba' => array (
        'box_score' => array (
            'player_stats' => array (
                'three_point_field_goals_made'  => 3.5,
                'field_goals_made'              => 2,
                'free_throws_made'              => 1,
                'rebounds_total'                => 1.25,
                'assists'                       => 1.5,
                'blocked_shots'                 => 2,
                'steals'                        => 2,
                'turnovers'                     => -1,
              ),
            'team_stats' => array (
            ),
        ),
    ),
    'cbk' => array (
        'box_score' => array (
            'player_stats' => array (
                'points'            => 1,
                'rebounds_total'    => 1.25,
                'assists'           => 1.5,
                'blocked_shots'     => 2,
                'steals'            => 2,
                'turnovers'         => -1,
            ),
            'team_stats' => array (
            ),
            'summary' => array (
            ),
        ),
    ),
    'cfb' => array (
        'box_score' => array (
            'player_stats' => array (
                'rushing_yards'             => 0.1,
                'rushing_tds'               => 6,
                'passing_yards'             => 0.04,
                'passing_tds'               => 4,
                'passing_interceptions'     => -1,
                'punt_returning_tds'        => 6,
                'kick_returning_tds'        => 6,
                'receiving_yards'           => 0.1,
                'receiving_tds'             => 6,
                'receiving_receptions'      => 1,
                'fumbles_lost_number'       => -2,
                'own_fumbles_recovered_tds' => 6,
                'two_points_scores'         => 2,
                'two_points_passes'         => 2,
            ),
        ),
    ),
    'mlb' => array (
        'box_score' => array (
            'player_stats' => array (
                'P' => array(
                    "winning_pitcher"               => 4,
                    "earned_runs"                   => -2,
                    "strike_outs"                   => 1,
                    "innings_pitched_innings"       => 1,
                    "walks"                         => -0.25,
                    "hit_batsmen_number"            => -0.25,
                    "entered_game_complete_game"    => 1
                ),
                'H' => array(
                    "singles"               => 1,
                    "doubles"               => 2,
                    "triples"               => 3,
                    "home_runs"             => 4,
                    "runs_batted_in"        => 2,
                    "runs"                  => 1,
                    "walks"                 => 1,
                    "stolen_bases"          => 2,
                    "hit_by_pitch_number"   => 1,
                    "strike_outs"           => -0.25
                ),
            ),
        ),
    ),
    'nhl' => array (
        'box_score' => array (
            'goaltenders' => array (
                'saves'                 => 0.25,
                'goals_against_goals'   => -1,
                'shutout'               => 2,
                'decision'              => 2,
                'goals'                 => 4,
                'assists'               => 2,
            ),
            'skaters' => array (
                'goals'                     => 4,
                'assists'                   => 2,
                'shots_on_goal_shots'       => 0.5,
                'penalty_minutes_minutes'   => 0.25,
                'short_handed_goals_goals'  => 2,
                'blocks'                    => 1,
            ),
        ),
    ),
    'golf'=>array(
	  	"par-3"         => 10,
		"par-2"         => 5,
		"par-1"         => 3,
		"par0"          => 1,
		"par1"          => -1,
		"par2"          => -2,
		"par3+"         => -4,
		"place1"        => 40,
		"place2"        => 35,
		"place3"        => 30,
		"place4"        => 26,
		"place5"        => 22,
		"place6"        => 20,
		"place7"        => 19,
		"place8"        => 18,
		"place9"        => 17,
		"place10"       => 16,
		"place11-15"    => 10,
		"place16-20"    => 8,
		"place21-25"    => 6,
		"place26-30"    => 5,
		"place31-35"    => 3,
		"place36-40"    => 1,
		"bogey_free"    => 2,
		"holes_in_one"  => 5
	),
);


$config['position_filter']=array (
    'nfl' => array (),
    'nba' => array (
        'Center'            => 'C',
        'Forward'           => 'SF/PF',
        'Forward-Center'    => 'PF/C',
        'Guard'             => 'PG/SG',
        'Guard-Forward'     => 'SG/SF',
        'Point Guard'       => 'PG',
        'Power Forward'     => 'PF',
        'Shooting Guard'    => 'SG',
        'Small Forward'     => 'SF',
    ),
    'cbk' => array (
        'C'     => 'F',
        'F'     => 'F',
        'F-C'   => 'F',
        'G'     => 'G',
        'G-F'   => 'G/F',
    ),
    'cfb' => array (),
    'mlb' => array (),
    'nhl' => array (
        'Center'        => 'C',
        'Defense'       => 'D',
        'Left Wing'     => 'LW',
        'Right Wing'    => 'RW',
        'Goaltender'    => 'G',
    ),
    'golf'=>array()
);

/***********************************
* Site Specific Config Vars
***********************************/
