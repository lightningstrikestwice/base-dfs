<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['data_provider'] = array(
    'fantasy_data' => array(
        "apiKeys" => array(
            "nfl" => "d984dbb67a7044beb1e596ad67e46406",
            "mlb" => "3c57f939ecb04ec78cb2bf6e173d9fde",
            "nba" => "4d32605e4ed04ffb83748d2b14a0635a",
            "nhl" => "877bc0d4854e4c819bf3260cd8f2f7ba",
            "cbk" => "235eb9a35aaf4605b732f64b639b34ee",
            "cfb" => "4b4ffc07182b442c9162679bdb55669d",
            "golf" => "d5423c0122e241489a47e1b0971ef6d3",
        ),
    ),
    'sport_radar' => array(

    ),
    'rotowire' => array(

    ),
    'goal_serve' => array(

    ),
);