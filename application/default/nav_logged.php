	<div class="top-nav">
		<div class="left d-flex flex-row">
			<ul class="nav justify-content-start">
				<li class="nav-item">
					<a href="<?=base_url('my_account/deposit_funds') ?>" class="nav-link"><i class="fas fa-wallet"></i> Deposit Funds</a>
				</li>
				<li class="nav-item">
					<a href="<?=base_url('my_account/invite_friends') ?>" class="nav-link"><i class="fas fa-users"></i> Invite Friends</a>
				</li>
        <?  if ($this->session->user->is_admin == 1 || $this->session->user->super == 1):   ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('admin') ?>"><i class="fas fa-user-shield"></i> Admin</a>
                </li>
        <?  endif;  ?>
			</ul>
		</div>

    <?
        $userID = $this->Users->id();

        $userBalance = $this->User_funds->user_balance($userID)['balance'];

        $image = $this->Misc->get_avatar_url($userID);
    ?>
		<div class="profile-info">
			<img src="<?=$image?>" alt="" />
			<a href="#"><?=$this->session->user->username?></a>&nbsp;|&nbsp;<?=$siteOptions['currency'].$userBalance?>
		</div>
	</div>

	<nav class="navbar navbar-expand-md overlay-nav">
        <a href="<?=base_url() ?>" class="navbar-brand"><img alt="Logo" src="<?=site_url('assets/'.THEME.'/img/'.$siteOptions['logo']) ?>" /></a>
		<div class="navbar-wrap">
			<ul class="nav justify-content-end">
				<li class="nav-item">
					<a href="<?= base_url('lobby') ?>" class="nav-link" style="color: #1f375b; font-weight: 500;">Lobby</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">My Contests</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<?= base_url('match/upcoming') ?>">Upcoming Contests</a>
						<a class="dropdown-item" href="<?= base_url('match/live') ?>">Live Contests</a>
						<a class="dropdown-item" href="<?= base_url('match/history') ?>">Contest History</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">My Account</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<?= base_url('my_account/manage') ?>">Manage Account</a>
						<a class="dropdown-item" href="<?= base_url('my_account/add_funds') ?>">Deposit Funds</a>
						<a class="dropdown-item" href="<?= base_url('my_account/withdrawal_request') ?>">Withdrawal Request</a>
						<a class="dropdown-item" href="<?= base_url('my_account/transactions') ?>">Transaction History</a>
                        <a class="dropdown-item" href="<?= base_url('logout') ?>">Logout</a>
					</div>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('support') ?>" class="nav-link">Support</a>
				</li>
			</ul>
		</div>

		<button class="toggle-menu menu-left ml-auto" role="button" type="button"><i class="far fa-bars"></i></button>

	</nav>

	<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">

		<a class="navbar-brand" href="index.php">
			<img src="https://via.placeholder.com/180x120.png?text=Site+Logo" alt="<?=$siteOptions['website_name'] ?>"" />
		</a>

		<ul>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('lobby') ?>">Lobby</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('match/upcoming') ?>">Upcoming Contests</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('match/live') ?>">Live Contests</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('match/history') ?>">Contest History</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('my_account/manage') ?>">Manage Account</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('my_account/add_funds') ?>">Deposit Funds</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('my_account/withdrawal_request') ?>">Withdrawal Request</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('my_account/transactions') ?>">Transaction History</a>
			</li>
			<li class="nav-item">
				<a href="<?= base_url('invite_friends') ?>" class="nav-link">Invite Friends</a>
			</li>
			<li class="nav-item">
				<a href="<?= base_url('support') ?>" class="nav-link">Support</a>
			</li>
			<li class="nav-item">
				<a href="<?= base_url('logout') ?>" class="nav-link">Logout</a>
			</li>
		</ul>
	</nav>
