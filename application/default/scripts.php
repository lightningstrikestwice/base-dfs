        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="<?=site_url('assets/'.GLOBAL_CONTENT.'/js/jquery-ui.min.js') ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="<?=site_url('assets/'.GLOBAL_CONTENT.'/js/slick/slick.min.js') ?>"></script>
        <script type="text/javascript" src="<?=site_url('assets/'.GLOBAL_CONTENT.'/font-awesome/js/all.min.js') ?>"></script>
        <script type="text/javascript" src="<?=site_url('assets/'.GLOBAL_CONTENT.'/js/jPushMenu.js') ?>"></script>

        <script type="text/javascript" src="<?=site_url('assets/'.THEME.'/js/general.js');?>"></script>

    <?  if(isset($scripts) && !empty($scripts)):
            foreach ($scripts as $script):  ?>
                <script src="<?=$script?>"></script>
    <?      endforeach;
        endif;  ?>

        <script type="text/javascript">
            jQuery(document).ready(function($){
                localStorage.clear('/assets/<?=THEME;?>/');
            });
            var HOST_NAME="<?=site_url('/');?>";
        </script>


