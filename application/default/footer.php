        <div class="subfooter">
<?  $siteOptions = $this->Options->get_group(1);    ?>
            <div class="social-media">
            <?  $socialMedia = $this->Options->get_group(2); ?>
                <ul>
                    <li><a href="<?=$socialMedia['facebook'] ?>" target="_blank"><i class="fab fa-facebook"></i></a></li>
                    <li><a href="<?=$socialMedia['twitter'] ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="<?=$socialMedia['instagram'] ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>
            <div class="logos">
                <img src="<?=base_url() ?>/assets/<?=THEME?>/img/ssl.png" alt="Comodo SSL Secure" />
            </div>
        </div>

        <footer class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="footer-nav">
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="<?=base_url('page/view/about')?>" class="nav-link">About Us</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('faq')?>" class="nav-link">FAQs</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('page/view/how_it_works')?>" class="nav-link">How It Works</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('page/view/rules_scoring')?>" class="nav-link">Rules &amp; Scoring</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('page/view/terms')?>" class="nav-link">Terms</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('page/view/privacy')?>" class="nav-link">Privacy</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?=base_url('page/view/legal')?>" class="nav-link">Legal Notice</a>
                            </li>
                        </ul>
                    </div>

                    <span class="copyright">
                        &copy; <?php echo date('Y'); ?> <a href="index.php"><?=$siteOptions['website_name'] ?></a> | <a title="web design company" href="http://www.dreamcodesign.com">Web Design Company</a> - <a title="dreamco design" href="https://www.dreamcodesign.com">DreamCo Design</a>
                    </span>
                </div>
                <div class="col">
    				<div class="credit-cards">
                        <img src="<?=site_url() ?>/assets/<?=THEME?>/img/amex.png" alt="American Express" />
                        <img src="<?=site_url() ?>/assets/<?=THEME?>/img/disc.png" alt="Discover" />
                        <img src="<?=site_url() ?>/assets/<?=THEME?>/img/mc.png" alt="Mastercard" />
                        <img src="<?=site_url() ?>/assets/<?=THEME?>/img/visa.png" alt="Visa" />
                    </div>
                </div>
            </div>
        </footer>

    </body>
</html>