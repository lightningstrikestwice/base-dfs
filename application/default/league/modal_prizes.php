<div class="modal-content">
    <div class="modal-header">
        <h2 class="modal_title text-center"><?= $league->name ?> Prize</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <table class="table prizes">
            <tbody>
                <tr class="prize">
                    <td><i class="fal fa-trophy"></i> Winner</td>
                    <td><?= $siteOptions['currency'].number_format($league->total_prize, 2) ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
</div>
