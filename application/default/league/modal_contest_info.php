<?
$buyin = $league->entry_fee == 0 ? "FREE" : $siteOptions['currency'].number_format($league->entry_fee, 2);
$prize = $siteOptions['currency'].number_format($league->entry_fee, 2);

$start = $league->first_game_cutoff;
$y = date('Y',$start);
$m = date('m',$start);
$m = $m - 1;
$d = date('d',$start);
$h = date('H',$start);
$i = date('i',$start);
$s = date('s',$start);

?>

<div class="modal-content">
    <div class="modal-header">
        <h2 class="modal_title"><?= $league->name ?></h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <ul class="league-options">
            <li>Buy-In: <?= $buyin ?></li>
            <li>Size: <?= $league->entries ?>/<?= $league->size ?></li>
            <li>Prize: <?= $prize ?></li>
        <?  if($league->first_game_cutoff - time() < 24 * 60 * 60):
                if($league->retain_lobby==1 && $league->first_game_cutoff <= time()):   ?>
                    <li><div class="liveclock font-weight-bold">This Contest is Live</div></li>
            <?  elseif($league->retain_lobby==1 && $league->entries>=$league->size):    ?>
                    <li><div class="liveclock font-weight-bold">This Contest is Full</div></li>
            <?  else:   ?>
                    <li>Begins in: <span id="modal_countdown" class="clock" data-cutoff-y="<?= $y ?>" data-cutoff-m="<?= $m ?>" data-cutoff-d="<?= $d ?>" data-cutoff-h="<?= $h ?>" data-cutoff-i="<?= $i ?>" data-cutoff-s="<?= $s ?>"></span></li>
            <?  endif;
            else:   ?>
                <li>Begins:
            <?  if($league->first_game_cutoff>=$tomorrow && $league->first_game_cutoff < $tomorrow+(86400*6)){
                    echo date('D g:ia', $league->first_game_cutoff);
                } elseif($league->first_game_cutoff<$today || $league->first_game_cutoff > $tomorrow+(86400*6)){
                    echo date('j/n g:ia', $league->first_game_cutoff);
                } else {
                    echo date('g:ia', $league->first_game_cutoff);
                }   ?>
                </li>
        <?  endif;  ?>
        </ul>
    </div>
    <div class="modal-footer">
        <a href="<?=base_url('league/join/'.$league->cid.'/'.$league->type) ?>" class="btn btn-default">Join</a>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
</div>
