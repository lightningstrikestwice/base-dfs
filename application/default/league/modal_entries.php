<div class="modal-content">
    <div class="modal-header">
        <h2 class="modal_title text-center"><i class="fas fa-users"></i> Entries</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <table class="table entries table-bordered">
            <tbody>
        <?  if ($entries):
                foreach ($entries as $entry):?>
                    <tr>
                        <td><img src="<?= $this->Misc->get_avatar_url($entry->cid) ?>" alt="<?= $entry->username ?>" class="rounded-circle" /> <?= $entry->username ?></td>
                    </tr>
        <?      endforeach;
            endif;  ?>
            </tbody>
        </table>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
</div>
