<?
// set the link open
$url = $this->uri->uri_string;


$activeMenu = " active";
$selManage = $selProfile = $selUpload = $selDeposit = $selWithdrawal = $selTransactions = "";

// Manage Account
if (strpos($url,'my_account/manage') !== false) {$selManage = $activeMenu;}
// Profile Settings
if (strpos($url,'my_account/profile_settings') !== false) {$selProfile = $activeMenu;}
// Upload Photo
if (strpos($url,'my_account/edit_photo') !== false) {$selUpload = $activeMenu;}
// Deposit Funds
if (strpos($url,'my_account/deposit_funds') !== false) {$selDeposit = $activeMenu;}
// Withdrawal Request
if (strpos($url,'my_account/withdrawal_request') !== false) {$selWithdrawal = $activeMenu;}
// Transaction History
if (strpos($url,'my_account/transactions') !== false) {$selTransactions = $activeMenu;}

?>

<ul class="nav flex-column nav-pills">
	<li class="nav-item">
		<a class="nav-link<?=$selManage?>" href="<?=base_url('my_account/manage') ?>">Manage Account</a>
	</li>
	<li class="nav-item">
		<a class="nav-link<?=$selProfile?>" href="<?=base_url('my_account/profile_settings') ?>">Profile Settings</a>
	</li>
	<li class="nav-item">
		<a class="nav-link<?=$selUpload?>" href="<?=base_url('my_account/edit_photo') ?>">Edit Profile Photo</a>
	</li>
	<li class="nav-item">
		<a class="nav-link<?=$selDeposit?>" href="<?=base_url('my_account/deposit_funds') ?>">Deposit Funds</a>
	</li>
	<li class="nav-item">
		<a class="nav-link<?=$selWithdrawal?>" href="<?=base_url('my_account/withdrawal_request') ?>">Withdrawal Request</a>
	</li>
	<li class="nav-item">
		<a class="nav-link<?=$selTransactions?>" href="<?=base_url('my_account/transactions') ?>">Transaction History</a>
	</li>
</ul>

