<section class="content-wrap">

	<div class="container">

		<h1 class="title">Register for <?= $siteOptions['website_name'] ?></h1>

		<div class="row">

			<div class="col-md-8 registration">

				<p>Register for free by creating your profile below.</p>

				<form action="" method="post">
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="email">Email</label>
							<input type="email" class="form-control" name="email" id="email" value="<?=$passon['email']?>">
                            <?= form_error('email') ?>
						</div>
						<div class="form-group col-md-6">
							<label for="username">Username</label>
							<input type="text" class="form-control" name="username" id="username" value="<?=$passon['username']?>">
                            <?= form_error('username') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="password">Password</label>
							<input type="password" class="form-control" name="password" id="password" value="<?=$passon['password']?>">
                            <?= form_error('password') ?>
						</div>
						<div class="form-group col-md-6">
							<label for="confirmPassword">Confirm Password</label>
							<input type="password" class="form-control" name="password_confirm" id="confirmPassword" value="<?=$passon['password_confirm']?>">
                            <?= form_error('password_confirm') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="firstName">First Name</label>
							<input type="text" class="form-control" name="first_name" id="firstName" value="<?=$passon['first_name']?>">
                            <?= form_error('first_name') ?>
						</div>
						<div class="form-group col-md-6">
							<label for="lastName">Last Name</label>
							<input type="text" class="form-control" name="last_name" id="lastName" value="<?=$passon['last_name']?>">
                            <?= form_error('last_name') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="phone">Phone Number</label>
							<input type="text" class="form-control" name="phone" id="phone" aria-describedby="phoneHelp" value="<?=$passon['phone']?>">
							<small id="phoneHelp" class="form-text text-muted">100% Confidential. Necessary to verify cash withdrawals.</small>
                            <?= form_error('phone') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="address">Address</label>
							<input type="text" class="form-control" name="address" id="address" value="<?=$passon['address']?>">
							<small id="provinceHelp" class="form-text text-muted">Contest participation is 100% restricted to residents of Canada at this time.</small>
                            <?= form_error('address') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="city">City</label>
							<input type="text" class="form-control" name="city" id="city" value="<?=$passon['city']?>">
                            <?= form_error('city') ?>
						</div>
						<div class="form-group col-md-6">
							<label for="province">Province</label>
							<select class="form-control" id="province" name="state" aria-describedby="provinceHelp">
								<option value="">Select One</option>
                        <?  foreach ($provinces as $abbr=>$name):
                                $sel = $abbr == $passon['state'] ? " selected" : "";   ?>
                                <option value="<?=$abbr?>"<?=$sel?>><?=$name ?></option>
                        <?  endforeach; ?>
							</select>
                            <?= form_error('state') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="postalCode">Postal Code</label>
							<input type="text" class="form-control" name="zip" id="postalCode" value="<?=$passon['zip']?>">
                            <?= form_error('zip') ?>
						</div>
						<div class="form-group col-md-6">
							<label for="country">Country</label>
							<select class="form-control" name="country" id="country">
								<option value="CA">Canada</option>
							</select>
                            <?= form_error('country') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12">
							<label for="month">Date of Birth</label>
						</div>
						<div class="form-group col-md-4">
                            <select class="form-control" name="month" id="month">
                        <?  foreach ($months as $k=>$mon):
                                $sel = $k == $passon['month'] ? " selected" : "";   ?>
                                <option value="<?=$k?>"<?=$sel?>><?=$mon?></option>
                        <?  endforeach; ?>
							</select>
                            <?= form_error('month') ?>
						</div>
						<div class="form-group col-md-4">
							<select class="form-control col" name="date" id="day">
						<?  for($d=1; $d<=31; $d++):
                                $sel = $d == $passon['date'] ? " selected" : "";   ?>
								<option<?=$sel?>><?=$d;?></option>
                        <?  endfor; ?>
							</select>
                            <?= form_error('date') ?>
						</div>
						<div class="form-group col-md-4">
							<?php
							$year = date('Y');
							$me = strtotime($year.' -18 years');
							$y = date('Y',$me);
							$old = strtotime($year.' -100 years');
							$o = date('Y',$old);    ?>
                            <select class="form-control col" name="year" id="year">
						<?  for($yr=$y; $yr>=$o; $yr--):
                                $sel = $yr == $passon['year'] ? " selected" : "";?>
								<option<?=$sel?>><?= $yr?></option>
						<?  endfor; ?>
							</select>
                            <?= form_error('year') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="question">Where did you hear about <?= $siteOptions['website_name'] ?>?</label>
							<select class="form-control" name="usersource" id="question">
                        <?  foreach ($sources as $key=>$source):
                                $sel = $key == $passon['usersource'] ? " selected" : "";   ?>
                                <option value="<?=$key?>"<?=$sel?>><?=$source?></option>
                        <?  endforeach; ?>
							</select>
                            <?= form_error('usersource') ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12">
							<div class="form-check">
                            <?  $sel = isset($passon['tos']) ? " checked" : ""; ?>
								<input class="form-check-input" type="checkbox" value="1" name="tos" id="terms"<?=$sel?>>
								<label class="form-check-label" for="terms">
									I agree to the <?= $siteOptions['website_name'] ?> <a href="#" data-toggle="modal" data-target="#termsConditions">Terms & Conditions</a>.
								</label>
							</div>
                            <?= form_error('tos') ?>
						</div>
					</div>
					<div class="text-center">
						<button type="submit" class="btn-primary btn">Continue</button>
					</div>
				</form>

			</div>

			<div class="d-none d-sm-block col-md-4 register-sidebar">

				<p class="mb-1">Like survival style fantasy sports contests? You’re going to love EliminatorPools! In just a few moments, you’ll be able to join our exciting contests.</p>

				<img class="mb-1" src="<?=site_url('assets/'.THEME.'/img/motivation.svg') ?>">

				<ul>
                    <li>100% Free Account Setup</li>
                    <li>Contests for NFL, NBA, MLB, NHL, GOLF, & CBK</li>
                    <li>Multiple Survival Gameplay Styles</li>
                    <li>Safe, Secure, and Fully Legal Gameplay</li>
				</ul>

			</div>

		</div>

	</div>

</section>

<div id="termsConditions" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				TOS CONTENT
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
