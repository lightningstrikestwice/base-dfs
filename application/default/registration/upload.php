<section class="content-wrap">

	<div class="container">

		<h1 class="title">Upload Your Avatar</h1>

		<div class="row">

            <div class="col-sm-6 full-fix image-upload">
                <div class="clearfix">
                    <div id="avatar_upload_wrap">
                        <div id="avatar_image_zone" class="clearfix">
                            <div id="avatar_drag_drop">
                                Drop Image File Here<br />
                                <em>Or</em><br />
                                <button id="browse_files" class="btn btn-primary btn-large">Browse Files</button>
                            </div>
                        </div>
                        <div id="media-items"></div>
                    </div>
                </div>
                <form method="post" action="<?=site_url('register/crop_avatar');?>" id="cropform">
                <?= form_open('register/crop_avatar',array('id'=>'cropform'))   ?>
                    <input type="hidden" id="temp_image" name="temp_image" />
                    <input type="hidden" id="edit" name="edit" value="1"/>
                    <input type="hidden" id="x" name="x" />
                    <input type="hidden" id="y" name="y" />
                    <input type="hidden" id="w" name="w" />
                    <input type="hidden" id="h" name="h" />
                    <div class="form-actions mt-3">
                        <input type='submit' id="avatar_save" name='submit' class="btn btn-default submit disabled" disabled='disabled' value='Save' />
                        <a href="<?=base_url()?>lobby" class="btn btn-primary float-right">Skip this step</a>
                    </div>
                <?= form_close() ?>

            </div>
            <div class="col-sm-6">
                <div id="avatar_preview_wrapper">
                    <h3>Preview</h3>
                    <p>Drop an image in the area to the right (above if on mobile) and crop it to set your primary picture. File must be .jpg or .png and must be smaller than 2mb.</p>
                    <div id="avatar_preview" style="width:200px;height:200px;">
                    </div>
                </div>
            </div>

        </div>

	</div>

</section>
