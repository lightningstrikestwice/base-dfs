<section class="content-wrap">

	<div class="container">

		<h1 class="title">Invite Friends</h1>
        <? $this->load->view(THEME.'/alerts'); ?>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

        <?= form_open() ?>
			<h4>Email Addresses</h4>
			<div class="form-row">
				<div class="form-group col-md-6">
					<input type="email" name="email[]" class="form-control" placeholder="Email">
				</div>
				<div class="form-group col-md-6">
					<input type="email" name="email[]" class="form-control" placeholder="Email">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<input type="email" name="email[]" class="form-control" placeholder="Email">
				</div>
				<div class="form-group col-md-6">
					<input type="email" name="email[]" class="form-control" placeholder="Email">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<input type="email" name="email[]" class="form-control" placeholder="Email">
				</div>
				<div class="form-group col-md-6">
					<input type="email" name="email[]" class="form-control" placeholder="Email">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<input type="email" name="email[]" class="form-control" placeholder="Email">
				</div>
				<div class="form-group col-md-6">
					<input type="email" name="email[]" class="form-control" placeholder="Email">
				</div>
			</div>
			<div class="text-center mt-4">
				<button type="submit" class="btn btn-primary">Invite Friends</button>
			</div>
        <?= form_close() ?>

	</div>

</section>
