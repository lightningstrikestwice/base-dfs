<div class="main-overlay" style="background-image: url(<?=site_url('assets/'.THEME.'/img/field_red.jpg') ?>); background-size: cover; background-position: center center;">

	<div class="main-background" style="background-image: url(<?=site_url('assets/'.THEME.'/img/field_red.jpg') ?>); background-size: cover; background-position: center center;">

		<div class="content animated fadeIn">

			<img class="logo" src="<?=site_url('assets/'.THEME.'/img/logo.png') ?>">

			<h1>Play NFL, NBA, MLB, GOLF, &amp; CBK Survival games for cash.</h1>
			<h1>"SURVIVE AND ADVANCE"</h1>

			<br><br>

			<a class="btn-primary btn" href="<?= site_url('register') ?>">Sign Me Up</a>

			<div class="link-group">

				<a class="text-link" href="<?= site_url('login') ?>"><i class="fas fa-sign-in"></i> Login</a>

				<a class="text-link" href="how-it-works.php"><i class="fas fa-question-circle"></i> How It Works</a>

			</div>

		</div>

	</div>

</div>

<section class="content-wrap landing-info">

	<div class="container">

		<div class="row align-items-center">

			<div class="col-md-6">

				<img src="<?=site_url('assets/'.THEME.'/img/devices2.png') ?>" alt="" />

			</div>

			<div class="col-md-6 landing-content text-center">

				<h3>IT'S EASY TO GET STARTED</h3>

				<img src="<?=site_url('assets/'.THEME.'/img/motivation.svg') ?>">

				<p>Join for Free. Create an account <br> in just a few minutes.</p>

				<img src="<?=site_url('assets/'.THEME.'/img/strat.svg') ?>">

				<p>Choose a Sport, <br> select an open contest, and make your picks.</p>

				<img src="<?=site_url('assets/'.THEME.'/img/cash.svg') ?>">

				<p>Win Cash Prizes as the action <br> unfolds daily and weekly.</p>

			</div>

		</div>

	</div>

</section>

<section class="content-wrap gray-bg landing-info">

	<div class="container">

		<div class="row align-items-center">

			<div class="col-md-12">

				<h3 class="text-left">EliminatorPools – We Make Survival Style Fantasy Games Fun and Easy</h3>

				<h4 class="text-left">EliminatorPools works great on computers, tablets, and all smartphones.</h4>

				<p class="text-left">EliminatorPools allows you to play in NFL, NBA, MLB, NHL, GOLF, and NCAA college basketball fantasy sports contests.</p>

				<p class="text-left">Some of our contests are straight up traditional survivor-style where you pick a team and move on each day or week where you can’t pick the same team again. Other contests added additional layers to the gameplay, like point spreads, over/under options, or in some cases, even the picking of specific athletes. If you survive longer than anyone else, you win. It’s that easy. There’s no season-long commitment; simply sign up and play.</p>

				<a href="#" class="btn-primary btn">Sign Up Now</a>

			</div>

		</div>

	</div>

</section>
