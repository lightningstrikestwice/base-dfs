<?
                    $sports = json_decode($league->sport,true);
                    $sportDesc = "";
                    $c = 0;
                    foreach ($sports as $sport) {
                        $c++;
                        $sep = $c > 1 ? "<BR>" : "";
                        $sportDesc .= $sep.'<i class="'.$this->Misc->get_sport_icon($sport).'"></i> '.$sport;
                    }

                    $multi = $league->multi_entry_limit > 1 ? '<img class="pr-1 smallIcon" src="'.base_url('assets/'.THEME.'/img/multi.png') .'" alt="Multi Entry" />' : '';
                    $guaranteed = $league->guaranteed == 1 ? '<img class="pr-1 smallIcon" src="'.base_url('assets/'.THEME.'/img/guaranteed.png') .'" alt="Guaranteed" />' : '';
                    $buyin = $league->entry_fee == 0 ? "FREE" : $siteOptions['currency'].number_format($league->entry_fee, 2);
                ?>
                    <tr>
                        <td><?=$sportDesc ?></td>
						<td><span class="modal-link contestInfo" data-leagueid="<?=$league->cid?>"><?=$multi.$guaranteed.$league->name ?></span></td>
						<td class="text-center"><?= $durations[$league->duration] ?></td>
                        <td class="text-center"><?= $buyin ?></td>
						<td class="text-center"><span class="modal-link contestEntries" data-leagueid="<?=$league->cid?>"><?=$league->entries ?></span>/<?=$league->size ?></td>
						<td class="text-center"><span class="modal-link contestPrizes" data-leagueid="<?=$league->cid?>"><?= $siteOptions['currency'].number_format($league->total_prize, 2) ?></span></td>
						<td class="text-center">
                    <?  if($league->first_game_cutoff - time() < 24 * 60 * 60):
                            if($league->retain_lobby==1 && $league->first_game_cutoff <= time()):   ?>
                                <div class="liveclock font-weight-bold">Live</div>
                        <?  elseif($league->retain_lobby==1 && $league->entries>=$league->size):    ?>
                                <div class="liveclock font-weight-bold">Full</div>
                        <?  else:   ?>
                                <div class="clock font-weight-bold" title="<?=date('j/n g:ia', $league->first_game_cutoff)?>" data-league-id="<?=$league->cid;?>" data-cutoff-y="<?=date('Y', $league->first_game_cutoff);?>" data-cutoff-m="<?=(date('m', $league->first_game_cutoff) - 1);?>" data-cutoff-d="<?=date('d', $league->first_game_cutoff);?>" data-cutoff-h="<?=date('H', $league->first_game_cutoff);?>" data-cutoff-i="<?=date('i', $league->first_game_cutoff);?>" data-cutoff-s="<?=date('s', $league->first_game_cutoff);?>"></div>
                        <?  endif;
                        else:
                            if($league->first_game_cutoff>=$tomorrow && $league->first_game_cutoff < $tomorrow+(86400*6)){
                                echo date('D g:ia', $league->first_game_cutoff);
                            } elseif($league->first_game_cutoff<$today || $league->first_game_cutoff > $tomorrow+(86400*6)){
                                echo date('j/n g:ia', $league->first_game_cutoff);
                            } else {
                                echo date('g:ia', $league->first_game_cutoff);
                            }
                        endif;  ?>
                        </td>
						<td class="join">
                    <?  if($league->retain_lobby==1 && ($league->first_game_cutoff <= time() || $league->entries>=$league->size)):?>
                            <span class="row_actions" data-entered="1"><a href="<?=site_url('view/live/'.$league->cid);?>" class="btn btn-default btn-block">View</a></span>
                    <?  else:   ?>
                        <?  if($league->joined >= 1 || ($league->multi_entry == 1 && $league->joined == $league->multi_entry_limit)):?>
                                <span class="row_actions" data-entered="1"><a href="<?=site_url('view/live/'.$league->cid);?>" class="btn btn-black btn-block">View</a></span>
                        <?  else:   ?>
                                <span class="row_actions" data-entered="0"><a href="<?=base_url('league/join/'.$league->cid.'/'.$league->type) ?>" class="btn btn-default btn-block view join_view">Join <i class="fa fa-chevron-right"></i></a></span>
                        <?  endif;
                        endif   ?>
                        </td>
                    </tr>
