<section class="content-wrap lobby">

	<div class="container" style="max-width: 1300px;">

		<div class="row">

			<div class="col-md-12 lobby-games">
        <?  if ($components['use_slider'] == 1):    ?>
                <div class="featured-games">
            <?  foreach($slides as $slide):

                    $urlCheck = strpos($slide->image, "http");
                    $imageURL = $urlCheck > -1 ? $slide->image : base_url('uploads/slides/'.$slide->image);     ?>
                    <div class="px-2">
                        <a href="<?=$slide->link?>">
                            <img src="<?=$imageURL ?>" />
                        </a>
                    </div>
            <?  endforeach; ?>
                </div>
        <?  endif;  ?>
			</div>

		</div>

		<button class="btn btn-default filter-toggle">Filters <i class="fas fa-plus"></i></button>

		<div class="filters">
			<form method="post" action="">
				<div class="league-filter filter">
					<label>Sport</label>
            <?  foreach($sports as $sport=>$icon): ?>
					<div class="checkbox">
						<input id="sport_<?=$sport?>" type="checkbox" name="sports" value="<?=$sport?>">
                        <label for="sport_<?=$sport?>"><i class="<?=$icon?>"></i> <?=$sport?></label>
					</div>
            <?  endforeach; ?>
				</div>
				<div class="league-filter filter">
					<label>Type</label>
            <?  foreach($types as $k=>$type):   ?>
					<div class="checkbox">
                        <input id="type_<?=$k?>" type="checkbox" name="types" value="<?=$k?>">
                        <label for="type_<?=$k?>"><?=$type?></label>
					</div>
            <?  endforeach; ?>
				</div>
				<div class="entry-filter filter">
					<label>Entry</label>
					<div id="entry_fee"></div>
					<div class="little-label" id="entry_value"></div>
				</div>
				<div class="legend filter">
					<div>
						<img src="<?=base_url('assets/'.THEME.'/img/multi.png') ?>" alt="Multi Entry" /> = Multi Entry
					</div>
					<div>
						<img src="<?=base_url('assets/'.THEME.'/img/guaranteed.png') ?>" alt="Guaranteed" /> = Guaranteed
					</div>
				</div>
			</form>
		</div>

		<div class="table-responsive">
			<table class="table lobby-table tablesorter table-striped" id="lobby_table">
				<thead class="thead-light">
					<tr>
						<th>Sport</th>
						<th class="text-center">Game</th>
						<th class="text-center">Duration</th>
						<th class="text-center">Buy-In</th>
						<th class="text-center">Size</th>
						<th class="text-center">Prize</th>
						<th class="text-center">Begins</th>
						<th class="text-center"></th>
					</tr>
				</thead>
                <tbody id="league_rows">
                    <? // Content Loaded Via Ajax ?>
				</tbody>
			</table>

	</div>

</section>

<?  $this->load->view(THEME.'/league/modals.php')   ?>

<!-- CREATE MODALS file -->
<div id="contest_info" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<h2>NFL Standard Survivor</h2>

				<ul class="league-options">
					<li>Buy-In: $10.00</li>
					<li>Size: 78/100</li>
					<li>Prize: $500.00</li>
					<li>Begins In: 00:38:05</li>
				</ul>
			</div>
			<div class="modal-footer">
				<a href="select.php" class="btn btn-default">Join</a>
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- league size modal -->
<div id="league_size" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">

				<h2><i class="fas fa-users"></i> Entries</h2>

				<table class="table entries table-bordered">
					<tbody>
						<?php $c = 1; while($c <= 10):?>
						<tr>
							<td><img src="theme/img/default.jpeg" alt="Username" class="rounded-circle" /> Username</td>
							<td><img src="theme/img/default.jpeg" alt="Username" class="rounded-circle" /> Username</td>
						</tr>
						<?php $c++; endwhile;?>
					</tbody>
				</table>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- prize modal -->
<div id="prizes" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">

				<h2>NFL Survivor Game Prize</h2>

				<table class="table prizes">
					<tbody>
						<tr class="prize">
							<td><i class="fal fa-trophy"></i> Winner</td>
							<td>$500.00</td>
						</tr>
					</tbody>
				</table>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
    $(function(){

    });
</script>