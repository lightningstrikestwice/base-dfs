<?
$version=$this->config->item('ftd_version');
$siteOptions = $this->Options->get_group(1);

if (isset($title) && !empty($title)) {
    $pageTitle = $title . " - " . $siteOptions['website_name'];
} else {
    $pageTitle = $siteOptions['website_name'];
}

if (isset($seoDescription) && !empty($seoDescription)) {
    $seoDesc = $seoDescription;
} else {
    $seoDesc = $siteOptions['seo_desc'];
}

if (isset($seoKeywords) && !empty($seoKeywords)) {
    $seoKeywords = $seoKeywords;
} else {
    $seoKeywords = $siteOptions['seo_keywords'];
}
?>
<!doctype html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="-1">

        <title><?= $pageTitle ?></title>
        <meta name='description' content='<?= $seoDesc ?>' />
        <meta name='keywords' content='<?= $seoKeywords ?>' />

        <!--<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">-->
        <!--<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">-->
        <!--<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">-->

        <?
        $this->load->view(THEME.'/styles');
        $this->load->view(THEME.'/scripts');
        ?>
    </head>
    <body>
        <div id="magic_cover" class="modal-backdrop fade in" style="cursor: pointer; display: none;"></div>

    <?  if ($this->Users->is_loggedin()):
            $this->load->view(THEME.'/nav_logged');
        else:
            $this->load->view(THEME.'/nav_not_logged');
        endif;  ?>