$(document).ready(function(){
	//Set all of the clocks
	function serverTime() { 
		var time = null; 
		$.ajax({url: HOST_NAME+'servertime.php', 
			async: false, dataType: 'text', 
			success: function(text) { 
				time = new Date(text);
				//alert(time);
			}, error: function(http, message, exc) { 
				time = new Date();
		}}); 
		return time; 
	}	
	
	var dst = null; 
	console.log(HOST_NAME+'serveroffset.php');
	$.ajax({
		url: HOST_NAME+'serveroffset.php', 
		async: false, dataType: 'text', 
		success: function(text){ 
			dst = parseInt(text);
		},
		error: function(http, message, exc) { 
			dst = -4;
		}
	});
	
    var clocks = $('.clock');
    var i;
    for (i=0; i <= clocks.length-1; i++) {
        var thisdom = $(clocks[i]);
        thisdom.countdown('destroy');
        var end_date = new Date(thisdom.data('cutoff-y'), thisdom.data('cutoff-m'), thisdom.data('cutoff-d'), thisdom.data('cutoff-h'), thisdom.data('cutoff-i'), thisdom.data('cutoff-s'));
        thisdom.countdown({
            until: end_date,
            serverSync: serverTime,
            timezone: dst,
            format: 'HMS', 
            compact: true, 
            description: '',
            onExpiry: function() {
                window.setTimeout(function(){
                    window.location.reload();
                }, 15000);
            }
        });
    }

	// lobby featured games
	$('.featured-games').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		dots: true,
		arrows: false,
		responsive: [
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 767,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
		]
	});	

    $( "#entry_fee" ).slider({
        range: true,
        min: 0,
        max: 100,
        values: [ 0, 60 ],
        slide: function( event, ui ) {
            $( "#entry_value" ).val( ui.values[ 0 ] + ui.values[ 1 ] );
            var div = document.getElementById('entry_value');

            var minvalue = $( "#entry_fee" ).slider( "values", 0 );
            var maxvalue = $( "#entry_fee" ).slider( "values", 1 );

            if( maxvalue < 99 ){
                var newmax = $( "#entry_fee" ).slider( "values", 1 ) + "";
            }else{
                var newmax = "$100+";
            }

            if( minvalue < 1 ){
                var newmin = "Free";
            }else{
                var newmin = $( "#entry_fee" ).slider( "values", 0 );
            }

            div.innerHTML = newmin + " - " + newmax;
        },
        stop: function(event, ui){
            $( "#entry_value" ).val( ui.values[ 0 ] + ui.values[ 1 ] );
            var div = document.getElementById('entry_value');

            var minvalue = $( "#entry_fee" ).slider( "values", 0 );
            var maxvalue = $( "#entry_fee" ).slider( "values", 1 );

            if( maxvalue < 99 ){
                var newmax = "$" + $( "#entry_fee" ).slider( "values", 1 );
            }else{
                var newmax = "$100+";
            }

            if( minvalue < 1 ){
                var newmin = "Free";
            }else{
                var newmin = "$" + $( "#entry_fee" ).slider( "values", 0 );
            }

            div.innerHTML = newmin + " - " + newmax;
        },
        change: function(event,ui) {
            filter_lobby_rows();
        }
    });

    var div = document.getElementById('entry_value');
    var minvalue = $( "#entry_fee" ).slider( "values", 0 );
    var maxvalue = $( "#entry_fee" ).slider( "values", 1 );

    if( maxvalue < 99 ){
        var newmax = "$" + $( "#entry_fee" ).slider( "values", 1 );
    }else{
        var newmax = "$100+";
    }

    if( minvalue < 1 ){
        var newmin = "Free";
    }else{
        var newmin = "$" + $( "#entry_fee" ).slider( "values", 0 );
    }

    div.innerHTML = newmin + " - " + newmax;		

    /***************
    * Lobby Rows
    ***************/
    $('input[name="sports"], input[name="types"]').on('change',function(e){
        filter_lobby_rows();
    });

    // Load Initial Lobby Rows
    filter_lobby_rows();
    
    /***************
    * Modals
    ***************/
    $(document.body).on('click','.contestInfo',function(e){
    	e.preventDefault();
    	var lID = $(this).attr('data-leagueid');
        $.ajax({
            url:HOST_NAME+'league/modal_contest_info/'+lID,
        }).done(function(response){
            console.log(response);
            $('#generalModal .modal-content').html(response);
        });
        $("#generalModal").modal("show");
	});	

    $(document.body).on('click','.contestEntries',function(e){
    	e.preventDefault();
    	var lID = $(this).attr('data-leagueid');
        $.ajax({
            url:HOST_NAME+'league/modal_contest_entries/'+lID,
        }).done(function(response){
            $('#generalModal .modal-content').html(response);
        });
        $("#generalModal").modal("show");
	});	
    
    $(document.body).on('click','.contestPrizes',function(e){
    	e.preventDefault();
    	var lID = $(this).attr('data-leagueid');
        $.ajax({
            url:HOST_NAME+'league/modal_contest_prizes/'+lID,
        }).done(function(response){
            $('#generalModal .modal-content').html(response);
        });
        $("#generalModal").modal("show");
	});	
    


    function filter_lobby_rows(){
        var minEntry = $('#entry_fee').slider('values',0);
        var maxEntry = $('#entry_fee').slider('values',1);
        var sports = filter_stringify("sports");
        var types = filter_stringify("types");
        var c = 0;        
        var sep = "";
        
        var url = HOST_NAME + "lobby/filter_rows/"+minEntry+"/"+maxEntry+"/"+sports+"/"+types;
        $.ajax({
            url: url
        }).done(function(results){
            $("#league_rows").html(results);

            init_countdown();
        });
    }
    
    function filter_stringify(fType) {
        var c = 0;        
        var sep = "";
        var returnString = "";
        
        $('input[name="'+fType+'"]:checked').each(function(){
            c++;
            sep = c > 1 ? "_" : "";
            returnString += sep + $(this).val();
        });
        if (c == 0) {returnString = "ALL";}
        
        return returnString;
    }
    
    function init_countdown() {
        var dst = null; 
        $.ajax({
            url: HOST_NAME+'serveroffset.php',
            async: false, dataType: 'text', 
            success: function(text){ 
                dst = parseInt(text);
            },
            error: function(http, message, exc) { 
                dst = -3;
            }
        });

        var clocks=$('.clock');
        var i;
        for (i=0; i <= clocks.length-1; i++) {
            var thisdom = $(clocks[i]);
            thisdom.countdown('destroy');

            var end_date=new Date(thisdom.data('cutoff-y'), thisdom.data('cutoff-m'), thisdom.data('cutoff-d'), thisdom.data('cutoff-h'), thisdom.data('cutoff-i'), thisdom.data('cutoff-s'));
            thisdom.countdown({
                until: end_date,
                serverSync: serverTime,
                timezone: dst,
                format: 'HMS', 
                compact: true, 
                description: '',
                onExpiry: function() {
                    window.setTimeout(function(){
                        window.location.reload();
                    }, 15000);
                }
            });
        }
    }
    
    
    
});






