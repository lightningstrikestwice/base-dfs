$(document).ready(function(){
	
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	$('div.main-overlay').animate({ opacity: 1 }, { duration: 300 });
	
	[].forEach.call(document.querySelectorAll('img[data-src]'),    function(img) {
		img.setAttribute('src', img.getAttribute('data-src'));
		img.onload = function() {
			img.removeAttribute('data-src');
		};
	});
	
	$('.toggle-menu').jPushMenu();
	
	$( ".toggle-menu" ).click(function() {
		$('#magic_cover').toggle(); 
	});
	$( "#magic_cover" ).click(function() {
		$('#magic_cover').toggle(); 
	});
	
	$('.filter-toggle').on('click',function(){
		$('.lobby .filters').slideToggle();
	});
	
	// game slate
	$('.game-slate').slick({
		infinite: true,
		slidesToShow: 12,
		slidesToScroll: 12,
		autoplay: false,
		dots: true,
		arrows: false,
		responsive: [
		{
			breakpoint: 1441,
			settings: {
				slidesToShow: 8,
				slidesToScroll: 8,
				infinite: true,
				dots: true
			}
		},
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 6,
				slidesToScroll: 6,
				infinite: true,
				dots: true
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			}
		},
		]
	});
	
	// lobby stuff
	$('.league-buttons').fadeIn();
	$('.lobby-games').fadeIn();
	
	// player select stuff
	$('.game-slate').fadeIn();
	
	// Lobby filters
//	$( function() {
//		
		
//		$( "#size" ).slider({
//			range: true,
//			min: 0,
//			max: 100,
//			values: [ 0, 50 ],
//			slide: function( event, ui ) {
//				$( "#size_value" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
//				var div = document.getElementById('size_value');
//				
//				var minvalue = $( "#size" ).slider( "values", 0 );
//				var maxvalue = $( "#size" ).slider( "values", 1 );
//				
//				if( minvalue < 1 ){
//					var newmin = "H2H";
//				}else{
//					var newmin = $( "#size" ).slider( "values", 0 );
//				}
//				
//				if( maxvalue < 100 ){
//					var newmax = $( "#size" ).slider( "values", 1 );
//				}else{
//					var newmax = "No Limit";
//				}
//										
//				div.innerHTML = newmin + " - " + newmax;
//			},
//			stop: function(event, ui){
//				$( "#size_value" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
//				var div = document.getElementById('size_value');
//				
//				var minvalue = $( "#size" ).slider( "values", 0 );
//				var maxvalue = $( "#size" ).slider( "values", 1 );
//				
//				if( minvalue < 1 ){
//					var newmin = "H2H";
//				}else{
//					var newmin = $( "#size" ).slider( "values", 0 );
//				}
//				
//				if( maxvalue < 100 ){
//					var newmax = $( "#size" ).slider( "values", 1 );
//				}else{
//					var newmax = "No Limit";
//				}
//										
//				div.innerHTML = newmin + " - " + newmax;
//			}
//		});
//		
//		var div = document.getElementById('size_value');
//		
//		var minvalue = $( "#size" ).slider( "values", 0 );
//		var maxvalue = $( "#size" ).slider( "values", 1 );
//		
//		if( minvalue < 1 ){
//			var newmin = "H2H";
//		}else{
//			var newmin = $( "#size" ).slider( "values", 0 );
//		}
//		
//		if( maxvalue < 99 ){
//			var newmax = $( "#size" ).slider( "values", 1 );
//		}else{
//			var newmax = "No Limit";
//		}
//								
//		div.innerHTML = newmin + " - " + newmax;
//				
//	} );
	
	
	// Player select
	if( $(window).width() < 1024 ){
				
		$('.roster-player').click(function(e){
			
			e.preventDefault();
			
			var pos = $(this).attr('data-pos');
			
			$('.player-list').addClass('open');
			$('body').addClass('no-overflow');
			$('#'+pos+'_tab').tab('show');
			
			$('.add-btn').click(function(e){
				e.preventDefault();
				$('.player-list').removeClass('open');
				$('body').removeClass('no-overflow');
			});
		});
	}
	
});






