jQuery(document).ready(function($){
	$('#avatar_save').click(function(){
		setTimeout(function(){location.reload();}, 3000);	
	});
    
	var uploader = new plupload.Uploader({
		runtimes 			:'gears,html5,flash,silverlight,browserplus',
		container 			:'avatar_upload_wrap',
		drop_element 		:'avatar_drag_drop',
		browse_button  		:'browse_files',
		max_file_count 		:1,
		max_file_size 		:'1mb',
		file_data_name		:'file',
		multiple_queues		:false,
		multi_selection		:false,
		url					:HOST_NAME+'register/add_avatar',
		flash_swf_url 		:HOST_NAME+'assets/common/js/plupload/plupload.flash.swf',
		silverlight_xap_url :HOST_NAME+'assets/common/js/plupload/plupload.silverlight.xap',
		filters 			:[
								{title : 'Photos', extensions : 'jpg,png' }
							],
		multipart_params 	:''					
	});

    uploader.bind("Init", function(up, params) {
		$("#media-items").html("<br><div style=\"display:none\">Current runtime: " + params.runtime + "</div>");
	});
	
	uploader.init();
	
	uploader.bind("FilesAdded", function(up, files) {
//		console.log(files);
		$.each(files, function(i, file) {
			$("#avatar_drag_drop").hide();
		});
		up.refresh();
		$("#avatar_image_zone").addClass("loading");
		$('#media-items').append('<div class="alert alert-info">File must be .jpg or .png and must be smaller than 2mb.</div>');
		uploader.start();
	});
	
	uploader.bind("Error", function(up, err) {
//		console.log(err);
		$("#media-items").append("<div class=\"alert alert-error\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>"+
			"Error: " + err.code +
			", Message: " + err.message +
			(err.file ? ", File: " + err.file.name : "") +
			"</div>"
		);
		$("#media-item-thumb-" + err.file.id ).remove();
		
		up.refresh();
		$("#avatar_drag_drop").show();
	});
	
	uploader.bind("FileUploaded", function(up, file, response) {
        var obj = jQuery.parseJSON(response.response);
        console.log(obj.url);
        
		if(UrlExists(obj.url)){
			$("#avatar_image_zone").html("<img src=\""+obj.url+"\" alt=\"\" id=\"cropbox\" />");
			$("#avatar_preview").prepend("<img src=\""+obj.url+"\" alt=\"\" id=\"preview\" />");
			$("#temp_image").val(obj.filename);
			$("#avatar_image_zone").removeClass("loading");
			$("#avatar_save").removeClass("disabled");
			$('.form-actions').attr('id', 'frick');
			$("#avatar_save").removeAttr("disabled");
			$("#cropform").attr("onsubmit","return checkCoords()");
			
			$('#cropbox').Jcrop({
				onChange: showPreview,
				onSelect: showPreview,
				onRelease: hidePreview,
				setSelect: [ 0, 0, obj.final_width, obj.final_height ],
				aspectRatio: 1
			});

            var preview = jQuery('#avatar_preview');
			// Our simple event handler, called from onChange and onSelect
			// event handlers, as per the Jcrop invocation above
			function showPreview(coords){
				if (parseInt(coords.w) > 0){
					var rx = obj.final_width / coords.w;
					var ry = obj.final_height / coords.h;
					
					var scale = obj.width / jQuery(".jcrop-holder").width();
					
					jQuery("#preview").css({
						width: Math.round(rx * jQuery(".jcrop-holder").width()) + "px",
						height: Math.round(ry * jQuery(".jcrop-holder").height) + "px",
						marginLeft: "-" + Math.round(rx * coords.x) + "px",
						marginTop: "-" + Math.round(ry * coords.y) + "px"
					});
				}
				
				jQuery("#x").val(Math.round(coords.x * scale));
				jQuery("#y").val(Math.round(coords.y * scale));
				jQuery("#w").val(Math.round(coords.w * scale));
				jQuery("#h").val(Math.round(coords.h * scale));
			}
			
			function hidePreview(){
                preview.stop().fadeOut('fast');
			}
		}
		up.refresh();
	});
});
											
function checkCoords(){
	if (parseInt(jQuery("#w").val())>0) return true;
	alert("Select area for desired image before saving.");
	return false;
};

function UrlExists(url){
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}