$(document).ready(function(){
    var resizefunc = [];
    var ua = navigator.userAgent,
        event = (ua.match(/iP/i)) ? "touchstart" : "click";    
    
    var w,h,dw,dh;
    
    function changeptype() {
        w = $(window).width();
        h = $(window).height();
        dw = $(document).width();
        dh = $(document).height();

        if(jQuery.browser.mobile === true){
            $("body").addClass("mobile").removeClass("adminbody");
        }

        if(!$("#main").hasClass("forced")){
            if(w > 990){
                $("body").removeClass("smallscreen").addClass("widescreen");
                $("#main").removeClass("enlarged");
            }else{
                $("body").removeClass("widescreen").addClass("smallscreen");
                $("#main").addClass("enlarged");
                $(".left ul").removeAttr("style");
            }
            if($("#main").hasClass("enlarged") && $("body").hasClass("adminbody")){
                $("body").removeClass("adminbody").addClass("adminbody-void");
            }else if(!$("#main").hasClass("enlarged") && $("body").hasClass("adminbody-void")){
                $("body").removeClass("adminbody-void").addClass("adminbody");
            }
        }
    }
    
    // Slide Side Menu In/Out
    $('.open-left').on(event,function(){
        $("#main").toggleClass("enlarged");
        $("#main").addClass("forced");

        if($("#main").hasClass("enlarged") && $("body").hasClass("adminbody")) {
            $("body").removeClass("adminbody").addClass("adminbody-void");
        } else if(!$("#main").hasClass("enlarged") && $("body").hasClass("adminbody-void")) {
            $("body").removeClass("adminbody-void").addClass("adminbody");
        }

        if($("#main").hasClass("enlarged")) {
            $(".left ul").removeAttr("style");
        } else {
            $(".subdrop").siblings("ul:first").show();
        }
        
        $(this).toggleClass('pl-4');
    });
    
    // Expand Multi-level Menus
    $("#sidebar-menu a").on(event,function(){
        if(!$("#main").hasClass("enlarged")){
            if($(this).parent().hasClass("submenu")) {

            }
            if(!$(this).hasClass("subdrop")) {
                // hide any open menus and remove all other classes
                $("ul",$(this).parents("ul:first")).slideUp(350);
                $("a",$(this).parents("ul:first")).removeClass("subdrop");
                $("#sidebar-menu .pull-right i").removeClass("md-remove").addClass("md-add");

                // open our new menu and add the open class
                $(this).next("ul").slideDown(350);
                $(this).addClass("subdrop");
                $(".pull-right i",$(this).parents(".submenu:last")).removeClass("md-add").addClass("md-remove");
                $(".pull-right i",$(this).siblings("ul")).removeClass("md-remove").addClass("md-add");
            }else if($(this).hasClass("subdrop")) {
                $(this).removeClass("subdrop");
                $(this).next("ul").slideUp(350);
                $(".pull-right i",$(this).parent()).removeClass("md-remove").addClass("md-add");
            }
        }
    });
    
    changeptype();

    $("body").trigger("resize");
    
    // Mobile Right side bar toggle
    $('.right-bar-toggle').on('click', function(e){
        $('#main').toggleClass('right-bar-enabled');
    });

});

